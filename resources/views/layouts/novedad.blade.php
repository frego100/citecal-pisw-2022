@extends ('layouts.admin')
@section ('contenido')
<div class="" align="center">
  <h2><strong><u>Novedades v 1.1.1</u></strong></h2>
</div>
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link" id="noticias" data-toggle="tab" href="#notice" role="tab" aria-controls="home" aria-selected="true">Noticias</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="generales" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">General</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="recursos" data-toggle="tab" href="#rh" role="tab" aria-controls="profile" aria-selected="false">Recursos Humanos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="log" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Logistica</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="desarrollo" data-toggle="tab" href="#dp" role="tab" aria-controls="profile" aria-selected="false">Desarrollo de Producto</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="cost" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">Costos</a>
  </li>
</ul>

<div class="tab-content">
  <div class="tab-pane" id="notice" role="tabpanel" aria-labelledby="profile-tab">
    <div style="margin:10px">
      <h3><strong><u>Nuevas Noticias:</u></strong></h3>
      <div class="row">
        <div class="col-md-4" id="f0">
        </div>
        <div class="col-md-4" id="f1">
        </div>
        <div class="col-md-4" id="f2">
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane" id="home" role="tabpanel" aria-labelledby="home-tab">
    <div style="margin:10px">
      <h3><strong><u>Mantente al dia:</u></strong></h3>
      <div class="row">
        <div class="col-md-4" align="center">
          <img src="../photo/novedades/general/novedad1.png" class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-md-8">
          <p>Enterate de las ultimas noticias, actualizaciones y nuevas funcionalidades del sistema ERP <br>
          pulsando tan solo un boton.</p>
          <p>Con una sencilla interfaz enterate de todo.</p>
          <p> <img src="../photo/novedades/general/novedad2.png" class="img-fluid border border-dark" style="padding:5px; margin:5px" alt="Responsive image"></p>
          <p>Precionando el modulo de tu interes enterate de las nuevas funcionalidades y actualizaciones <br>que mejoran tu experiencia.</p>
          <br>
          <blockquote class="blockquote">
            <footer class="blockquote-footer">Cite Cuero y Calzado</footer>
          </blockquote>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane" id="rh" role="tabpanel" aria-labelledby="profile-tab">
    <div style="margin:10px">
      <h3><strong><u>Escala Salarial y Beneficios:</u></strong></h3>
      <div class="row">
        <div class="col-md-4" align="center">
          <img src="../photo/novedades/rh/rhnovedad1.png" class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-md-8">
          <h3><strong><u>Nuevos Submodulos</strong></u></h3>
          <div class="">
            <h4>Configuracion de horas laborales</h4>
            <div>
                <p><img src="../photo/novedades/rh/rhnovedad2.png" class="img-fluid border border-dark" alt="Responsive image"></p>
                <ul>
                  <li>
                    <p>Configura las horas laborales en tu empresa para conocer la escala salarial de tus trabajadores.</p>
                  </li>
                  <li>
                    <p class="text-red">Informacion importante para otros modulos</p>
                  </li>
                </ul>
            </div>
          </div>
          <div class="">
            <h4>Escala Salarial</h4>
            <div class="">
              <p><img src="../photo/novedades/rh/rhnovedad3.png" class="img-fluid border border-dark" alt="Responsive image"></p>
              <p><img src="../photo/novedades/rh/rhnovedad4.png" class="img-fluid border border-dark" alt="Responsive image"></p>
              <p><img src="../photo/novedades/rh/rhnovedad5.png" class="img-fluid border border-dark" alt="Responsive image"></p>
              <ul>
                <li>
                  <p>Registra tus trabajadores con sus respectivas caracteristicas y observa el sueldo semanal, diario de tu trabajador.</p>
                </li>
                <li>
                  <p class="text-red">Informacion importante para otros modulos</p>
                </li>
              </ul>
            </div>
          </div>
          <div class="">
            <h4>Beneficios</h4>
            <div class="">
              <p><img src="../photo/novedades/rh/rhnovedad6.png" class="img-fluid border border-dark" alt="Responsive image"></p>
            </div>
            <ul>
              <li>
                <p>Observa los beneficios que poseen tus trabajadores</p>
              </li>
            </ul>
          </div>
          <blockquote class="blockquote">
            <footer class="blockquote-footer">Cite Cuero y Calzado</footer>
          </blockquote>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div style="margin:10px">
      <h3><strong><u>Nuevas vistas y mejor experiencia:</u></strong></h3>
      <div class="row">
        <div class="col-md-4" align="center">
          <img src="../photo/novedades/log/lnovedad1.png" class="img-fluid" alt="Responsive image">
        </div>
        <div class="col-md-8">
          <h3><strong><u>Nuevas Vistas</strong></u></h3>
          <div class="">
            <h4>Materiales</h4>
            <div>
                <p><img src="../photo/novedades/log/lnovedad2.png" class="img-fluid border border-dark" alt="Responsive image"></p>
                <p><img src="../photo/novedades/log/lnovedad3.png" class="img-fluid border border-dark" alt="Responsive image"></p>
                <p><img src="../photo/novedades/log/lnovedad4.png" class="img-fluid border border-dark" alt="Responsive image"></p>
                <p><img src="../photo/novedades/log/lnovedad5.png" class="img-fluid border border-dark" alt="Responsive image"></p>
                <ul>
                  <li>
                    <p>Escoje lo que desees ver.</p>
                  </li>
                </ul>
            </div>
          </div>
          <div class="">
            <h4>Recepcion de materiales</h4>
            <div class="">
              <p><img src="../photo/novedades/log/lnovedad6.png" class="img-fluid border border-dark" alt="Responsive image"></p>
              <p><img src="../photo/novedades/log/lnovedad7.png" class="img-fluid border border-dark" alt="Responsive image"></p>
              <ul>
                <li>
                  <p>Selecciona las fechas que se desee ver.</p>
                </li>
                <li>
                  <p class="text-red">La lista de ordenes de compra se actualizan cada 20 segundos.</p>
                </li>
              </ul>
            </div>
          </div>
          <div class="">
            <h4>Kardex</h4>
            <div class="">
              <p><img src="../photo/novedades/log/lnovedad8.png" class="img-fluid border border-dark" alt="Responsive image"></p>
              <p><img src="../photo/novedades/log/lnovedad9.png" class="img-fluid border border-dark" alt="Responsive image"></p>
              <p><img src="../photo/novedades/log/lnovedad10.png" class="img-fluid border border-dark" alt="Responsive image"></p>
              <p><img src="../photo/novedades/log/lnovedad11.png" class="img-fluid border border-dark" alt="Responsive image"></p>
            </div>
            <ul>
              <li>
                <p>Escoje lo que desees observar</p>
              </li>
              <li>
                <p class="text-red">El kardex se actualiza cada 20 segundos.</p>
              </li>
            </ul>
          </div>
          <div class="">
            <h4>Historial</h4>
            <div class="">
              <p><img src="../photo/novedades/log/lnovedad12.png" class="img-fluid border border-dark" alt="Responsive image"></p>
              <p><img src="../photo/novedades/log/lnovedad13.png" class="img-fluid border border-dark" alt="Responsive image"></p>
            </div>
            <ul>
              <li>
                <p>Selecciona la fecha que desees observar</p>
              </li>
            </ul>
          </div>
          <blockquote class="blockquote">
            <footer class="blockquote-footer">Cite Cuero y Calzado</footer>
          </blockquote>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane" id="dp" role="tabpanel" aria-labelledby="profile-tab">
  </div>
  <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab">
  </div>
</div>

<div class="modal fade" id="modal_img" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo"></h4>
            </div>
            <div class="modal-body">
              <img src="" id="img_modal" width="100%" alt="">
            </div>
            <div class="modal-footer">
      				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
      			</div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
  $(function () {
    var data;
    $('#myTab li:first-child a').tab('show')
    $.ajax({
      url: "/novedades/noticias",
      success: function(html){
        data=html;
        cargar_datos(data);
      }
    });
    setInterval(function(){obt()},20000)
    function obt(){
      $("#actualizar_datos").toggle("slow");
      $.ajax({
        url: "/novedades/noticias",
        success: function(html){
          data=html;
          $("#f0").empty();
          $("#f1").empty();
          $("#f2").empty();
          cargar_datos(data);
          $("#actualizar_datos").toggle("slow");
        }
      });
    }
    function cargar_datos(data)
    {
      var datos=data
      var f=0;
      for(var i=0;i<data.length;i++)
      {
        switch (f) {
          case 0:
          $("#f"+f).append(
          "<div class='card border border-dark' style='margin:10px;'>"+
              "<img id='i"+data[i].cod+"' src='photo/noticias/"+data[i].url_imagen+"' class='card-img-top' style='height:500px'>"+
              "<div class='card-body'>"+
                "<h5 class='card-title'><strong>"+data[i].nombre_evento+"</strong></h5>"+
                "<p class='card-text'>"+data[i].descripcion+"</p>"+
                "<p class='card-text'><small class='text-muted'>"+data[i].nombre_categoria+"</small></p>"+
              "</div>"+
            "</div>"
          )
          f=1;
          break;
          case 1:
          $("#f"+f).append(
          "<div class='card border border-dark' style='margin:10px;'>"+
              "<img id='i"+data[i].cod+"' src='photo/noticias/"+data[i].url_imagen+"' class='card-img-top' style='height:500px' >"+
              "<div class='card-body'>"+
                "<h5 class='card-title'><strong>"+data[i].nombre_evento+"</strong></h5>"+
                "<p class='card-text'>"+data[i].descripcion+"</p>"+
                "<p class='card-text'><small class='text-muted'>"+data[i].nombre_categoria+"</small></p>"+
              "</div>"+
            "</div>"
          )
          f=2;
          break;
          case 2:
          $("#f"+f).append(
          "<div class='card border border-dark' style='margin:10px;'>"+
              "<img id='i"+data[i].cod+"' src='photo/noticias/"+data[i].url_imagen+"' class='card-img-top' style='height:500px' >"+
              "<div class='card-body'>"+
                "<h5 class='card-title'><strong>"+data[i].nombre_evento+"</strong></h5>"+
                "<p class='card-text'>"+data[i].descripcion+"</p>"+
                "<p class='card-text'><small class='text-muted'>"+data[i].nombre_categoria+"</small></p>"+
              "</div>"+
            "</div>"
          )
          f=0;
          break;
          default:
        }
        $("#i"+data[i].cod).off('click');
        $("#i"+data[i].cod).on('click',function(){
          var id = $(this).attr("id");
          id=id.substring(1,id.length)
          var index=buscar(id);
          moda_imagen(index);
        });
      }
    }
    function moda_imagen(index)
    {

      $("#img_modal").attr("src","photo/noticias/"+data[index].url_imagen)
      $("#modal_img").modal({show:true});
    }
    function buscar(id)  {
      var index = -1;
      var filteredObj = data.find(function(item, i){
        if(item.cod == id){
          index = i;
          return index;
        }
      });
      return index;
    }
  })
</script>
@endsection
