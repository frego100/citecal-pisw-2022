@extends ('layouts.app')
@section ('content')
<div class="container">
<div>
  <h1 class="asd">Empresa: {{$var}}</h1>
</div>
  <hr class=" my-4">
<h2 class="">Usuarios Registrados</h2>
<h6>Leyenda de Roles</h6>
<p>2:Dueño de Empresa 3: Jefe de Logistica</p>
<div class="x_content table-responsive">
  <table id="table_mp" class="table table-hover">
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Usuario</th>
        <th>Rol</th>
        <th>Estado</th>
      </tr>
    </thead>
    <tbody>
      @foreach($usuarios as $us)
      <tr>
        <td>{{$us->name}}</td>
        <td>{{$us->email}}</td>
        <td>{{$us->role}}</td>
        @if($us->estado==0)
          <td>Desactivado</td>
          <td>
            <a href="" data-target="#modal-activar-{{$us->email}}" data-toggle="modal"><button class="bttn-slant bttn-md bttn-warning ">Activar</button></a>
          </td>
        @else
          <td>Activo</td>
          <td>
            <a href="" data-target="#modal-desactivar-{{$us->email}}" data-toggle="modal"><button class="bttn-slant bttn-md bttn-warning">Desactivar</button></a>
          </td>
        @endif
      </tr>
        @include('User.modal_activar')
        @include('User.modal_desactivar')
        @endforeach
    </tbody>
  </table>
</div>

</div>
@endsection
