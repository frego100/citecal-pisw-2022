@if($message =Session::get('success'))
<div class="alert alert-success alert-block">
  <button type="button" class="close" data-dismiss="alert"></button>
  <strong>{{$message}}</strong>
</div>
@endif
@if($message =Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert"></button>
  <strong>{{$message}}</strong>
</div>
@endif
@if($message =Session::get('warning'))
<div class="alert alert-warning alert-block">
  <button type="button" class="close" data-dismiss="alert"></button>
  <strong>{{$message}}</strong>
</div>
@endif
@if($message =Session::get('info'))
<div class="alert alert-info alert-block">
  <button type="button" class="close" data-dismiss="alert"></button>
  <strong>{{$message[0]}}</strong>
  <a target="_blank" href="/logistica/kardex/orden_salida_pdf/{{$message[2]}}">{{$message[1]}}</a>
</div>
@endif
@if($message =Session::get('salida_tienda'))
<div class="alert alert-info alert-block">
  <button type="button" class="close" data-dismiss="alert"></button>
  <strong>{{$message[0]}}</strong>
  <a target="_blank" href="/logistica/tienda/orden_salida_pdf/{{$message[2]}}">{{$message[1]}}</a>
</div>
@endif
