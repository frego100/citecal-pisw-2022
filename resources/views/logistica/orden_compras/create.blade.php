@extends ('layouts.admin')
@section('contenido')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div id="buscando" align="center" style="display:none; margin:40px;">
                    <div class="alert alert-success" role="alert">
                        Obteniendo materiales...
                    </div>
                </div>
                <div class="x_title">
                    <h2 class="font-weight-bold">Nueva Orden de Compra- Materiales Normales</h2>
                    <div class="clearfix"></div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['url' => 'logistica/orden_compras/store/', 'method' => 'POST', 'autocomplete' => 'off']) !!}

                {{ Form::token() }}
                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="fecha">
                            <label for="total_orden_compra">Fecha:</label>
                             <input type="date" required="required" id="fecha" name="fecha" value="{{$fechaActual->format('Y-m-d')}}" disabled>
                             <input type="hidden" id="fecha2" name="fecha2">
                        </label>
                    </div>

                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12">
                      <label for="total_orden_compra">Orden de Compra Nª:</label>
                      <input type="text" required="required" id="ordenCompraN" name="codigo_orden" value="{{$ordenCompraN}}" disabled >
                    </div>
                </div>
                <div class="row" style="margin-bottom:-1%">
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12" >
                      <label for="total_orden_compra" >Proveedor:</label>
                    </div>
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12">
                      <label for="total_orden_compra">Tipo de Compra:</label>
                    </div>
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12"  id="dias_credito_div">
                    <label for="total_orden_compra">Días de crédito:</label>
                    </div>
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12">
                        <select id="proveedor" name="proveedor" class="custom-select" required>
                        <option value="" selected disabled >Proveedor</option>
                            @foreach ($proveedores as $item)
                              <option value="{{ $item->RUC_proveedor}}" >{{ $item->nom_proveedor}}</option>
                             @endforeach
                        </select>
                    </div>
                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12">
                        <select  id="tipo_compra" name="tipo_compra" class="custom-select" required>
                            <option value="" disabled selected>Tipo de Compra</option>
                            <option value="1">Credito</option>
                            <option value="0">Contado</option>
                        </select>
                    </div>

                    <div class="form-group  col-md-3 col-md-offset-4 col-xs-12"  id="dias_credito_div2">
                      <input type="number"   id="dias_credito" name="dias_credito">
                    </div>


                    <!--MODAL-->
                    <div class="form-group  col-md-3 col-md-offset-3 col-xs-12" id="modal-data" >
                    <button class="bttn-unite bttn-md bttn-warning" type="button" data-target="#modal-create"
                    data-toggle="modal" data-pro="$aux" id="modalCreate" >Agregar Materiales</button>
                    </div>
                </div>
                <div class="x_content table-responsive">
                  <h3 class="font-weight-bold">Listado de Materiales
                  </h3>

                  <table id="material_normal" class="display">
                      <thead>
                          <tr>
                              <th>Cod. Material</th>
                              <th>Descripción</th>
                              <th>Cantidad</th>
                              <th>Unidad Compra</th>
                              <th>Valor Unitario (Sin IGV)</th>
                              <th>Valor Total</th>
                              <th>Eliminar</th>
                          </tr>
                      </thead>
                      <tbody id="materialSelec">
                      </tbody>
                  </table>

               </div>


                <div class="row" style='margin-top : 1%;'>
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                            <label   style='width : 130%;'>
                                <label  style='margin-right : 3%;'>Sub Total:</label>
                                S/.<input type="number" step='any'  style='width : 55%;' id="subTotalval" name="subTotalval" disabled="true">

                            </label>
                        </div>
                </div>

                <div class="row" style='margin-top : -1%;'>
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                            <label   style='width : 130%;'>
                                <label  style='margin-right : 3%;'>IGV (18%):</label>
                                S/.<input type="number" step='any'  style='width : 55%;' id="igvSubtotal" name="igvSubtotal" disabled="true">
                            </label>
                        </div>
                </div>
                <!-- CANTIDAD TOTAL Y FALTANTE-->
                <input type="hidden" step='any'  style='width : 55%;' id="cantidad_total_controller" name="cantidad_total_controller" >
                <input type="hidden" step='any'  style='width : 55%;' id="cantidad_faltante_controller" name="cantidad_faltante_controller" >
                  <!-- -->
                <div class="row" style='margin-top : -1%;'>
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                            <label   style='width : 130%;'>
                                <label  style='margin-right : 8%;'>Total:</label>
                                   S/.<input type="number" step='any'  style='width : 55%;' id="idTotal" name="idTotal"  disabled="true">
                            </label>
                        </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="observaciones">Observaciones</label>
                        <input type="hidden" id="tipoDetalle" name="tipoDetalle" value="1" class="form-control">
                        <input type="text" id="obervaciones" name="observaciones" maxlength="2000" class="form-control">
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                    <a href="{{ url('logistica/orden_compras') }}"><button type="button"
                                class="bttn-unite bttn-md bttn-danger ">Cancelar</button></a>
                        <button type="submit" id="guardar"
                            class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5" value="GuardarNormal">Guardar</button>
                    </div>
                </div>


                {!! Form::close() !!}


                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script >



                </script>
                <script>

                    $(document).ready(function() {

                        var data =<?php echo $proveedores; ?>;
                        var categoria =<?php echo $categorias; ?>;
                        var cateNomb =<?php echo $categoria; ?>;
                        var materiales =<?php echo $materiales; ?>;
                        var datatemp = [];
                        var costoTotalMat=0;
                        var subTotal=0;
                        var cantidadTot=0;
                        $("#consultar").click(function() {
                            var ruc = $("#ruc").val();
                            $("#consultar").text("Consultando...");
                            $.ajax({
                                url: "consulta/ruc/" + ruc,
                                Type: 'get',
                                dataType: "json",
                                success: function(datos) {
                                    $("#consultar").text("Consultar");
                                    if (datos[0] != "nada") {
                                        $("#razon_social").val(datos[0]);
                                        $("#direccion1").val(datos[1])
                                    } else {
                                        alert("RUC no existente")
                                    }
                                }
                            })
                        });

                        $('#guardar').on('click', function() {
                            $('#dias_credito_div').show();
                            $('#dias_credito').val(0);
                            $('#ordenCompraN').prop('disabled',false);
                            $('#subTotalval').prop('disabled',false);
                            $('#igvSubtotal').prop('disabled',false);
                            $('#idTotal').prop('disabled',false);
                        });

                        $('#tipo_compra').on('change',function(){

                            var id = $(this).val();
                            if(id==0){

                                $('#dias_credito_div').hide();
                                $('#dias_credito_div2').hide();
                                $('#dias_credito').prop('disabled', false);
                                $('#dias_credito').prop('required', false);

                                $('#dias_credito').val(null);
                            }//
                            else if(id==1){
                                $('#dias_credito_div').show();
                                $('#dias_credito_div2').show();
                                $('#dias_credito').prop('required', true);
                            }
                            var index=buscar(id);
                            modal_editar(index);
                         });


                        $('#proveedor').on('change',function(){
                            datatemp = [];
                            $("#modal-data").empty();
                            var id = $(this).val();
                            var index=buscar(id);
                            modal_editar(index);
                         });
                         $("#categoria").change(function() {
                            let valor = $("#categoria").val();
                            $("#subcategoria").empty();
                            $("#materiales").empty();
                            generar_opciones_subcategoria(valor);
                        });
                         $( "#subcategoria" ).change(function() {
                            let valor=$("#subcategoria").val();
                            generar_opciones_materiales(valor);
                            $("#materiales").empty();
                        });

                        function modal_editar(index)  {

                            var nombre=data[index].nom_proveedor;
                            var catego=data[index].proveedor_categoria;
                            var subcatego=data[index].RUC_proveedor;

                            var index=buscarCategoria(catego);
                            if(index!=-1){
                              var nombreCategoria=categoria[index];
                            } else{
                                var nombreCategoria="Sin categoria";
                            }
                            var orden = $("#ordenCompraN").attr('value');

                            $("#nombre").val(nombre);
                            $("#catego").val(catego);
                            $("#orden").val(orden);
                            $("#categoria").val(nombreCategoria);
                            $("#modal-data").append("<button class='bttn-unite bttn-md bttn-warning' type='button' data-target='#modal-create' data-toggle='modal' data-pro="+nombre+"  data-cod="+catego+" data-orden="+orden+" data-categoria="+categoria+" id='modalCreate' >Agregar Materiales</button>");
                            $("#categoria").empty();

                            $("#subcategoria").empty();
                            generar_opciones_categoria(catego);



                            var val=0;


                            //AGREGAR VALOR A ORDEN DE COMPRA
                            var t = $("#material_normal").DataTable();

                            $('#material_normal').on( 'click', 'button', function () {
                            t
                                .row( $(this).parents('tr') )
                                .remove()
                                .draw();
                                var sumaTotal=0;
                                $('input[name^="costo_total"]').each(function()
                                {
                                    sumaTotal = sumaTotal + 1*$(this).val();

                                });

                                $("#subTotalval").val(sumaTotal);
                                $("#idTotal").val((sumaTotal*1.18).toFixed(2));
                                $("#igvSubtotal").val( ($("#idTotal").val() - $("#subTotalval").val()).toFixed(2));

                                var cantidadT=0;
                                var contador = 0;
                                $('input[name^="cantidad_algo"]').each(function()
                                {
                                    cantidadT = cantidadT + 1*$(this).val();
                                    contador++;
                                });
                                $("#cantidad_total_controller").val(cantidadT);
                                $("#cantidad_faltante_controller").val(cantidadT);
                            } );
                            $("#modalCreate").click(function() {

                                $("#materiales").empty();
                                $("#subcategoria").empty();
                                $("#subcategoria").append("<option value='' selected disabled>Subcategoria</option>");
                                generar_opciones_subcategoria(subcatego);
                            //$("#materiales input[type=checkbox]").prop("checked", false);
                            });


                             $("#agregar").click(function() {
                                var valor=1;
                                var total_temp=0;

                                $.each(materiales,function(key,value){
                                    $('input[name^="material_lista"]').each(function(){
                                        var index=0;
                                        codigo_temporal = $(this).val();
                                            var seleccionado = $(this).is(':checked');
                                            if(seleccionado && value.cod_material == codigo_temporal && !(datatemp.find(element => element == value.cod_material))){
                                            const tr = $("<tr><td><input name='nombre[]' type='hidden'value='"+value.cod_material+"' >"+value.cod_material+"</td>"+
                                            "<td><input name='descripcion[]' type='hidden' value='"+value.descrip_material+"' >"+value.descrip_material+"</td>"+
                                            "<td><input name='cantidad_algo[]'  type='number'  min='0' step='0.01'  id='cantidadMat"+value.cod_material+"'style='width : 90%;'required ></td>"+
                                            "<td><input name='unidad_compra[]' type='hidden'value='"+value.unidad_compra+"'>"+value.descrip_unidad_compra+"</td>"+
                                            "<td><input name='costo_sin_igv[]' type='hidden'value='"+value.costo_sin_igv_material+"'>S/."+value.costo_sin_igv_material+"</td>"+

                                            "<td><input name='costo_total[] costo_total_sum' id='cantidadTotalMat"+value.cod_material+"' type='hidden'>S/.<input type='text' value='0'  id='cantidadTotalMat2"+value.cod_material+"' disabled='true'  style='width : 80%;'></td>"+
                                            "<td><button class='bttn-unite bttn-md bttn-danger'><i class='fas fa-trash-alt'></i></button></a></td>");

                                            t.row.add(tr[0]).draw();

                                            var valortotal;
                                            var sumTotal;
                                            var sumCantidadTotal;
                                            var valorCantidadTotal;
                                            $("#cantidadMat"+value.cod_material).change(function () {

                                                costoTotalMat=$(this).val();

                                                $("#cantidadTotalMat"+value.cod_material).val(($("#cantidadMat"+value.cod_material).val()*value.costo_sin_igv_material).toFixed(2));

                                                $("#cantidadTotalMat2"+value.cod_material).val(($("#cantidadMat"+value.cod_material).val()*value.costo_sin_igv_material).toFixed(2));


                                                if(valortotal==null){
                                                    subTotal=subTotal+($("#cantidadMat"+value.cod_material).val()*value.costo_sin_igv_material);
                                                    valortotal = $("#cantidadTotalMat2"+value.cod_material).val();
                                                    cantidadTot=cantidadTot+($("#cantidadMat"+value.cod_material).val()*1);
                                                    valorCantidadTotal=$("#cantidadMat"+value.cod_material).val()*1;
                                                    $("#subTotalval").val(subTotal.toFixed(2));
                                                    $("#cantidad_total_controller").val(cantidadTot);
                                                    $("#cantidad_faltante_controller").val(cantidadTot);
                                                    $("#idTotal").val((subTotal*1.18).toFixed(2));
                                                    $("#igvSubtotal").val( ($("#idTotal").val() - $("#subTotalval").val()).toFixed(2));

                                                }

                                                else{

                                                    sumTotal = ($("#cantidadMat"+value.cod_material).val()*value.costo_sin_igv_material);
                                                    sumCantidadTotal=$("#cantidadMat"+value.cod_material).val()*1;
                                                    subTotal= subTotal- valortotal+ sumTotal;
                                                    cantidadTot=cantidadTot-valorCantidadTotal+sumCantidadTotal;
                                                    valorCantidadTotal=$("#cantidadMat"+value.cod_material).val()*1;
                                                    valortotal = $("#cantidadTotalMat2"+value.cod_material).val();
                                                    $("#cantidad_total_controller").val(cantidadTot);
                                                    $("#cantidad_faltante_controller").val(cantidadTot);
                                                    $("#subTotalval").val(subTotal.toFixed(2));
                                                    $("#idTotal").val((subTotal*1.18).toFixed(2));
                                                    $("#igvSubtotal").val( ($("#idTotal").val() - $("#subTotalval").val()).toFixed(2));
                                                }
                                                console.log($("#cantidadTotalMat2"+value.cod_material).val());
                                            });

                                            $("#costototal").empty();

                                            datatemp.push(value.cod_material);
                                        }
                                        index++;
                                    });
                                });
                            });
                            cargaDatos();
                        }

                        function cargaDatos(){

                           $( "#categoria" ).select(function() {
                            let valor=$("#categoria").val();
                            $("#subcategoria").empty();
                            generar_opciones_subcategoria(valor);
                         });
                        }

                        function buscar(id)  {

                            var index = -1;
                            var filteredObj = data.find(function(item, i){

                            if(item.RUC_proveedor == id){
                                index = i;
                                return index;
                            }
                            });
                            return index;
                        }
                        function buscarMaterial(id)  {

                         var index = -1;
                         var filteredObj = data.find(function(item, i){

                         if(item.cod_material == id){
                             index = i;
                             return index;
                         }
                         });
                         return index;
                        }
                        function buscarCategoria(id)  {

                         var index = -1;
                         var filteredObj = categoria.find(function(item, i){

                         if(item.cod_categoria == id){
                             index = i;
                             return index;
                         }
                         });
                         return index;
                        }
                        var lista_materiales=[];
                        var cod_materiales=[];


                        function generar_opciones_materiales(valor) {
                            $.ajax({
                                url: "consulta/obtener_mat/"+valor,
                                success: function(html){
                                    $.each(html,function(key,value){
                                        var codigo=value.cod_material;
                                        var nombre=value.descrip_material;
                                        if (datatemp.find(element => element == value.cod_material)) {

                                            $("#materiales").append(
                                            "<input  type='checkbox' data-idMaterial='" + codigo +
                                            "' name='material_lista[]' value='" + codigo + "' checked disabled>" +
                                            "<label for='" + codigo + "'>" + nombre + "</label></br>");
                                        } else {
                                            $("#materiales").append(
                                            "<input  type='checkbox' data-idMaterial='" + codigo +
                                            "' name='material_lista[]' value='" + codigo + "' >" +
                                            "<label for='" + codigo + "'>" + nombre + "</label></br>");
                                        }
                                    });
                                }
                            });

                        };
                        function generar_opciones_categoria(valor) {
                                $.ajax({
                                        url: "consulta/obtener_cat/"+valor,
                                        success: function(html){
                                            $.each(html,function(key,value){
                                                var codigo=value.cod_categoria;
                                                var nombre=value.nom_categoria;

                                                $("#categoria").append(" <option value='"+codigo+"'>"+nombre+"</option>");
                                            });

                                        }
                                    });

                                };
                        function generar_opciones_subcategoria(valor) {
                            $.ajax({
                                    url: "consulta/obtener_subcat/"+valor,
                                    success: function(html){
                                        $.each(html,function(key,value){
                                            var codigo=value.cod_subcategoria;
                                            var nombre=value.nom_subcategoria;

                                            $("#subcategoria").append(" <option value='"+codigo+"'>"+nombre+"</option>");
                                            });
                                        }
                                    });
                                };
                            });
                </script>
            </div>
        </div>
    </div>
    <div class="modal fade"  id="modal-create" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" >

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header float-right">
                <h5>Agrear Materiales Normales</h5>
                <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i> </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-3 col-md-offset-3 col-xs-12 ">
                        <label for="total_orden_compra">Orden de compra Nª: </label>

                    </div>
                    <div class="form-group col-md-3 col-md-offset-3 col-xs-12 ">

                        <input type="text" id="orden" name="orden_compra" maxlength="100" class="form-control" disabled>
                    </div>
                    <div class="form-group col-md-2 col-md-offset-2 col-xs-12">
                        <label for="total_orden_compra">Proveedor:</label>
                   </div>
                        <div class="form-group col-md-4 col-md-offset-4 col-xs-12">
                        <input type="text" id="nombre" name="" maxlength="100" class="form-control" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                        <label for="total_orden_compra">Categoría del Producto:</label>
                        <select required="required" id="categoria" name="tipo_categoria" class="custom-select">
                            <option value="" disabled>Categoria</option>

                        </select>
                    </div>
                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                        <label for="total_orden_compra">Subcategoría del Producto:</label>
                        <select required="required" id="subcategoria" name="tipo_subcategoria" class="custom-select">
                        </select>
                    </div>

                    <div class="form-group col-md-4 col-md-offset-4 col-xs-12">
                        <label for="materiales">Materiales:</label>
                        <div id="materiales" style="overflow-y:scroll;height:150px">
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer" style="margin-right: 70%;">
            <button type="submit" id="agregar"
                            class="bttn-unite bttn-md bttn-success" data-dismiss="modal">Agregar</button>
                <button type="button"
                                class="bttn-unite bttn-md bttn-danger "data-dismiss="modal">Cancelar</button>

			</div>
        </div>
    </div>

    </div>

    @push('scripts')
        <script>
            $('#liAlmacen').addClass("treeview active");
            $('#liCategorias').addClass("active");

        </script>
    @endpush
@endsection
