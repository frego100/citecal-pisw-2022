@extends ('layouts.admin')
@section('contenido')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Salida de varios materiales Normales</h2>
                    <div class="clearfix"></div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['url' => 'logistica/kardex/store', 'method' => 'POST', 'autocomplete' => 'off']) !!}

                {{ Form::token() }}
                <div class="row">
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label class="col-md-6 col-form-label text-md-right" for="total_orden_compra">Fecha:</label>
                        <div class="col-md-6">
                            <input type="date" required="required" id="fecha" name="fecha"
                                value="{{ $fechaActual->format('Y-m-d') }}">
                        </div>
                    </div>

                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="name" class="col-md-6 col-form-label text-md-right">Area a entregar:</label>
                        <div class="col-md-6">
                            <select id="area_sal" required name="area" class="custom-select">
                                <option value="" selected disabled>Area</option>
                                @foreach ($areas as $ar)
                                    <option value="{{ $ar->cod_area }}">{{ $ar->descrip_area }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12" id="modal-data">
                        <button class="bttn-unite bttn-md bttn-warning" type="button" data-target="#modal-create"
                            data-toggle="modal" data-pro="$aux" id="modalCreate">Agregar Materiales</button>
                    </div>
                </div>

                <div class="x_content table-responsive">

                    <table id="table_salida_normal" class="display">
                        <thead>
                            <tr>
                                <th>Cod. Material</th>
                                <th>Subcategoria</th>
                                <th>Descripcion del Material</th>
                                <th>Stock Actual</th>
                                <th>Cantidad de Salida</th>
                                <th>Unidad de Compra</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody id="materialSelec">
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="form-group  col-md-3 col-xs-12">
                        <label for="observaciones">Observaciones</label>
                        <input type="hidden" id="tipoSalida" name="tipoSalida" value="0" class="form-control">
                        <input type="text" id="obervaciones" name="observaciones" maxlength="2000" class="form-control">
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <a href="{{ url('logistica/kardex') }}"><button type="button"
                                class="bttn-unite bttn-md bttn-danger ">Cancelar</button></a>
                        <button type="submit" id="guardar" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5"
                            value="GuardarNormal">Guardar</button>
                    </div>
                </div>
                {!! Form::close() !!}

                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 
                <script>
                    $(document).ready(function() {
                        var tabla_normal = $('#table_salida_normal').DataTable( {
                        'columnDefs': [
                        {
                            'targets': 0,
                            'checkboxes': {
                            'selectRow': true
                            }
                        }
                        ],
                        "language": {
                            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                        }
                        });

                        var categoria = <?php echo $categorias; ?>;
                        var material = <?php echo $materiales; ?>;
                        var datatemp = [];
                        var costoTotalMat = 0;
                        var subTotal = 0;
                        var cantidadTot = 0;

                        $("#modalCreate").click(function() {
                            $("#categoria").empty();
                            $("#subcategoria").empty();
                            $("#materiales").empty();
                            $("#subcategoria").append("<option value='' selected disabled>Subcategoria</option>");

                            generar_opciones_categoria();
                            //$("#materiales input[type=checkbox]").prop("checked", false);
                        });

                        $("#categoria").change(function() {
                            let valor = $("#categoria").val();
                            $("#subcategoria").empty();
                            $("#materiales").empty();
                            generar_opciones_subcategoria(valor);
                        });

                        $('#guardar').on('click', function() {
                            $('#ordenCompraN').prop('disabled', false);
                            $('#subTotalval').prop('disabled', false);
                            $('#igvSubtotal').prop('disabled', false);
                            $('#idTotal').prop('disabled', false);
                        });

                        $("#subcategoria").change(function() {
                            let valor = $("#subcategoria").val();
                            generar_opciones_materiales(valor);
                            $("#materiales").empty();
                        });

                        $("#agregar").click(function() {
                            
                            $("#materiales input[type=checkbox]").removeAttr("disabled");
                            var valor = 1;
                            $.each(material, function(key, value) {
                                $('input[name^="material_lista"]').each(function(){
                                    var seleccionado = $(this).is(':checked');
                                    codigo_temporal = $(this).val();
                                    if(seleccionado && value.cod_material == codigo_temporal && !(datatemp.find(element => element == value.cod_material))){
                                        const tr = $(
                                            "<tr>"
                                                +"<td><input name='codigo_material[]' type='hidden'value='" + value.cod_material + "' >" + value.cod_material +"</td>" 
                                                +"<td><input name='costo_con_igv[]' type='hidden' value='" + value.costo_con_igv_material + "' >" + value.nom_subcategoria + "</td>" 
                                                +"<td><input name='costo_sin_igv[]' type='hidden' value='" + value.costo_sin_igv_material + "' >" + value.descrip_material + "</td>" 
                                                +"<td><input name='stock_actual[]' type='hidden'value='" + value.stock_actual + "'>" + value.stock_actual + "</td>"
                                                +"<td><input name='cantidad_salida[]' type='number' step='0.01'  value='0'  min='0' max='"+value.stock_actual+"' id='cantidad_salida" + value.cod_material +"' style='width : 80%;'></td>"
                                                +"<td><input name='unidad_medida[]' type='hidden'value='" + value._compra + "'>"+ value.unidad + "</td>"
                                                +"<td><button class='bttn-unite bttn-md bttn-danger' name='"+value.cod_material+"'><i class='fas fa-trash-alt'></i></button></a></td>"
                                            +"</tr>"
                                        );
                                        tabla_normal.row.add(tr[0]).draw();
                                        datatemp.push(value.cod_material);
                                        console.log(datatemp);
                                    }
                                });
                            });
                        });

                        //ELIMINANDO FILA DE MATERIAL EN ORDEN DE COMPRA
                        $('#table_salida_normal').on('click', 'button', function() {
                            console.log($(this).attr('name'));
                            var indice = datatemp.indexOf($(this).attr('name'));
                            datatemp.splice(indice,1);
                            console.log(datatemp);
                            tabla_normal
                                .row($(this).parents('tr'))
                                .remove()
                                .draw();
                        });

                        function buscarCategoria(id) {
                            var index = -1;
                            var filteredObj = categoria.find(function(item, i) {
                                if (item.cod_categoria == id) {
                                    index = i;
                                    return index;
                                }
                            });
                            return index;
                        }

                        function generar_opciones_materiales(valor) {
                            $.ajax({
                                url: "salida/obtenerMaterial/" + valor,
                                success: function(html) {
                                    $.each(html, function(key, value) {
                                        var codigo = value.cod_material;
                                        var nombre = value.descrip_material;
                                        if (datatemp.find(element => element == value.cod_material)) {
                                            $("#materiales").append(
                                            "<input  type='checkbox' data-idMaterial='" + codigo +
                                            "' name='material_lista[]' value='" + codigo + "' checked disabled>" +
                                            "<label for='" + codigo + "'>" + nombre + "</label></br>");
                                        } else {
                                            $("#materiales").append(
                                            "<input  type='checkbox' data-idMaterial='" + codigo +
                                            "' name='material_lista[]' value='" + codigo + "' >" +
                                            "<label for='" + codigo + "'>" + nombre + "</label></br>");
                                        }
                                    });
                                }
                            });
                        };

                        function generar_opciones_categoria(valor) {
                            $("#categoria").append("<option value='' selected disabled>Categoria</option>");
                            $.each(categoria, function(key, value) {
                                $("#categoria").append(" <option value='" + value.cod_categoria + "'>" + value
                                    .nom_categoria +
                                    "</option>");
                            });
                        };

                        function generar_opciones_subcategoria(valor) {
                            $("#subcategoria").append("<option value='' selected disabled>Subcategoria</option>");
                            $.ajax({
                                url: "salida/obtenerSubcategoria/" + valor,
                                success: function(html) {
                                    $.each(html, function(key, value) {
                                        var codigo = value.cod_subcategoria;
                                        var nombre = value.nom_subcategoria;
                                        $("#subcategoria").append(" <option value='" + codigo + "'>" +
                                            nombre + "</option>");
                                    });
                                }
                            });
                        };
                    });
                </script>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-create" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header float-right">
                    <h5>Agrear Materiales Normales</h5>
                    <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                            <label for="total_orden_compra">Categoría del Producto:</label>
                            <select required="required" id="categoria" name="tipo_categoria" class="custom-select">
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-md-offset-4 col-xs-12 ">
                            <label for="total_orden_compra">Subcategoría del Producto:</label>
                            <select required="required" id="subcategoria" name="tipo_subcategoria" class="custom-select">
                                <option value="" disabled>Subategoria</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4 col-md-offset-4 col-xs-12">
                            <label for="materiales">Buscar:</label>
                            <div id="materiales" style="overflow-y:scroll;height:150px">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-right: 70%;">
                    <button type="submit" id="agregar" class="bttn-unite bttn-md bttn-success"
                        data-dismiss="modal">Agregar</button>
                    <button type="button" data-dismiss="modal"
                            class="bttn-unite bttn-md bttn-danger ">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            $('#liAlmacen').addClass("treeview active");
            $('#liCategorias').addClass("active");
        </script>
    @endpush
@endsection
