@extends ('layouts.admin')
@section('contenido')
    <div class="preloader">

    </div>
    <div>
        <h3 class="font-weight-bold">Listado de Materiales en Almacen</h3>
        <div>
            <div>
                <a href="{{ url('logistica/kardex/salida_normal') }}">
                    <button class="bttn-unite bttn-md bttn-primary float-right mr-sm-5 ">Salida de varios Materiales</button>
                </a>
                <!--
                <a href="" data-target="#modal-reporte" data-toggle="modal">
                    <button class="bttn-unite bttn-md bttn-warning float-right mr-sm-5">Reportes</button>
                </a>

                <a data-target="#modal-reporte-actual" data-toggle="modal">
                    <button class="bttn-unite bttn-md bttn-royal float-right mr-sm-5">Kardex Actual en PDF</button>
                </a>
                
                <a target="_blank" href="/indicadores/kardex">
                    <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Indicadores de Kardex</button>
                </a>
                -->
            </div>
        </div>
    </div>
    @include('logistica.kardex.modal_reporte')
    <div id="tablas">
        <div id="filtros" class="row" style="width:100%;padding:5px">
            <div class="col-md-4 col-xs-12 col-sm-12" style="border:1px solid black">
                <h4>Materias Primas</h4>
                <div class='custom-control custom-checkbox'>
                    <input type='checkbox' class='custom-control-input' id='all_mp'><label class='custom-control-label'
                        for='all_mp'>Todos</label>
                </div>
                <div id="mat" class="" style="height:100px; overflow-y:scroll">
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-12" style="border:1px solid black">
                <h4>Insumos</h4>
                <div class='custom-control custom-checkbox'>
                    <input type='checkbox' class='custom-control-input' id='all_in'><label class='custom-control-label'
                        for='all_in'>Todos</label>
                </div>
                <div id="ins" class="" style="height:100px; overflow-y:scroll">
                </div>
            </div>
            <div class="col-md-4 col-xs-12 col-sm-12" style="border:1px solid black">
                <h4>Suministro</h4>
                <div class='custom-control custom-checkbox'>
                    <input type='checkbox' class='custom-control-input' id='all_sum'><label class='custom-control-label'
                        for='all_sum'>Todos</label>
                </div>
                <div id="sum" class="" style="height:100px; overflow-y:scroll">
                </div>
            </div>
        </div>
        <div id="busq" style="width:100%;display:none">
            <div class="row">
                <div class="col-md-4">
                    <input type="text" id="texto_busq" class="form-control" value=""
                        placeholder="Buscar Descripcion de material...">
                </div>
                <div class="col-md-2">
                    <button type="button" id="boton_busq" class="bttn-unite bttn-md bttn-primary"
                        name="button">Buscar</button>
                </div>
            </div>
        </div>
    </div>
    <button id="ocultar" class="bttn-unite bttn-md bttn-warning float-left mr-sm-5" style="margin:10px">Ocultar</button>

    <br>
    <div id="buscando" align="center" style="display:none; margin:40px;">
        <div class="alert alert-success" role="alert">
            Obteniendo materiales...
        </div>
    </div>
    <br>
    <br>

    @if ($minimo > 0)
        <div style=" margin:40px;">
            <div class="alert alert-warning" role="alert">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" align="center" id="headingThree">
                            <h2 class="mb-0">
                                <button class="btn btn-link col lapsed" type="button" data-toggle="collapse"
                                    data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Posee {{ $minimo }} materiale(s) por debajo del STOCK MINIMO
                                </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                            data-parent="#accordionExample">
                            <div class="card-body text-black">
                                @foreach ($mat_minimo as $mat)
                                    <div class="material_peligro">
                                        <a style="cursor:pointer">{{ $mat }}</a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ($maximo > 0)
        <div style=" margin:40px;">
            <div class="alert alert-danger" role="alert">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" align="center" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                    data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Posee {{ $maximo }} materiale(s) por encima del STOCK MAXIMO
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                            data-parent="#accordionExample">
                            <div class="card-body text-black">
                                @foreach ($mat_maximo as $mat)
                                    <div class="material_peligro">
                                        <a style="cursor:pointer">{{ $mat }}</a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="">
        <span class="badge badge-danger">Stock Alto</span>
        <span class="badge badge-warning">Stock Bajo</span>
    </div>
    @foreach ($valores as $val)
        <?php $cambio_dolar = $val->valor; ?>
    @endforeach
    <div class="x_content table-responsive">
        <table id="kardex_tabla" class="display">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Subcategoria</th>
                    <th>Descripcion del Material</th>
                    <th>Nivel de Stock</th>
                    <th>Stock Actual</th>
                    <th>Unidad de compra</th>
                    <th>Costo Unitario (S/.)</th>
                    <th>Costo Total (S/.)</th>
                    <th>Almacén - Ubicación</th>
                    <th>Salida</th>
                    <th>Devolucion</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <th colspan="7">Valor total de materiales seleccionados(S/.): </th>
                <th colspan="2" id="totales"></th>
                <th>-</th>
                <th>-</th>
            </tfoot>
        </table>
    </div>

    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-reporte-actual">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reporte Actual de Kardex</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <a data-target="#modal-reporte-subcategoria" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-primary">Subcategoria</button>
                        </a>
                    </div>
                    <div class="form-group ">
                        <a data-target="#modal-reporte-almacen" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-primary">Almacen</button>
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-reporte-almacen">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reporte Actual de Kardex: Almacen</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Almacenes:</label>
                        <div class="col-md-6">
                            <select required id="almacen_actual" name="accion" class="custom-select">
                                <option value="" selected disabled>Almacen</option>
                                @foreach ($almacen_normal as $al)
                                    <option value="{{ $al->cod_almacen }}">{{ $al->nom_almacen }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a target="_blank" id="reporte_almacen">
                        <button class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    </a>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
        id="modal-reporte-subcategoria">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reporte Actual de Kardex: Subcategoria</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Subcategoria:</label>
                        <div class="col-md-6">
                            <select required id="subcategoria_actual" name="accion" class="custom-select">
                                <option value="" selected disabled>Subcategoria</option>
                                @foreach ($subcategoria as $ar)
                                    <option value="{{ $ar->cod_subcategoria }}">{{ $ar->nom_subcategoria }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a target="_blank" id="reporte_actual">
                        <button class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    </a>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-reporte-actual">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Reporte Actual de Kardex</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Subcategoria:</label>
                        <div class="col-md-6">
                            <select required id="subcategoria_actual" name="accion" class="custom-select">
                                <option value="" selected disabled>Subcategoria</option>
                                @foreach ($subcategoria as $ar)
                                    <option value="{{ $ar->cod_subcategoria }}">{{ $ar->nom_subcategoria }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a target="_blank" id="reporte_actual">
                        <button class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    </a>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal_salida" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Salida de material</h4>
                </div>
                <div class="modal-body">
                    {{Form::Open(['action' => ['KardexMatController@update', '2'], 'method' => 'PATCH']) }}
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Codigo del Material:</label>
                        <input type="hidden" name="costo_sin_igv" id="costo_sin_igv">
                        <input type="hidden" name="costo_con_igv" id="costo_con_igv">
                        <div class="col-md-6">
                            <input class="cod_mat" type="text" disabled>
                            <input class="cod_mat" type="text" name="cod_mat_buscar" style="display:none">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Descripcion del material:</label>
                        <div class="col-md-6">
                            <textarea id="descrip_material" type="text" value="" required disabled></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Stock actual:</label>

                        <div class="col-md-6">
                            <input id="stock_tot" class="stock_tot" type="text" disabled>
                            <input class="stock_tot" type="text" name="stock_mat" style="display:none">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Unidad de Compra:</label>

                        <div class="col-md-6">
                            <input id="unidad_compra" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Area a entregar:</label>
                        <div class="col-md-6">
                            <select id="area_sal" required name="area" class="custom-select">
                                <option value="" selected disabled>Area</option>
                                @foreach ($area as $ar)
                                    <option value="{{ $ar->cod_area }}">{{ $ar->descrip_area }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Cantidad por entregar:</label>

                        <div class="col-md-6">
                            <input type="number" id="cantidad_entregar" step="0.01" name="cantidad_entregar" min="0"  value="" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="tipoAccion" name="tipoAccion" value="1" class="form-control">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
                {{ Form::Close() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_devolucion" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Devolucion de material</h4>
                </div>
                <div class="modal-body">
                    {{Form::Open(['action' => ['KardexMatController@store', '1'], 'method' => 'POST']) }}
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Codigo del Material:</label>
                        <input type="hidden" name="costo_sin_igv2" id="costo_sin_igv2">
                        <input type="hidden" name="costo_con_igv2" id="costo_con_igv2">
                        <div class="col-md-6">
                            <input class="cod_dev" type="text" disabled>
                            <input class="cod_dev" type="text" name="codmatbuscar" style="display:none">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Subcategoria:</label>
                        <div class="col-md-6">
                            <textarea id="dev_descrip" type="text" value="" disabled></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Descripcion del material:</label>
                        <div class="col-md-6">
                            <textarea id="dev_descrip" type="text" value="" disabled></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Stock actual:</label>

                        <div class="col-md-3">
                            <input class="dev_stock" size="10px" type="text" disabled>
                            <input class="dev_stock" type="text" name="stocktotal" style="display:none">
                        </div>
                        <div class="col-md-3">
                            <input id="dev_unidad" size="10px" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Cantidad Devuelta:</label>

                        <div class="col-md-3">
                            <input type="number" step="0.01"   name="cantidad_devuelta" min="0" value="" required style="width:120%">
                        
                        </div>
                        <div class="col-md-3">
                            <input id="dev_unidad2" size="10px" type="text" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="tipoAccion" name="tipoAccion" value="1" class="form-control">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
                {{ Form::Close() }}
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#collapseThree").collapse('hide')
            $("#collapseTwo").collapse('hide')
            var pb = 1;
            var totales = 0;
            var data = <?php echo $kardex_total; ?>;
            console.log(data);
            var lista = [];
            var cambio_dolares = "{{ $cambio_dolar }}"
            var t = $("#kardex_tabla").DataTable();
            $("#reporte_actual").on("click", function() {
                var cod = $("#subcategoria_actual").val()
                cod += "-" + $("#subcategoria_actual option:selected").text();
                window.open("../pdf/kardex_actual/" + cod)
            })
            $("#reporte_almacen").on("click", function() {
                var cod = $("#almacen_actual").val()
                cod += "-" + $("#almacen_actual option:selected").text();
                window.open("../pdf/reporte_almacen/" + cod)
            })
            $(document).on("keypress", function(event) {
                if (event.which == 13) {
                    $("#boton_busq").trigger("click")
                }
            })
            /*$("#area_sal").on('change',function(){
              let val=$(this).val();
              console.log(val)
              if(val=="47812")
              {
                $("#almacen_sal").show("fast")
                $("#select_almacen_sal").prop('required',true);
              }
              else
              {
                $("#almacen_sal").hide("fast")
                $("#select_almacen_sal").prop('required',false);
              }
            })*/
            $("#boton_busq").on('click', function() {
                var costo = 0;
                var total = 0;
                var totales = 0;
                if (lista.length == 0) {
                    t.rows()
                        .remove()
                        .draw()
                    var texto = $("#texto_busq").val().toUpperCase();
                    if (texto != "") {
                        $("#buscando").toggle("fast");
                        $.each(data, function(key, value) {
                            let valor = value.descrip_material.toUpperCase();
                            console.log(valor);
                            if (valor.indexOf(texto) != -1) {
                                /*if(value.stock_total<= value.stock_minimo)
                                {
                                  var color='<div style="width:20px; height:20px" class="bg-warning text-warning">z</div>'
                                }
                                else {
                                  if(value.stock_total>= value.stock_maximo)
                                  {
                                    var color='<div style="width:20px; height:20px" class="bg-danger text-danger">z</div>'
                                  }
                                  else {
                                    var color='<div style="width:20px; height:20px" class="text-white">a</div>'
                                  }
                                }*/

                                totales = totales + total;
                                t.row.add([
                                    //color,
                                    value.cod_material,
                                    value.nom_subcategoria,
                                    value.descrip_material,
                                    "<p>Min: " + value.stock_minimo + "</p><p>Max: " + value
                                    .stock_maximo + "</p>",
                                    value.stock_actual,
                                    value.descrip_unidad_medida,
                                    costo,
                                    total.toFixed(2),
                                    "<p>" + value.nom_almacen + " - " + value
                                    .lugar_almacenaje + "</p>",
                                    '<a class="salida" id="' + value.cod_material +
                                    '"><button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-sign-out-alt"></i></button></a>',
                                    '<a class="devolucion" id="' + value.cod_material +
                                    '"><button class="bttn-unite bttn-md bttn-primary  "><i class="fas fa-sign-in-alt"></i></button></a>'
                                ]).draw();
                                $("#kardex_tabla").on('click', 'a.salida', function() {
                                    var id = $(this).attr("id");
                                    var index = buscar(id);
                                    modal_salida(index);
                                });
                                $("#kardex_tabla").on('click', 'a.devolucion', function() {
                                    var id = $(this).attr("id");
                                    var index = buscar(id);
                                    modal_devolucion(index);
                                });
                                let igv = totales * 1.18
                                $("#totales").html("S/. " + totales.toFixed(2))
                            }

                        })
                        $("#buscando").toggle("fast");
                    } else {
                        $("#totales").html("S/. " + 0)
                    }
                } else {
                    alert("Deseleccionar busqueda por subcategoria")
                    $("#texto_busq").val("");
                }
            })

            setInterval(function() {
                cargar_datos()
            }, 30000)

            function cargar_datos() {
                $("#actualizar_datos").toggle("slow");
                $.ajax({
                    url: "kardex/obtener",
                    success: function(html) {
                        var flag = 0;
                        if (data.length != html.length) {
                            flag = 1;
                            data = html;
                        } else {
                            for (var i = 0; i < html.length; i++) {
                                for (var j = 0; j < data.length; j++) {
                                    if ((html[i].cod_material == data[j].cod_material) && (html[i]
                                            .stock_actual != data[j].stock_actual)) {
                                        flag = 1;
                                    }
                                }
                            }
                        }
                        if (flag != 0) {
                            data = html;
                            actualizar_datos();
                        }
                    }
                });
            }

            function actualizar_datos() {

                var arr = $('[name="checks[]"]:checked').map(function() {
                    return this.id;
                }).get();
                let tmp = arr;
                if (arr.length >= 0) {
                    for (var i = 0; i < arr.length; i++) {
                        $("#" + arr).prop("checked", false);
                        $("#" + arr).trigger("change");
                        $("#" + arr).prop("checked", true);
                        $("#" + arr).trigger("change");
                    }
                } else {
                    $("#boton_busq").trigger("click");
                }
                $("#actualizar_datos").toggle("slow");
            }

            @foreach ($subcategoria as $sub)
                @if ($sub->cod_categoria == 634)
                    $("#sum").append("<div class='custom-control custom-checkbox'>"
                        +"<input name='checks[]' type='checkbox' class='custom-control-input c_su' id='{{ $sub->cod_subcategoria }}'>"
                        +"<label class='custom-control-label' for='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</label>" 
                        +"</div>") 
                @endif
                @if ($sub->cod_categoria == 306)
                    $("#ins").append("<div class='custom-control custom-checkbox'>"
                        +"<input name='checks[]' type='checkbox' class='custom-control-input c_in' id='{{ $sub->cod_subcategoria }}'>"
                        +"<label class='custom-control-label'for='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</label>"
                        +"</div>")
                @endif
                @if ($sub->cod_categoria == 969)
                    $("#mat").append("<div class='custom-control custom-checkbox'>"
                        +"<input name='checks[]' type='checkbox' class='custom-control-input c_mp' id='{{ $sub->cod_subcategoria }}'>"
                        +"<label class='custom-control-label' for='{{ $sub->cod_subcategoria }}'>{{ $sub->nom_subcategoria }}</label>"
                        +"</div>")
                @endif
                $("#{{ $sub->cod_subcategoria }}").off('change');
            
            
                $("#{{ $sub->cod_subcategoria }}").on('change',function(){
                console.log("Entro A subcategoria");
                if(pb!=0)
                {
                $("#buscando").toggle("fast");
                }
                if($("#texto_busq").val()=="")
                {
                if(lista.length==0)
                {
                t.rows()
                .remove()
                .draw()
                totales=0;
                }
                var texto=$(this).attr('id');
                if($(this).is(":checked"))
                {
                var costo
                var total = 10;
                $.each( data, function( key, value ) {
                /*if(value.stock_total<= value.stock_minimo) { var
                    color='<div style="width:20px; height:20px" class="bg-warning text-warning">z</div>' } else {
                    if(value.stock_total>= value.stock_maximo)
                    {
                    var color='<div style="width:20px; height:20px" class="bg-danger text-danger">z</div>'
                    }
                    else {
                    var color='<div style="width:20px; height:20px" class="text-white">a</div>'
                    }
                    }*/
                    console.log(texto +" --- "+ value.cod_subcategoria);
                    if(value.cod_subcategoria==texto)
                    {
            
                    totales=totales+value.costo_total_con_igv;
                    lista.push({cod:value.cod_material,cod_sub:value.cod_subcategoria,cod_total:total});
            
            
                    if(value.stock_minimo == null){
                    t.row.add( [
                    //color,
                    value.cod_material,
                    value.nom_subcategoria,
                    value.descrip_material,
                    "-",
                    value.stock_actual,
                    value.descrip_unidad_compra,
                    "S/. "+value.costo_con_igv_material.toFixed(2),
                    "S/. "+value.costo_total_con_igv,
                    value.nom_almacen,
                    '<a class="salida" id="'+value.cod_material+'"><button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-sign-out-alt"></i></button></a>',
                    '<a class="devolucion" id="'+value.cod_material+'"><button class="bttn-unite bttn-md bttn-primary  "><i class="fas fa-sign-in-alt"></i></button></a>'
                    ] ).draw();
                    }else{
                    t.row.add( [
                    //color,
                    value.cod_material,
                    value.nom_subcategoria,
                    value.descrip_material,
                    value.stock_minimo+"-"+value.stock_maximo,
                    value.stock_actual,
                    value.descrip_unidad_compra,
                    "S/. "+value.costo_con_igv_material.toFixed(2),
                    "S/. "+value.costo_total_con_igv,
                    value.nom_almacen,
                    '<a class="salida" id="'+value.cod_material+'"><button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-sign-out-alt"></i></button></a>',
                    '<a class="devolucion" id="'+value.cod_material+'"><button class="bttn-unite bttn-md bttn-primary  "><i class="fas fa-sign-in-alt"></i></button></a>'
                    ] ).draw();
                    }
                    $("#kardex_tabla").on('click','a.salida',function(){
                    var id = $(this).attr("id");
                    var index=buscar(id);
                    modal_salida(index);
                    });
                    $("#kardex_tabla").on('click','a.devolucion',function(){
                    var id = $(this).attr("id");
                    var index=buscar(id);
                    modal_devolucion(index);
                    });
            
                    }
                    });
                    let igv=totales*1.18
                    $("#totales").html("S/. "+totales.toFixed(2))
                    }
                    else {
                        $.each( lista, function( key, value ) {
                        if(value.cod_sub==texto)
                        {
                        totales=totales-value.cod_total
                        t.rows(function(idx,data,node){
                            return data[0]==value.cod;
                        })
                        .remove()
                        .draw()
                        lista=$.grep(lista,function(value){
                
                        return value.cod_sub!=texto
                        })
                
                        }
                        });
                        let igv=totales*1.18
                        $("#totales").html("S/. "+totales.toFixed(2))
                    }
                    }
                    else {
                    $(this).prop("checked",false);
                    alert("Desactive busqueda por descripcion")
                    }
            
                    if(pb!=0)
                    {
                    $("#buscando").toggle("fast");
                    }
                });
            @endforeach
            $("#all_mp").on("change", function() {
                pb = 0;
                $("#buscando").toggle("fast");
                var mp = $(".c_mp");
                if ($(this).prop("checked") == true) {
                    mp.prop("checked", true);
                    mp.trigger('change');
                } else {
                    mp.prop("checked", false);
                    mp.trigger('change');
                }
                $("#buscando").toggle("fast");
                pb = 1;
            });
            $("#all_in").on("change", function() {
                pb = 0;
                $("#buscando").toggle("fast");
                var ins = $(".c_in");
                if ($(this).prop("checked") == true) {
                    ins.prop("checked", true);
                    ins.trigger('change');
                } else {
                    ins.prop("checked", false);
                    ins.trigger('change');
                }
                $("#buscando").toggle("fast");
                pb = 1;
            });
            $("#all_sum").on("change", function() {
                pb = 0;
                $("#buscando").toggle("fast");
                var sum = $(".c_su");
                if ($(this).prop("checked") == true) {
                    sum.prop("checked", true);
                    sum.trigger('change');
                } else {
                    sum.prop("checked", false);
                    sum.trigger('change');
                }
                pb = 1;
                $("#buscando").toggle("fast");
            });
            $("#ocultar").on('click', function() {
                if ($(this).text() == "Ocultar") {
                    $(this).text("Mostrar")
                } else {
                    $(this).text("Ocultar")
                }
                $("#tablas").toggle("fast");
            })
            $("#cambiar_busq").on('click', function() {
                if ($(this).text() == "Busqueda por subcategoria") {
                    $(this).text("Busqueda por descripcion")
                } else {
                    $(this).text("Busqueda por subcategoria")

                }
                $("#filtros").toggle("fast");
                $("#busq").toggle("fast");
            })

            function buscar(id) {
                var index = -1;
                var filteredObj = data.find(function(item, i) {
                    if (item.cod_material == id) {
                        index = i;
                        return index;
                    }
                });
                return index;
            }

            function modal_salida(index) {
                console.log(data[index]);
                $(".cod_mat").val(data[index].cod_material)
                $("#descrip_material").val(data[index].descrip_material)
                $("#stock_tot").val(data[index].stock_actual)
                $("#cantidad_entregar").prop("max",data[index].stock_actual);
                $("#unidad_compra").val(data[index].descrip_unidad_compra)
                $("#costo_sin_igv").val(data[index].costo_sin_igv_material)
                $("#costo_con_igv").val(data[index].costo_con_igv_material)
                $("#modal_salida").modal({
                    show: true
                })
            }

            function modal_devolucion(index) {
                $(".cod_dev").val(data[index].cod_material);
                $("#dev_descrip").val(data[index].descrip_material);
                $(".dev_stock").val(data[index].stock_actual);
                $("#dev_unidad").val(data[index].descrip_unidad_compra);
                $("#dev_unidad2").val(data[index].descrip_unidad_compra);
                $("#costo_sin_igv2").val(data[index].costo_sin_igv_material)
                $("#costo_con_igv2").val(data[index].costo_con_igv_material)
                $("#modal_devolucion").modal({
                    show: true
                })
            }
            $(".material_peligro").on("click", function() {
                var material = $.trim($(this).text())
                $("#texto_busq").val(material)
                $("#boton_busq").trigger('click')
            })
        });
    </script>
@endsection
