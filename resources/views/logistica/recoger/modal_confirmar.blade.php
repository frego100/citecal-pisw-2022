<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-reporte-cancelar">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desea Cerrar la Orden {{$var}}?</h4>
			</div>
            
			<div class="modal-body">
                <input type="text" style="display:none" id='cancelar_orden' value='{{$var}}'>
                <h5>Si acepta no podra realizar ningun cambio mas.</h5>
			</div>

			<div class="modal-footer">
				<a >
					<button id="cancelar_but"  class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				</a>

				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>

</div>
