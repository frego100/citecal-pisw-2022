<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="agregar">
{!!Form::open(array('url'=>'logistica/nueva_importacion','method'=>'POST','autocomplete'=>'off'))!!}
      {{Form::token()}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Agregar nuevo Gasto</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="detalle" value="{{$codigo_detalle}}">
        <input type="text" style="display:none" name="descrip" value="{{$nombre}}">
        <div class="form-group  ">
          <label for="total_orden_compra">Descripcion de gasto:</label>
           <textarea type="text" class="form-control" required name="descripcion" style="width:100%" maxlength="300"></textarea>
        </div>
        <div class="row">
          <div class="form-group  col-md-6">
            <label for="">Tipo de moneda</label>
            <select name="moneda" class="custom-select" required>
              <option value="" disabled>Escoja uno</option>
              <option value="0">Soles</option>
              <option value="1">Dolares</option>
            </select>
          </div>
          <div class="form-group  col-md-6">
            <label for="total_orden_compra">Importe:</label>
             <input type="text" class="form-control" required name="importe" onkeypress="return isNumberKey(event)" >
          </div>
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
