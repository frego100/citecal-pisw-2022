<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
    id="modal-edit">

    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">

            {{ Form::Open(['action' => ['ProveedorController@update', $pro->RUC_proveedor], 'method' => 'patch']) }}
            <div class="modal-header">
                <h4 class="modal-title">Editar Proveedor</h4>
            </div>
            <div class="modal-body">
                <input type="text" style="display:none" name="ruc_editar" id="ruc_editar">
                <div class="row">
                    <div class="form-group  col-md-6 col-xs-6">
                        <label for="total_orden_compra">Categoria: {{$pro->RUC_proveedor}}</label>
                        <select required="required" id="categoria" name="cod_categoria" class="custom-select" >
							<option value=""  disabled>Categoria</option>
							@foreach($categoria as $sub)
								<option value="{{$sub->cod_categoria}}" selected>{{$sub->nom_categoria}}</option>
							@endforeach
						</select>
                    </div>

                    <div class="form-group  col-md-6 col-xs-6" id="probando">
                        <label for="materiales">Materiales Principales:</label>
                        <div id="materiales" style="overflow-y:scroll;height:150px">
                        </div>
                        <input type="hidden" name="subcat[]" id="subcat">
                    </div>

                </div>
                <div class="row">
                    <div class="form-group  col-md-12   col-xs-12">
                        <label for="total_orden_compra">Razon Social:</label>
                        <input maxlength="50" required="required" type="text"
                            name="nomb_proveedor" id="nomb_proveedor" required="required" class="form-control ">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Direccion 1:</label>
                        <textarea class="form-control" id="dir_proveedor" maxlength="100" rows="3" name="dir_proveedor"
                            required="required"></textarea>
                    </div>
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Direccion 2 (Tienda):</label>
                        <textarea class="form-control" maxlength="100" rows="3"
                            name="dir_tienda" id="dir_tienda"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Celular del proveedor:</label>
                        <input onkeypress="return isNumberKey(event)" maxlength="9"
                            type="text" name="tele_proveedor" id="tele_proveedor" required="required" class="form-control ">
                    </div>
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Correo:</label>
                        <input type="email" name="correo_proveedor" id="correo_proveedor"
                            maxlength="50" class="form-control ">
                    </div>

                </div>
                <div class="row">
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Nombre de contacto 1:</label>
                        <input type="text" name="nom_contacto" id="nom_contacto" maxlength="100"
                            class="form-control ">
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Telefono de contacto 1:</label>
                        <input onkeypress="return isNumberKey(event)" 
                            maxlength="9" type="text" name="tel_contacto" id="tel_contacto" class="form-control ">
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Correo de contacto 1:</label>
                        <input type="email" name="correo_contacto" id="correo_contacto" maxlength="50"
                            class="form-control ">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Nombre de contacto 2:</label>
                        <input type="text" name="nom_contacto2" id="nom_contacto2" maxlength="100"
                            class="form-control ">
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Telefono de contacto 2:</label>
                        <input onkeypress="return isNumberKey(event)"
                            maxlength="9" type="text" name="tel_contacto2" id="tel_contacto2" class="form-control ">
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Correo de contacto 2:</label>
                        <input type="email" name="correo_contacto2" id="correo_contacto2"
                            maxlength="50" class="form-control ">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="guardar" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
            {!! Form::Close() !!}
        </div>
    </div>
    
</div>

	
