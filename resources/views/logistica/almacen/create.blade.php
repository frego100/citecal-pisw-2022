@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Registrar Almacen</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'logistica/almacen','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
                
                <div class="row">
                      <div class="form-group  col-md-4">
                        <label for="">Categorias</label>
                          <select id="categoria" required="required" name="tipo_categoria" class="custom-select">
                          <option value="" selected disabled >Categorias</option>  
                            @foreach ($categorias as $item )
                              <option value="{{$item->cod_categoria}}" >{{$item->nom_categoria}}</option>
                            @endforeach
                  
                          </select>
                        </div>
                      </div>
                    <div class="row">
                      <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Nombre de Almacen:</label>
                         <input type="text" id="categoria" name="nombre" required="required" maxlength="100" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-4">
                        <label for="">Encargado de Almacen</label>
                        <select required="required" name="trabajador" class="custom-select">
                          <option value="" selected disabled >Trabajador Encargado</option>
                          @foreach ($trabajadores as $trab)
                            <option value="{{$trab->DNI_trabajador}}" >{{$trab->apellido_paterno." ".$trab->apellido_materno.", ".$trab->nombres}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                              
                          <div class="ln_solid"></div>
                      <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                            <a href="{{ url('logistica/almacen') }}"><button type="button"
                                class="bttn-unite bttn-md bttn-danger ">Cancelar</button></a>
                              <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                            </div>
                          </div>
          {!!Form::close()!!}
        </div>
      </div>
    </div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
