@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">
</div>
<div>
<h3 class="font-weight-bold">Listado de articulos del modelo {{$var}}
  <a href="{!! route('nuevo_articulo',['var'=>$var.'+'.$serie.'+'.$codigo]) !!}" >
    <button class="bttn-unite bttn-md bttn-primary float-right mr-sm-5">Nuevo Articulo</button>
    </a>
  </h3>
  <span class="badge badge-warning">Consumo desactualizado</span>
</div>

<div class="x_content table-responsive">
  <table id="example" class="display">
    <thead>
      <tr>
        <th></th>
        <th>Imagen</th>
        <th>Codigo del Modelo</th>
        <th>Serie</th>
        <th>Nombre del Modelo</th>
        <th>Descripcion del Modelo</th>
        <th>Costo de Material <br> Directos</th>
        <th>Costo de Mano de <br> Obra</th>
        <th>Total Costo Directo <br> de Fabricacion</th>
        <th>Cabecera</th>
        <th>Materiales Directos</th>
        <th>Mano de Obra Directo</th>
      </tr>
    </thead>
    <tbody>
      @foreach($modelos as $mat)
      <tr>
        <td>
          <div class="row">
            @if($mat->estado==9)
              <div class="col-md-6 bg-warning" style="height:20px; width:10px">
              </div>
            @endif
            @if($mat->estado==8)
              <div class="col-md-6 bg-warning" style="height:20px; width:10px">
              </div>
              <div class="col-md-6">
                <br>
              </div>
              <div class="col-md-6 bg-info"  style="height:20px; width:10px">
              </div>
            @endif
            @if($mat->estado==7)
              <div class="col-md-6 bg-info"  style="height:20px; width:10px">
              </div>
            @endif
          </div>
        </td>
        <td><a href="" data-target="#modal-imagen-{{$mat->cod_modelo}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-primary">{{Html::image('photo/modelos/'.$mat->RUC_empresa.'/'.$mat->imagen,$mat->nombre,array( 'width' => 70, 'height' => 70 ))}}</button></a></td>
        <td>{{$mat->cod_modelo}}</td>
        <td>{{$mat->nombre_serie}}</td>
        <td>{{$mat->nombre}}</td>
        <td>{{$mat->descripcion}}</td>
        <td>{{$mat->total_materiales}}</td>
        <td>{{$mat->total_obra}}</td>
        <td>{{$mat->total_materiales+$mat->total_obra}}</td>
        <td>
          <a href="{!! route('norma_tecnica',['var'=>$mat->cod_modelo.'+'.$mat->codigo_serie.'+'.$mat->codigo]) !!}" >
            <button class="bttn-unite bttn-md bttn-primary "><i class="fas fa-newspaper"></i></button>
            </a>
        </td>
        <td>
          <a href="{!! route('costos_directos',['var'=>$mat->cod_modelo.'+'.$mat->codigo]) !!}" >
            <button class="bttn-unite bttn-md bttn-primary "><i class="fas fa-shopping-basket"></i></button></a>
        </td>
        <td>
          <a href="{!! route('mano_directo',['var'=>$mat->cod_modelo.'+'.$mat->codigo]) !!}" >
            <button class="bttn-unite bttn-md bttn-primary "><i class="fas fa-user-alt"></i></button></a>
        </td>
      </tr>
      @include('costos.modelo_articulo.modal_imagen')
      @endforeach
    </tbody>
  </table>
  <a href="{!! url('costo/vermodelos') !!}" >
    <button class="bttn-unite bttn-md bttn-danger ">Atras</button></a>
</div>
@endsection
