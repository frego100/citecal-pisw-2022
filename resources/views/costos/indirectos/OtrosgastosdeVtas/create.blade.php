@extends ('layouts.admin')
@section ('contenido')
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Nuevo Gasto de Representacion</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'costos_indirectos/OtrosgastosdeVtas','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
                 <div class="row">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                   
                    <h6 class="my-3">Area</h6>
                     <select required='required' name="area" "  class="custom-select">
                        <option value="" selected disabled>Area</option>
                        @foreach ($area as $cat)
                          <option value="{{$cat->descrip_area}}" >{{$cat->descrip_area}}</option>
                        @endforeach
                      </select>

                   
                    <h6 class="my-3">Descripcion:</h6>
                    <input type="text" id="" name="descripcion" maxlength="70" required="required" class="form-control ">

                </div>

                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Gasto:</h6>
                        <input type="text" id="gasto4" name="gasto" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Cantidad anual:</h6>
                        <input type="text" id="cantidad_anual4" name="cantidad_anual" maxlength="70" required="required" class="form-control ">

               
                    <h6 class="my-3">Gasto Mensual:</h6>
                        <input readonly="" type="text" id="gasto_mensual4" name="gasto_mensual" maxlength="70" required="required" class="form-control ">

                </div>
            </div>
            <div class="form-group my-5">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-slant bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <a href=".">  
                    <button type="" class="bttn-slant bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</input>
                    </a>
                </div>
            </div>
          {!!Form::close()!!}
          
        </div>
      </div>
    </div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liareas').addClass("active");
</script>
@endpush
@endsection
