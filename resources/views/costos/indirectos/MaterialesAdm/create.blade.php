@extends ('layouts.admin')
@section ('contenido')
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Nuevo Sueldo Administrativo</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'costos_indirectos/MaterialesAdm','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
                 <div class="row">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Area:</h6>
                    <select required='required' name="area"  class="custom-select">
                        @foreach ($area as $cat)
                          <option value="{{$cat->descrip_area}}" >{{$cat->descrip_area}}</option>
                        @endforeach
                    </select>
                    <h6 class="my-3">Descripcion de gasto</h6>
                     <select required='required' name="descripcion" "  class="custom-select">
                        <option value="" selected disabled>Descripcion</option>
                        @foreach ($descripcion as $cat)
                          <option value="{{$cat->nom_subcategoria}}" >{{$cat->nom_subcategoria}}</option>
                        @endforeach
                      </select>

                    <h6 class="my-3">Unidad:</h6>
                    <select required='required' name="unidad" "  class="custom-select">
                        <option value="" selected disabled>Unidad</option>
                        @foreach ($unidad_medida as $cat)
                          <option value="{{$cat->unidad_medida}}" >{{$cat->unidad_medida}}</option>
                        @endforeach
                      </select>

                    <h6 class="my-3">Valor Unitario:</h6>
                    <input type="text" id="valor_unitario2" name="valor_unitario" maxlength="70" required="required" class="form-control ">

                </div>

                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Consumo:</h6>
                        <input type="text" id="consumo2" name="consumo" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Meses de duracion</h6>
                        <input type="text" id="meses_duracion2" name="meses_duracion" maxlength="70" required="required" class="form-control ">

               
                    <h6 class="my-3">Gasto Mensual:</h6>
                        <input readonly type="text" id="gasto_mensual2" name="gasto_mensual" maxlength="70" required="required" class="form-control ">

                </div>
            </div>
            <div class="form-group my-5">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-slant bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <a href=".">  
                    <button type="" class="bttn-slant bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</input>
                    </a>
                </div>
            </div>
          {!!Form::close()!!}
          
        </div>
      </div>
    </div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liareas').addClass("active");
</script>
@endpush
@endsection
