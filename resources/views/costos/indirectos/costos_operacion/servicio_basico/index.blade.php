<div class="pb-3">
    <h3 class="font-weight-bold">Servicios Básicos
        <a href="" data-target="#modal-create-servicio" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Servicio Básico</button></a>
        <a href="#">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Reporte</button></a>

    </h3>
</div>
<div class="x_content table-responsive pt-3">
    <table id="servicio_basico" class="display">
        <thead>
            <tr>
                <th>N°</th>
                <th>Proceso/Área</th>
                <th>Descripción del Servicio</th>
                <th>Costo Mensual</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    @include('costos.indirectos.costos_operacion.servicio_basico.modalcrear')

</div>
