<div class="pb-3">
    <h3 class="font-weight-bold">Material Directo
        <a href="" data-target="#modal-create-material" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Agregar M. Indirectos y Suministros</button></a>
        <a href="#">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Reporte</button></a>

    </h3>
</div>
<div class="x_content table-responsive pt-3">
    <table id="material_directo" class="display">
        <thead>
            <tr>
                <th>N°</th>
                <th>Proceso/Área</th>
                <th>Tipo de Material</th>
                <th>Unidad de Compra</th>
                <th>Costo Unitario</th>
                <th>Consumo</th>
                <th>Meses de Duración</th>
                <th>Costo Mensual</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    @include('costos.indirectos.costos_operacion.material_directo.modalcrear')

</div>
