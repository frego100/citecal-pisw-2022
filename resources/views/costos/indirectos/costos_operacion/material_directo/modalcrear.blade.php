<div id="modal-create-material" class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1">
    <form method="POST" action="{{ url('configuracion/merma/create') }}">
        <!-- CSRF Token -->
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Material Indirecto y Suministro</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <label for="tipo_merma">Proceso:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Tipo de Material</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 ">
                            <label for="total_orden_compra">Descripcion de Material:</label>
                            <input type="text" class="form-control" maxlength="100" name="porcentaje_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <label for="tipo_merma">Unidad de Compra:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Costo Unitario</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="tipo_merma">Consumo:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Meses de Duración</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Costo Mensual</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Guardar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
