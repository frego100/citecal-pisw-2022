<div id="modal-create-mano" class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1">
    <form method="POST" action="{{ url('configuracion/merma/create') }}">
        <!-- CSRF Token -->
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nueva Mano de Obra Directa</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12 ">
                            <label for="tipo_merma">Proceso:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 ">
                            <label for="total_orden_compra">Descripcion del Puesto:</label>
                            <input type="text" class="form-control" maxlength="100" name="porcentaje_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 ">
                            <label for="tipo_merma">Tipo de Sueldo:</label>
                            <div class="form-group row">
                                <div class="form-group col-md-6 ">
                                    <label><input type="checkbox" id="cbox1" value="first_checkbox"> Planilla</label>
                                </div>
                                <div class="form-group col-md-6 ">
                                    <label><input type="checkbox" id="cbox1" value="first_checkbox"> Otros Sueldos</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Sueldo Mensual Planilla:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Beneficio Social:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Otros Sueldos:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Costo Mensual:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
