<div class="pb-3">
    <h3 class="font-weight-bold">Servicios Básicos

    </h3>
</div>
<div class="x_content table-responsive pt-3">
    <h5 class="pb-3"> Desarrollo del Producto - Materiales
        <a href="" data-target="#modal-create-desarrollo-material" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Material</button></a>
    </h5>
    <table id="desarrollo_producto-material" class="display">
        <thead>
            <tr>
                <th>N°</th>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>N° de Prototipos al mes </th>
                <th>Costo Total</th>
                <th>Importe por Prototipo</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr class="my-2">
    <h5 class="pb-3"> Desarrollo del Producto - Procesos Productivos
        <a href="" data-target="#modal-create-desarrollo-proceso" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Proceso Productivo</button></a>
    </h5>
    <table id="desarrollo_producto-procesos" class="display">
        <thead>
            <tr>
                <th>N°</th>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>N° de Prototipos al mes </th>
                <th>Costo Total</th>
                <th>Importe por Prototipo</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr class="my-2">
    <h5 class="pb-3">Desarrollo del Producto - Servicios
        <a href="" data-target="#modal-create-desarrollo-servicio" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Servicio</button></a>
    </h5>
    <table id="desarrollo_producto-servicios" class="display">
        <thead>
            <tr>
                <th>N°</th>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>N° de Prototipos al mes </th>
                <th>Costo Total</th>
                <th>Importe por Prototipo</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr class="my-2">
    <h5 class="pb-3">Desarrollo del Producto - Hormas
        <a href="" data-target="#modal-create-desarrollo-horma" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Operación</button></a>
    </h5>
    <table id="desarrollo_producto-hormas" class="display">
        <thead>
            <tr>
                <th>N°</th>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>N° de Prototipos al mes </th>
                <th>Costo Total</th>
                <th>Importe por Prototipo</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <hr class="my-2">
    <h5 class="pb-3">Desarrollo del Producto - Troqueles
        <a href="" data-target="#modal-create-desarrollo-troquel" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Operación</button></a>
    </h5>
    <table id="desarrollo_producto-troqueles" class="display">
        <thead>
            <tr>
                <th>N°</th>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>N° de Prototipos al mes </th>
                <th>Costo Total</th>
                <th>Importe por Prototipo</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcrearhormas')
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcrearmaterial')
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcrearproceso')
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcrearservicio')
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcreartroqueles')
</div>
