@extends ('layouts.admin')
@section('contenido')

<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto de Ventas y Masketing</h1>                    
                    <div class="list-group d-inline">
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 active ml-4" id="list-home-list" data-toggle="list" href="#list-Prototipo" role="tab" aria-controls="home">Sueldo de Ventas y Marketing</a>
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Hormas" role="tab" aria-controls="profile">Otros Gastos de ventas</a>
                    </div>
                    <div class="clearfix"></div>
            </div>


            <div class="row">  
            <div class="col-12">
                <div class="tab-content mt-4" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list"> 
                <h2 class="d-inline ">Sueldo de Ventas y Marketing</h2>
                <button type="submit" class="bttn-slant bttn-md bttn-success" target="_blank" id="sub"><i class="fas fa-plus"></i></button>
                <button type="submit" class="bttn-slant bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Sueldo</button>
                <div class="x_content table-responsive">
                    <table id="table_mp" class="table stacktable">
                    <thead>
                        <tr>
                        <th>Area</th>
                        <th>Descripcion del Puesto</th>
                        <th>DSueldo Mensual de Plantilla</th>
                        <th>Beneficios Sociales</th>
                        <th>Gasto Total</th>
                        <th>Importe po Prototipo</th>
                        </tr>
                    </thead>
                    
                        <tbody>
                            <tr>
                                <td>
                                    <a href="" >
                                    <button class="bttn-slant bttn-md bttn-warning "><i class="fas fa-search"></i></button></a>
                                </td>
                                <td>
                                    <a href="" >
                                    <button class="bttn-slant bttn-md bttn-primary"><i class="fas fa-search"></i></button></a>
                                </td>
                                <td>
                                    <a href="" >
                                    <button class="bttn-slant bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                                </td>
                            </tr>
                        </tbody>                           
                    </table>
                    <h4>Costo Total de Sueldos Administrativos</h4>
                </div>              
                </div>


                <div class="tab-pane fade" id="list-Hormas" role="tabpanel" aria-labelledby="list-profile-list">
                <h2 class="d-inline ">Otros Gastos de ventas</h2>
                <button type="submit" class="bttn-slant bttn-md bttn-success" target="_blank" id="sub"><i class="fas fa-plus"></i></button>
                <button type="submit" class="bttn-slant bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto de Ventas</button>

                <div class="x_content table-responsive">
                    <table id="table_mp" class="table stacktable">
                    <thead>
                        <tr>
                        <th>Area</th>
                        <th>Descripcion del Gasto</th>
                        <th>Gastos</th>
                        <th>Cantidad Anual</th>
                        <th>Gasto Mensual</th>
                        </tr>
                    </thead>
                    
                        <tbody>
                            <tr>
                                <td>
                                    <a href="" >
                                    <button class="bttn-slant bttn-md bttn-warning "><i class="fas fa-search"></i></button></a>
                                </td>
                                <td>
                                    <a href="" >
                                    <button class="bttn-slant bttn-md bttn-primary"><i class="fas fa-search"></i></button></a>
                                </td>
                                <td>
                                    <a href="" >
                                    <button class="bttn-slant bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                                </td>
                            </tr>
                        </tbody>                           
                    </table>
                    <h4>Costo Total de Gastos Representativos</h4> 
                </div>                       
                </div>  

        </div>
    </div>



@endsection