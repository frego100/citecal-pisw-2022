@extends ('layouts.admin')
@section('contenido')
<div class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title mb-3">
                    <h2 class="font-weight-bold d-inline ">Sueldo Administrativo</h2>
                    <div class="list-group d-inline">
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 active ml-4" id="list-home-list" data-toggle="list" href="#list-Prototipo" role="tab" aria-controls="home">Sueldo Administrativo</a>
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Hormas" role="tab" aria-controls="profile">Material Administrativo</a>
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Troqueles" role="tab" aria-controls="profile">Gastos Representacion</a>
                    </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<div class="col-12">
    
  </div>



<div class="row">
  
  <div class="col-12">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list">      
            <h2 class="mt-3">Nuevo Sueldo Administrativo</h2>
            <div class="row">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Area:</h6>
                    <select required='required' id="categoria"  class="custom-select">
                        <option value="" selected disabled>Administracion</option>
                        <option value="" ></option>
                    </select>

                    <h6 class="my-3">Descripcion del Puesto:</h6>
                    <select required='required' id="categoria"  class="custom-select">
                        <option value="" selected disabled>Gerente General</option>
                        <option value="" ></option>
                    </select>

                    <h6 class="my-3">DNI de Trabajador:</h6>
                    <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Nombre del Trabajador:</h6>
                    <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                </div>

                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Sueldo Mensual en Planilla:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Beneficios Sociales:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Otros Sueldos Mensuales:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Gastos Mensuales:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                </div>
            </div>
            <div class="form-group my-5">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-slant bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <button type="submit" class="bttn-slant bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button>
                </div>
            </div>

      </div>
      <div class="tab-pane fade" id="list-Hormas" role="tabpanel" aria-labelledby="list-profile-list">
            <h2 class="mt-3">Nuevo Material Administrativo</h2>
            <div class="row">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Area:</h6>
                    <select required='required' id="categoria"  class="custom-select">
                        <option value="" selected disabled>Administracion</option>
                        <option value="" ></option>
                    </select>

                    <h6 class="my-3">Tipo de Gasto:</h6>
                    <select required='required' id="categoria"  class="custom-select">
                        <option value="" selected disabled>Materiales Administrativo</option>
                        <option value="" ></option>
                    </select>

                    <h6 class="my-3">Descripcion del Gasto:</h6>
                    <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                </div>

                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Unidad de Compra:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Valor Unitario:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Consumo:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Meses de duracion:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Gasto Mensual:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                </div>
            </div>
            <div class="form-group my-5">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-slant bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <button type="submit" class="bttn-slant bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button>
                </div>
            </div>
      </div>

      <div class="tab-pane fade" id="list-Troqueles" role="tabpanel" aria-labelledby="list-profile-list">
            <h2 class="mt-3">Nuevo Gasto de Representacion</h2>
            <div class="row">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Area:</h6>
                    <select required='required' id="categoria"  class="custom-select">
                        <option value="" selected disabled>Administracion</option>
                        <option value="" ></option>
                    </select>

                    <h6 class="my-3">Descripcion del Gasto:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                </div>

                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Gasto Anual:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Cantidad Anual:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Gasto Mensual:</h6>
                        <input type="text" id="" name="" maxlength="70" required="required" class="form-control ">

                </div>
            </div>
            <div class="form-group my-5">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-slant bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <button type="submit" class="bttn-slant bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button>
                </div>
            </div>
      </div>
    </div>
  </div>
</div>

@endsection