<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ERP CITECCAL</title>
    <link href="https://fonts.googleapis.com/css?family=Croissant+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <nav id="barra-navegacion" class="navbar navbar-expand-lg navbar-dark transparent navegacion">
        <a class="navbar-brand logo-inicio" href="#"><img src="images/citeccal.png" wdith=10% class="img-fluid bg-dark" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link text-dark" href="/">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" href="#d-cualidad">Cualidades</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" href="#d-modulo">Modulos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" href="#d-contacto">Contactanos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bg-danger text-light" href="/login" target="_blank">Iniciar Sesion</a>
                </li>
            </ul>
        </div>
    </nav>
    <div id="fondo_pantalla" class="cuadro-fondo">
        <div class="texto-fondo">
            <h1 class="display-1 titulo-fondo">ERP CITECCAL</h1>
            <h2 class="cuerpo-fondo">ERP DISEÑADO PARA TODO TIPO DE EMPRESA DEL SECTOR CALZADO <br> QUE PERMITE INTEGRAR
            Y GESTIONAR LAS PRINCIPALES AREAS<br> DE SU ORGANIZACION DESDE UN SOLO SISTEMA. <h2>
            <div class="boton-fondo">
                    <button>Contactar</button>
                    <a href="/login"><button>Ingresar al sistema</button></a>
            </div>
        </div>
        <img src="images/fondo1.png" class="img-fluid fondo" alt="">
    </div>
    <section class="contenido-total">
        <div class="main cualidad" id="d-cualidad" align="center">
                <div class="container">
                    <h1 class="titulos-contenido display-2">Cualidades</h1>
                    <!--<div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active" data-interval="2000">
                                <img src="images/portabilidad.png" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block cuerpo">
                                    <h1 class="display-3">Portabilidad</h1>
                                    <h3>Acceda a su informacion desde cualquier dispositivo.</h3>
                                </div>
                            </div>
                            <div class="carousel-item" data-interval="2000">
                                <img src="images/servidor.png" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-light arriba">
                                    <h1 class="display-3">Seguridad</h1>
                                    <h3>Toda su informacion se encontrara respaldad y proteguida en nuestros servidores.</h3>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="images/calzado.png" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block text-light">
                                    <h1 class="display-3">Especializado</h1>
                                    <h3>Sistema totalmente especializado en la gestión empresarial.</h3>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tarjeta-cualidad arriba ">
                                <div class="icono-cualidad">
                                    <span><i class="fas fa-tv fa-4x"></i></span>
                                </div>
                                <div class="texto-cualidad">
                                    <h3 class="display-5">Portabilidad</h3>
                                </div>
                                <div class="Contenido-cualidad">
                                    <h4>Acceda desde cualquier dispositivo a toda su informacion.</h4>
                                </div>
                            </div>
                            <br>
                            <div class="tarjeta-cualidad arriba">
                                <div class="icono-cualidad">
                                    <span><i class="fas fa-lock fa-4x"></i></span>
                                </div>
                                <div class="texto-cualidad">
                                    <h3 class="display-5">Seguridad</h3>
                                </div>
                                <div class="Contenido-cualidad">
                                    <h4>Toda su informacion respaldada y asegurada en nuestros servidores.</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tarjeta-cualidad arriba">
                                <div class="icono-cualidad">
                                    <span><i class="fas fa-shoe-prints fa-4x"></i></span>
                                </div>
                                <div class="texto-cualidad">
                                    <h3 class="display-5">Especializado</h3>
                                </div>
                                <div class="Contenido-cualidad">
                                    <h4>Sistema totalmente especializado en el sector calzado.</h4>
                                </div>
                            </div>
                            <br>
                            <div class="tarjeta-cualidad arriba">
                                <div class="icono-cualidad">
                                    <span><i class="far fa-grin fa-4x"></i></span>
                                </div>
                                <div class="texto-cualidad">
                                    <h3 class="display-5">Usabilidad</h3>
                                </div>
                                <div class="Contenido-cualidad">
                                    <h4>Facilidad de utilizar en todos los modulos.</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

        <div class="main modulos" id="d-modulo">
            <div align="center">
                <h1 class="titulos-contenido display-1">Módulos</h1>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="card bg-dark text-white tarjeta">
                        <img class="card-img imagen-tarjeta" src="images/logistica.png" alt="Card image">
                        <div class="card-img-overlay texto-tarjeta">
                            <div align="center">
                                <h1 class="card-title">Logistica</h1>
                            </div>
                            <p class="card-text">Con la implementacion del modulo logistico usteded obtendra los siguientes
                                beneficios en su empresa.</p>
                            <p class="card-text">
                            <ul>
                                <li>
                                    <h4>Gestion de Materiales</h4>
                                </li>
                                <li>
                                    <h4>Gestion de Proveedores</h4>
                                </li>
                                <li>
                                    <h4>Gestion de Abastecimiento</h4>
                                </li>
                                <li>
                                    <h4>Gesiton de Inventarios</h4>
                                </li>
                                <li>
                                    <h4>Indicadores de Logistica</h4>
                                </li>
                            </ul>
                            </p>
                            <p class="card-text">Tiempo aproximado de implementacion 2 meses</p>
                        </div>
                    </div>
                    <br>
                    <div class="card bg-dark text-white tarjeta">
                        <img class="card-img imagen-tarjeta" src="images/costos.png" alt="Card image">
                        <div class="card-img-overlay texto-tarjeta">
                            <div align="center">
                                <h1 class="card-title">Estructura de Costos</h1>
                            </div>
                            <p class="card-text">Con la implementacion del modulo estructura de costos usteded obtendra los siguientes
                                beneficios en su empresa.</p>
                            <ul>
                                <li>
                                    <h4>Costos Directos</h4>
                                </li>
                                <li>
                                    <h4>Costos Indirectos</h4>
                                </li>
                                <li>
                                    <h4>Precio de Venta</h4>
                                </li>
                            </ul>
                            <p class="card-text">Tiempo aproximado de implementacion 2 meses</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card bg-dark text-white tarjeta">
                        <img class="card-img imagen-tarjeta" src="images/desarrollo.png" alt="Card image">
                        <div class="card-img-overlay texto-tarjeta">
                            <div align="center">
                                <h1 class="card-title">Desarrollo de Producto</h1>
                            </div>
                            <p class="card-text ">Con la implementacion del modulo desarrollo de producto usteded obtendra los siguientes
                                beneficios en su empresa.</p>
                            <ul>
                                <li>
                                    <h4>Gestion de Colecciones</h4>
                                </li>
                                <li>
                                    <h4>Gestion de Series</h4>
                                </li>
                                <li>
                                    <h4>Gestion de Lineas</h4>
                                </li>
                                <li>
                                    <h4>Gestion de Modelos</h4>
                                </li>
                                <li>
                                    <h4>Gesiton de Fichas de Prototipado</h4>
                                </li>
                            </ul>
                            <p class="card-text">Tiempo aproximado de implementacion 1 mes</p>
                        </div>
                    </div>
                    <br>
                    <div class="card bg-dark text-white tarjeta">
                        <img class="card-img imagen-tarjeta" src="images/produccion.png" alt="Card image">
                        <div class="card-img-overlay texto-tarjeta">
                            <div align="center">
                                <h1 class="card-title">Produccion</h1>
                            </div>
                            <p class="card-text">Con la implementacion del modulo estructura de costos usteded obtendra los siguientes
                                beneficios en su empresa.</p>
                            <ul>
                                <li>
                                    <h4>Gestion de Ordenes de Pedido</h4>
                                </li>
                                <li>
                                    <h4>Gestion de Ordenes de Produccion</h4>
                                </li>
                                <li>
                                    <h4>Reportes de Produccion</h4>
                                </li>
                            </ul>
                            <p class="card-text">Tiempo aproximado de implementacion 2 meses</p>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>

        <div class="main contacto" id="d-contacto" align="center">
            <h1 class="titulos-contenido display-1">Contáctanos</h1>
            <div class="container formulario">
                <div class="form-group">
                    <input type="text" value="" maxlength="200" class="form-control" required id="empresa"  placeholder="NOMBRE DE LA EMPRESA">
                </div>
                <div class="form-group">
                    <input type="text" value="" maxlength="300" class="form-control" required id="nombre"  placeholder="NOMBRE DE SOLICITANTE">
                </div>
                <div class="form-group">
                    <input type="text" value="" maxlength="9" class="form-control" required id="telefono"  placeholder="TELEFONO DE CONTACTO">
                </div>
                <div class="form-group">
                    <span id="Repuesta" style="display:none"></span>
                </div>
                <br>
                <button class="boton-formulario" id="enviar_datos">Enviar Datos</button>

            </div>
        </div>
    </section>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script>
    $(document).ready(function(){
        // Add scrollspy to <body>
        $('body').scrollspy({target: ".navbar", offset: 50});
        // Add smooth scrolling on all links inside the navbar
        $("#navbarNav a").on('click', function(event) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                scrollTop: $(hash).offset().top
                }, 800, function(){

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
                });
            }  // End if
        });
        $("#enviar_datos").click(function(){
            let empresa=nombre=telefono=""
             empresa=$("#empresa").val();
             nombre=$("#nombre").val();
             telefono=$("#telefono").val();
            if(empresa!="" && nombre !="" && telefono!="")
            {
                $.ajax({
                    url:"contactar/inicio",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:"POST",
                    data:
                    {
                    'empresa':empresa,
                    'nombre':nombre,
                    'telefono':telefono
                    },
                    success:function(data){
                        $("#Repuesta").text(data)
                        $("#Repuesta").toggle("fast")
                        setTimeout(function(){ $("#Repuesta").toggle("fast"); }, 3000);
                        $("#empresa").val("");
                        $("#nombre").val("");
                        $("#telefono").val("");
                    }
                })
            }
            else
            {
                $("#Repuesta").text("Llene todos los campos")
                $("#Repuesta").toggle("fast")
                setTimeout(function(){ $("#Repuesta").toggle("fast"); }, 3000);
            }
        })
    })
    function esVisible(){
            /* Ventana de Visualización*/
            var posTopView = $(window).scrollTop();
            var posButView = posTopView + $(window).height();
            /* Elemento a validar*/
            var elemTop = $("#fondo_pantalla").offset().top;
            var elemBottom = elemTop + $("#fondo_pantalla").height();
            /* Comparamos los dos valores tanto del elemento como de la ventana*/
            return ((elemBottom < posButView && elemBottom > posTopView) || (elemTop >posTopView && elemTop< posButView));
        }
    $(window).scroll(function(){
        if(esVisible())
        {
            console.log("no mostrar")
            $("#barra-navegacion").removeClass("bg-white");
        }
        else
        {
            console.log("mostrar")
            $("#barra-navegacion").addClass("bg-white");
        }

    })

    </script>
</body>

</html>
