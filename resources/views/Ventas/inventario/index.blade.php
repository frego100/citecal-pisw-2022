@extends ('layouts.admin')
@section ('contenido')

<div class="row">
    <div class="col-12">
        <h3 class="font-weight-bold">Listado de Productos en inventario
            <a href="inventario/ingresoVarios">
                <button class="bttn-unite bttn-md bttn-success float-right">Entrada de Varios Modelos</button>
            </a>
        </h3>
    </div>
    <div class="x_content table-responsive mt-4 col-12">
        <table id="example" class="display">
            <thead>
                <tr>
                    <th style="max-width: 60px; padding-right: 0;">Imagen</th>
                    <th style="max-width: 200px;">Colección, Línea y Serie</th>
                    <th>Cód. Modelo</th>
                    <th style="max-width: 175px;">Desc. Modelo</th>
                    @foreach([1, 2, 3, 4, 5, 6, 7] as $talla)
                    <th style="width: 40px; padding-right: 0;">T{{$talla}}</th>
                    @endforeach
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($modelos as $model)
                @if($model->estado==0)
                <tr class="bg-danger">
                    @endif
                    <td><img src="{{$model->imagen}}" alt="Foto modelo" width="60"></td>
                    <td>{{$model->coleccion}} {{$model->linea}} {{$model->serie}}</td>
                    <td>{{$model->codModelo}}</td>
                    <td>{{$model->descModelo}}</td>
                    @foreach($model->tallas as $talla)
                    <td style="padding: 0; border-left: 1px #DDDDDD solid;">
                        <div style="display: flex; flex-direction: column; align-items: center; row-gap: 0.5rem;">
                            <span>T{{$talla[0]}}</span>
                            <div style="border-bottom: 1px #DDDDDD solid; width: 100%;"></div>
                            <span>{{$talla[1]}}</span>
                        </div>
                    </td>
                    @endforeach
                    @for($i = 0; $i < 7 - count($model->tallas); $i++)
                        <td style="padding: 0; border-left: 1px #DDDDDD solid;">
                            <div style="display: flex; flex-direction: column; align-items: center;">
                                <span>-</span>
                            </div>
                        </td>
                        @endfor
                        <td style="border-left: 1px #DDDDDD solid;">
                            <a href="#" data-target="#modal-consulta-entrada-salida" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-warning" id="btn-consulta-entrada-{{$model->idModelo}}">
                                    <i class="fas fa-sign-in-alt"></i>
                                </button>
                            </a>
                            <a href="#" data-target="#modal-consulta-entrada-salida" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-danger" id="btn-consulta-salida-{{$model->idModelo}}">
                                    <i class="fas fa-sign-out-alt"></i>
                                </button>
                            </a>
                        </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-consulta-entrada-salida">
    <div class="modal-dialog modal-md modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="title_modal_consulta"></h4>
                <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i></div>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'Ventas/inventario/entradaSalidaIndividual', 'method' => 'POST', 'autocomplete' => 'off']) !!}
                {{ Form::token() }}
                <input name="tipo_consulta" id="tipo_consulta" type="hidden" value="0">
                <div style="display: flex; flex-direction: row;" id="cabecera_modelo_inventario" class="mb-4"></div>
                <div class="x_content table-responsive">
                    <table id="table_tallas_modelo" class="display">
                        <thead>
                            <tr>
                                <th>Talla</th>
                                <th>Cantidad en inventario</th>
                                <th id="descripcion_cantidad_tabla"></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="float-right" style="margin-top: 2rem; margin-bottom: 2rem;">
                    <button class="bttn-unite bttn-md bttn-danger mr-2" data-dismiss="modal" type="button">Cancelar</button>
                    <button class="bttn-unite bttn-md bttn-success" type="submit">Guardar</button>
                </div>
                {!! Form::close() !!}

                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script>
                    $(document).ready(function() {
                        let tabla_tallas_modelo = $('#table_tallas_modelo').DataTable({
                            'columnDefs': [{
                                'targets': 0,
                                'checkboxes': {
                                    'selectRow': true
                                }
                            }]
                        });

                        let modelos = <?php echo json_encode($modelos); ?>;
                        modelos.forEach(m => {
                            $(`#btn-consulta-salida-${m.idModelo}`).click(function() {
                                $("#title_modal_consulta").html("Salida de producto");
                                $("#descripcion_cantidad_tabla").html("Cantidad de salida");
                                $("#tipo_consulta").val(0); // Salida tipo 0
                                buildDataModelo(m);
                            });
                            $(`#btn-consulta-entrada-${m.idModelo}`).click(function() {
                                $("#title_modal_consulta").html("Ingreso de producto");
                                $("#descripcion_cantidad_tabla").html("Cantidad a ingresar");
                                $("#tipo_consulta").val(1); // Ingreso tipo 1
                                buildDataModelo(m);
                            });
                        });

                        function buildDataModelo(modelo) {
                            $("#cabecera_modelo_inventario").html(`
                                <input name="id_modelo" type="hidden" value="${modelo.idModelo}">
                                <input name="cantidad_tallas_modelo" type="hidden" value="${modelo.tallas.length}">
                                <img src="${modelo.imagen}" alt="Foto modelo" width="60">
                                <div style="display: flex; flex-direction: column; margin-left: 1rem;">
                                    <span>${modelo.coleccion}</span>
                                    <span>${modelo.linea}</span>
                                    <span>${modelo.serie}</span>
                                </div>
                                `);
                            // tabla_tallas_modelo.clear();
                            let index = 0;
                            modelo.tallas.forEach(t => {
                                let rowHTML = `<tr>
                                                    <td>T${t[0]}</td>
                                                    <td>${t[1]}</td>
                                                    <td>
                                                        <input type="number" step="1" name="cantidad-talla-${index}" value="0" style="width: 100px;">
                                                    </td>
                                                </tr>`;
                                const tr = $(rowHTML);
                                tabla_tallas_modelo.row.add(tr[0]).draw();
                                index++;
                            });
                        }
                    });
                </script>
            </div>
        </div>
    </div>
</div>
@endsection
