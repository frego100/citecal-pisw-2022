@extends ('layouts.admin')
@section ('contenido')

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            {!! Form::open(['url' => 'Ventas/inventario/ingresoVarios/store', 'method' => 'POST', 'autocomplete' => 'off', 'id' => 'formIngresarModelos']) !!}
            {{ Form::token() }}
            <div class="col-12">
                <h3 class="font-weight-bold">Entrada de varios Modelos</h3>
            </div>
            <div class="col-12" style="margin-bottom: 1.5rem; margin-top: 1rem; display: flex; flex-direction: row; justify-content: space-between; align-items: center;">
                <div>
                    <span>Ubicación: </span>
                    <select name="almacen" id="almacen" style="color: #8F8F8F; margin-left: 1rem; width: 300px;" required aria-required="Seleccione un almacén">
                        <option value="0" selected disabled>Ingrese nombre de almacén</option>
                        @foreach($almacenes as $almacen)
                        <option value="{{$almacen->cod_almacen}}">{{$almacen->nom_almacen}}</option>
                        @endforeach
                    </select>
                </div>
                <a href="#" data-target="#modal-agregar-modelo" data-toggle="modal">
                    <button class="bttn-unite bttn-md bttn-warning" id="modal_agregar_modelo">Agregar modelo</button>
                </a>
            </div>
            <div class="x_content table-responsive">
                <table id="table_modelos" class="display">
                    <thead>
                        <tr>
                            <th>Imagen</th>
                            <th>Colección, Línea y Serie</th>
                            <th>Cód. Modelo</th>
                            <th>Desc. Modelo</th>
                            @foreach([1, 2, 3, 4, 5, 6, 7] as $talla)
                            <th style="width: 40px; padding-left: 0;">T{{$talla}}</th>
                            @endforeach
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="modelos_selected"></tbody>
                </table>
            </div>
            <div class="float-right mt-3 mb-3">
                <a href="/Ventas/inventario">
                    <button class="bttn-unite bttn-md bttn-danger mr-2" type="button">Cancelar</button>
                </a>
                <button class="bttn-unite bttn-md bttn-success" type="submit">Guardar</button>
            </div>
            {!! Form::close() !!}
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script>
                $(document).ready(function() {
                    let tabla_modelos = $('#table_modelos').DataTable({
                        'columnDefs': [{
                            'targets': 0,
                            'checkboxes': {
                                'selectRow': true
                            }
                        }],
                        "language": {
                            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                        }
                    });
                    let modelos_seleccionados = [];
                    let modelo = <?php echo json_encode($modelos); ?>;
                    $("#modal_agregar_modelo").click(function() {
                        $("#modelos").empty();
                        generar_opciones_modelos();
                    });
                    $("#agregar_modelos").click(function() {
                        // $("#modelos input[type=checkbox]").removeAttr("disabled");
                        $.each(modelo, function(key, value) {
                            let contador = 0;
                            $('input[name^="lista_modelos"]').each(function() {
                                let codigo_temporal = $(this).val();
                                let seleccionado = $(this).is(':checked');
                                if (seleccionado && value.idModelo == codigo_temporal && !(modelos_seleccionados.find(element => element == value.idModelo))) {
                                    let rowHTML = `<tr>
                                                    <td><img src="${value.imagen}" alt="Foto modelo" width="60"></td>
                                                    <td>${value.coleccion} ${value.linea} ${value.serie}</td>
                                                    <td>
                                                        <input name="id_modelo[]" type="hidden" value="${contador}">
                                                        <input name="codModelo[]" type="hidden" value="${value.codModelo}">
                                                        <input name="descModelo[]" type="hidden" value="${value.descModelo}">
                                                        <input name="desarrollo[]" type="hidden" value="${value.idModelo}">
                                                        ${value.codModelo}
                                                    </td>
                                                    <td>${value.descModelo}</td>
                                                    `;
                                    let count = 0;
                                    value.tallas.forEach(t => {
                                        rowHTML += `<td>
                                                        <div style="width: 50px; display: flex; flex-direction: column; align-items: center;">
                                                            <span>T${t}</span>
                                                            <input name="cantidad-tallas-${value.idModelo}" type="hidden" value="${value.tallas.length}">
                                                            <input type="number" step="1" name="cantidad-por-talla-${value.idModelo}-${count}" value="0" style="width: 50px;">
                                                        </div>
                                                    </td>
                                                    `;
                                        count++;
                                    });
                                    for (let i = 0; i < 7 - value.tallas.length; i++) {
                                        rowHTML += "<td>-</td>";
                                    }
                                    rowHTML += `<td>
                                                    <button class="bttn-unite bttn-md bttn-danger" type="button" name="${value.idModelo}"><i class="fas fa-trash-alt"></i></button>
                                                </td>
                                            </tr>`;
                                    modelos_seleccionados.push(value.idModelo);
                                    const tr = $(rowHTML);
                                    tabla_modelos.row.add(tr[0]).draw();
                                }
                                contador++;
                            });
                        });
                    });

                    $('#table_modelos').on('click', 'button', function() {
                        let index = modelos_seleccionados.indexOf($(this).attr('name'));
                        modelos_seleccionados.splice(index, 1);
                        tabla_modelos.row($(this).parents('tr')).remove().draw();
                    });

                    function generar_opciones_modelos() {
                        $.ajax({
                            url: "ingreso/obtenerModelos/",
                            success: function(html) {
                                $.each(html, function(key, value) {
                                    let codigo = value.idModelo;
                                    console.log(modelos_seleccionados.length);
                                    if (!(modelos_seleccionados || []).find(m => m == codigo)) {
                                        $("#modelos").append(
                                            `<div style="display: flex; flex-direction: row;">
                                                <input type="checkbox" data-idModelo="${codigo}" name="lista_modelos[]" value="${codigo}" class="mr-3">
                                                <img src="${value.imagen}" alt="Foto modelo" width="60">
                                                <div style="display: flex; flex-direction: column; margin-left: 1rem;">
                                                    <span>${value.coleccion}</span>
                                                    <span>${value.linea}</span>
                                                    <span>${value.serie}</span>
                                                </div>
                                            </div>`
                                        );
                                    }
                                });
                            }
                        });
                    };

                    // Validación form
                    $('#formIngresarModelos').change(function() {
                        console.log('change form');
                    });
                    tabla_modelos.on('draw.dt', function() {
                        console.log('size', tabla_modelos.rows().data().length);
                        console.log("modelos seleccionados", modelos_seleccionados);
                        // tabla_modelos.rows().data().map(row => {
                        //     console.log("rowwww", row);
                        // });
                    });
                    // Fin Validación form
                });
            </script>
        </div>
    </div>
</div>


<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-agregar-modelo">
    <div class="modal-dialog modal-md modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar modelo</h4>
                <div class="text-right"> <i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i></div>
            </div>
            <div class="modal-body">
                <div id="modelos"></div>
                <div class="float-right" style="margin-top: 2rem; margin-bottom: 2rem;">
                    <button class="bttn-unite bttn-md bttn-danger mr-2" data-dismiss="modal">Cancelar</button>
                    <button class="bttn-unite bttn-md bttn-success" data-dismiss="modal" id="agregar_modelos">
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection