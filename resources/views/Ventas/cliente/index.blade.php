@extends ('layouts.admin')
@section ('contenido')

    <div class="preloader"></div>
    <div>
        <h3 class="font-weight-bold">Listado de Clientes
            <a class="#" id="#" href="#"
               data-target="#modal-create-clients" data-toggle="modal">

                <button type="button" class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nuevo Cliente
                </button>
            </a>
        </h3>

        @include('Ventas.cliente.modalcreate')
    </div>

    <div class="x_content table-responsive">
        <table id="example" class="display">
            <thead>
            <tr>
                <th>Tipo y Número de Documento</th>
                <th>Apellidos y Nombres</th>
                <th>País y Departamento</th>
                <th>Dirección 1</th>
                <th>Celular 1</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clientes as $client)
                <tr>
                    <td>
                        @if($client->tipo_documento == 0)
                            DNI: {{$client->numero_documento}}
                        @else
                            RUC: {{$client->numero_documento}}
                        @endif
                    </td>
                    <td>
                        {{Str::upper($client->apellidos)}}, {{Str::upper($client->nombres)}}
                    </td>
                    <td>
                        {{Str::upper($client->nombre_pais)}}, {{Str::upper($client->nombre_departamento)}}
                    </td>
                    <td>
                        {{Str::upper($client->direccion_1)}}
                    </td>
                    <td>
                        {{Str::upper($client->celular_1)}}
                    </td>
                    <td>
                        <a class="see-client" id="{{ $client->cliente_id }}" href="#"
                           data-target="#modal-ver-cliente" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-primary"><i class="far fa-eye"></i></button>
                        </a>
                        <a class="edit-client" id="{{ $client->cliente_id }}" href="#"
                           data-target="#modal-editar-cliente" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning "><i class="far fa-edit"></i></button>
                        </a>
                        <a class="delete-client" id="{{ $client->cliente_id }}" href="#"
                           data-target="#modal-delete-cliente" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <!-- VER DATOS DEL CLIENTE -->
        <div class="modal fade" id="modal-see-client" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header float-right">
                        <h5>Ver Detalles Cliente</h5>
                        <div class="text-right"><i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i>
                        </div>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Tipo de Documento:<span
                                            class="obg">(*)</span></strong></label><br/>
                                <input type="radio" id="dni_see_cli" name="tipo_documento" value="0" disabled>
                                <label for="dni">DNI</label>
                                <input type="radio" id="ruc_see_cli" name="tipo_documento" value="1" disabled>
                                <label for="ruc">RUC</label>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Número de Documento:<span
                                            class="obg">(*)</span></strong></label>
                                <input type="text" name="numero_documento" class="form-control "
                                       id="numero_documento_see_cli" disabled>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Nombres:<span
                                            class="obg">(*)</span></strong></label>
                                <input type="text" name="nombres" class="form-control " id="nombres_see_cli" disabled>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Apellidos:<span class="obg">(*)</span></strong></label>
                                <input type="text" name="apellidos" class="form-control " id="apellidos_see_cli"
                                       disabled>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>País:<span
                                            class="obg">(*)</span></strong></label>
                                <select name="pais_id_see_cli" class="custom-select" id="pais_id_see_cli" disabled>
                                    <option value="" disabled selected>Seleccione un país</option>
                                    @foreach($pais as $p)
                                        <option value="{{$p->pais_id}}">{{$p->nombre_pais}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Departamento:<span
                                            class="obg">(*)</span></strong></label>
                                <select name="departamento_id_see_cli" class="custom-select"
                                        id="departamento_id_see_cli" disabled>
                                    <option value="" disabled selected>Seleccione un departamento</option>
                                    @foreach($departamentos as $d)
                                        <option value="{{$d->departamento_id}}">{{$d->nombre_departamento}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12 col-md-offset-12 col-xs-12 ">
                                <label for="total_orden_compra"><strong>Dirección 01:</strong></label>
                                <input type="text" name="direccion_1" class="form-control " id="direccion_1_see_cli"
                                       disabled>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12 col-md-offset-12 col-xs-12 ">
                                <label for="total_orden_compra"><strong>Dirección 02: (Opcional)</strong></label>
                                <input type="text" name="direccion_2" class="form-control " id="direccion_2_see_cli"
                                       disabled>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Celular 01:</strong></label>
                                <input type="number" name="celular_1" class="form-control " id="celular_1_see_cli"
                                       disabled>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Celular 02:</strong></label>
                                <input type="number" name="celular_2" class="form-control " id="celular_2_see_cli"
                                       disabled>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12 col-md-offset-12 col-xs-12 ">
                                <label for="total_orden_compra"><strong>Correo:</strong></label>
                                <input type="text" name="correo" class="form-control " id="correo_see_cli" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-secondary col-md-2 col-md-offset-5"
                                data-dismiss="modal"
                                id="close_see_client_modal">Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- EDITAR DATOS DEL CLIENTE -->
        <div class="modal fade" id="modal-edit-client" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                {!!Form::open(array('action'=>['ClientesVentasController@update', '1'],'method'=>'PATCH'))!!}
                <div class="modal-content">
                    <div class="modal-header float-right">
                        <h5>Editar Cliente</h5>
                        <div class="text-right"><i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i>
                        </div>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" id="client_id_edit" name="client_id_edit">
                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Tipo de Documento: <span
                                            class="obg">(*)</span></strong></label><br/>
                                <input type="radio" id="dni_edit_cli" name="tipo_documento" value="0">
                                <label for="dni">DNI</label>
                                <input type="radio" id="ruc_edit_cli" name="tipo_documento" value="1">
                                <label for="ruc">RUC</label>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Número de Documento: <span
                                            class="obg">(*)</span></strong></label>
                                <input type="text" name="numero_documento_edit_cli" class="form-control "
                                       id="numero_documento_edit_cli" minlength="8" maxlength="11" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Nombres: <span
                                            class="obg">(*)</span></strong></label>
                                <input type="text" name="nombres_edit_cli" class="form-control " id="nombres_edit_cli"
                                       required>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Apellidos: <span
                                            class="obg">(*)</span></strong></label>
                                <input type="text" name="apellidos_edit_cli" class="form-control "
                                       id="apellidos_edit_cli" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>País: <span
                                            class="obg">(*)</span></strong></label>
                                <select name="pais_id_edit_cli" class="custom-select" id="pais_id_edit_cli" required>
                                    <option value="" disabled selected>Seleccione un país</option>
                                    @foreach($pais as $p)
                                        <option value="{{$p->pais_id}}">{{$p->nombre_pais}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Departamento: <span
                                            class="obg">(*)</span></strong></label>
                                <select name="departamento_id_edit_cli" class="custom-select"
                                        id="departamento_id_edit_cli" required>
                                    <option value="" disabled selected>Seleccione un departamento</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12 col-md-offset-12 col-xs-12 ">
                                <label for="total_orden_compra"><strong>Dirección 01: <span
                                            class="obg">(*)</span></strong></label>
                                <input type="text" name="direccion_1_edit_cli" class="form-control "
                                       id="direccion_1_edit_cli" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12 col-md-offset-12 col-xs-12 ">
                                <label for="total_orden_compra"><strong>Dirección 02: (Opcional)</strong></label>
                                <input type="text" name="direccion_2_edit_cli" class="form-control "
                                       id="direccion_2_edit_cli">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Celular 01: <span
                                            class="obg">(*)</span></strong></label>
                                <input type="text" name="celular_1_edit_cli" class="form-control "
                                       id="celular_1_edit_cli" minlength="9" maxlength="9" required>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                                <label for="total_orden_compra"><strong>Celular 02:</strong></label>
                                <input type="text" name="celular_2_edit_cli" class="form-control "
                                       id="celular_2_edit_cli" minlength="9" maxlength="9">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12 col-md-offset-12 col-xs-12 ">
                                <label for="total_orden_compra"><strong>Correo: <span
                                            class="obg">(*)</span></strong></label>
                                <input type="email" name="correo_edit_cli" class="form-control " id="correo_edit_cli"
                                       required>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5"
                                id="edit_client">Guardar
                        </button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5 "
                                style="  margin-right:35%;" data-dismiss="modal">Cancelar
                        </button>
                    </div>
                </div>
                {{ Form::Close() }}
            </div>
        </div>

        <!-- ELIMINAR CLIENTE -->
        <div class="modal fade" id="modal-delete-client" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                {!!Form::open(array('action'=>['ClientesVentasController@destroy', '2'],'method'=>'DELETE'))!!}
                <div class="modal-content">
                    <div class="modal-header float-right">
                        <h5>Eliminar Cliente</h5>
                        <div class="text-right"><i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i>
                        </div>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" id="client_id_delete" name="client_id_delete">
                        ¿Está seguro de eliminar este cliente?
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5"
                                id="delete_client">Confirmar
                        </button>
                        <button type="button" class="bttn-unite bttn-md bttn-secondary col-md-2 col-md-offset-5 "
                                style="  margin-right:35%;" data-dismiss="modal">Cancelar
                        </button>

                    </div>

                </div>
                {!! Form::Close() !!}
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //Global vars
            const clients = <?php echo $clientes; ?>;
            const departments = <?php echo $departamentos; ?>;

            $('#example').on('click', 'a.see-client', function () {
                const id = $(this).attr('id')
                const client = clients.find(cli => cli.cliente_id === Number(id))
                if (client.tipo_documento === 0) {
                    $('#dni_see_cli').prop('checked', true)
                } else {
                    $('#ruc_see_cli').prop('checked', true)
                }
                $('#numero_documento_see_cli').val(client.numero_documento)
                $('#nombres_see_cli').val(client.nombres)
                $('#apellidos_see_cli').val(client.apellidos)
                $('#pais_id_see_cli option[value=' + client.pais_id + ']').prop('selected', true)
                $('#departamento_id_see_cli option[value=' + client.departamento_id + ']').prop('selected', true)
                $('#direccion_1_see_cli').val(client.direccion_1)
                $('#direccion_2_see_cli').val(client.direccion_2)
                $('#celular_1_see_cli').val(client.celular_1)
                $('#celular_2_see_cli').val(client.celular_2)
                $('#correo_see_cli').val(client.correo)
                $("#modal-see-client").modal({
                    show: true
                });
            })

            $('#example').on('click', 'a.edit-client', function () {
                const id = $(this).attr('id')
                const client = clients.find(cli => cli.cliente_id === Number(id))
                if (client.tipo_documento === 0) {
                    $('#dni_edit_cli').prop('checked', true)
                } else {
                    $('#ruc_edit_cli').prop('checked', true)
                }
                $('#client_id_edit').val(client.cliente_id)
                $('#numero_documento_edit_cli').val(client.numero_documento)
                $('#nombres_edit_cli').val(client.nombres)
                $('#apellidos_edit_cli').val(client.apellidos)
                $('#pais_id_edit_cli option[value=' + client.pais_id + ']').prop('selected', true)
                $('#departamento_id_edit_cli').empty()
                const deps_by_country = departments.filter(dep => dep.pais_id === Number($('#pais_id_edit_cli').val()))
                $('#departamento_id_edit_cli').append('<option value="" disabled>Seleccione un departamento')
                for (let i = 0; i < deps_by_country.length; i++) {
                    $('#departamento_id_edit_cli').append('<option value=' + deps_by_country[i].departamento_id + '>' + deps_by_country[i].nombre_departamento + '</option>')
                }
                $('#departamento_id_edit_cli option[value=' + client.departamento_id + ']').prop('selected', true)
                $('#direccion_1_edit_cli').val(client.direccion_1)
                $('#direccion_2_edit_cli').val(client.direccion_2)
                $('#celular_1_edit_cli').val(client.celular_1)
                $('#celular_2_edit_cli').val(client.celular_2)
                $('#correo_edit_cli').val(client.correo)
                $("#modal-edit-client").modal({
                    show: true
                });
            })

            $('#example').on('click', 'a.delete-client', function () {
                const id = $(this).attr('id')
                const client = clients.find(cli => cli.cliente_id === Number(id))
                $('#client_id_delete').val(client.cliente_id)
                $("#modal-delete-client").modal({
                    show: true
                });
            })

            $('#pais_id_edit_cli').on('change', function () {
                $('#departamento_id_edit_cli').empty()
                const deps_by_country = departments.filter(dep => dep.pais_id === Number($('#pais_id_edit_cli').val()))
                $('#departamento_id_edit_cli').append('<option value="" selected disabled>Seleccione un departamento')
                for (let i = 0; i < deps_by_country.length; i++) {
                    $('#departamento_id_edit_cli').append('<option value=' + deps_by_country[i].departamento_id + '>' + deps_by_country[i].nombre_departamento + '</option>')
                }
            })

        });
    </script>

    <style>
        .modal-content {
            padding: 20px;
        }

        .obg {
            color: red;
        }
    </style>
@endsection
