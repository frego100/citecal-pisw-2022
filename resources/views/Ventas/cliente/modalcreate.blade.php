<div class="modal fade" id="modal-create-clients" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        {!!Form::open(array('action'=>'ClientesVentasController@store','method'=>'POST'))!!}
        <div class="modal-content">
            <div class="modal-header float-right">
                <h5>Registrar Cliente</h5>
                <div class="text-right"><i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i></div>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra"><strong>Tipo de Documento:<span class="obg">(*)</span></strong></label>
                        <input type="radio" id="dni" name="tipo_documento" value="0" checked>
                        <label for="dni">DNI</label>
                        <input type="radio" id="ruc" name="tipo_documento" value="1">
                        <label for="ruc">RUC</label>
                    </div>
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra"><strong>Número de Documento:<span
                                    class="obg">(*)</span></strong></label>
                        <input type="text" name="numero_documento" class="form-control" id="numero_documento"
                               minlength="8" maxlength="11" required>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra"><strong>Nombres:<span class="obg">(*)</span></strong></label>
                        <input type="text" name="nombres" class="form-control" id="nombres" required>
                    </div>
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra"><strong>Apellidos:<span class="obg">(*)</span></strong></label>
                        <input type="text" name="apellidos" class="form-control" id="apellidos" required>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra"><strong>País:<span class="obg">(*)</span></strong></label>
                        <select name="pais_id" class="custom-select" id="pais_id" required>
                            <option value="" disabled selected>Seleccione un país</option>
                            @foreach($pais as $p)
                                <option value="{{$p->pais_id}}">{{$p->nombre_pais}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra"><strong>Departamento:<span
                                    class="obg">(*)</span></strong></label>
                        <select name="departamento_id" class="custom-select" id="departamento_id" required>
                            <option value="" disabled selected>Seleccione un pais primero</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12 col-md-offset-12 col-xs-12 ">
                        <label for="total_orden_compra"><strong>Dirección 01:<span
                                    class="obg">(*)</span></strong></label>
                        <input type="text" name="direccion_1" class="form-control" id="direccion_1" required>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12 col-md-offset-12 col-xs-12 ">
                        <label for="total_orden_compra"><strong>Dirección 02: (Opcional)</strong></label>
                        <input type="text" name="direccion_2" class="form-control" id="direccion_2">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra"><strong>Celular 01:<span class="obg">(*)</span></strong></label>
                        <input type="text" name="celular_1" class="form-control" id="celular_1" minlength="9"
                               maxlength="9" required>
                    </div>
                    <div class="form-group col-md-6 col-md-offset-6 col-xs-6 ">
                        <label for="total_orden_compra"><strong>Celular 02: (Opcional)</strong></label>
                        <input type="text" name="celular_2" class="form-control" id="celular_2" minlength="9"
                               maxlength="9">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12 col-md-offset-12 col-xs-12 ">
                        <label for="total_orden_compra"><strong>Correo:<span class="obg">(*)</span></strong></label>
                        <input type="email" name="correo" class="form-control" id="correo" required>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5"
                        id="create_client">Crear
                </button>
                <button type="button" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5 "
                        style="  margin-right:35%;" data-dismiss="modal">Cancelar
                </button>

            </div>

        </div>
        {!! Form::Close() !!}
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //Global vars
        const clients = <?php echo $clientes; ?>;
        const departments = <?php echo $departamentos; ?>;

        $('#pais_id').on('change', function () {
            $('#departamento_id').empty()
            const deps_by_country = departments.filter(dep => dep.pais_id === Number($('#pais_id').val()))
            $('#departamento_id').append('<option value="" selected disabled>Seleccione un departamento')
            for (let i = 0; i < deps_by_country.length; i++) {
                $('#departamento_id').append('<option value=' + deps_by_country[i].departamento_id + '>' + deps_by_country[i].nombre_departamento + '</option>')
            }
        })
    });
</script>

<style>
    .modal-content {
        padding: 20px;
    }

    .obg {
        color: red;
    }
</style>
