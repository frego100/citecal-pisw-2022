@extends ('layouts.admin')
@section ('contenido')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3 class="font-weight-bold">Nueva Orden de Pedido
                    <div class="clearfix"></div>
                </h3>
            </div>
            @if (count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            {!!Form::open(array('url'=>'Ventas/pedidos/create/store','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 col-xs-8">
                        <label class="d-flex"><a class="col-md-4"> Cliente </a>
                            <select class="col-md-6 custom-select" id="cli" name="cliente">
                                <option value="" disabled selected>Ingrese nombre o N° documento</option>
                                @foreach($clientes as $cliente)
                                <option value="{{$cliente->cliente_id}}">{{$cliente->nombres}}</option>
                                @endforeach
                            </select> </label>
                        <label class="d-flex"> <a class="col-md-4"> Fecha de Entrega </a>
                            <input type="date" name="fecha_entrega" class=" col-md-6 form-control" id="inputFechaEntrega" required placeholder="">
                        </label>
                    </div>
                    <div class="col-md-4 col-md-offset-4 col-xs-4">
                        <a href="#" data-target="#modal-agregar-modelo" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning" id="modal_agregar_modelo">
                                Agregar modelo
                            </button>
                        </a>
                    </div>
                </div>

                <div class="x_content table-responsive">
                    <table id="table_modelos" class="display">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Cod. Modelo</th>
                                <th>Desc. Modelo</th>
                                @foreach([1, 2, 3, 4, 5, 6, 7] as $talla)
                                <th style="width: 40px; padding-left: 0;">T{{$talla}}</th>
                                @endforeach
                                <th>Valor Unitario (Sin IGV)
                                </th>
                                <th>Valor Total
                                </th>
                                <th>Acciones
                                </th>
                            </tr>
                        </thead>
                        <tbody id="modelos_selected"></tbody>
                    </table>
                </div>
                <div class="row" style="margin-top : 5%;">
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label style="width : 130%;">
                            <label style="margin-right : 0.5%;">Sub Total:</label>
                            S/.<input type="number" step="any" style="width : 55%;" id="subTotalval" name="subTotalval" disabled="true">
                        </label>
                    </div>
                </div>

                <div class="row" style="margin-top : -0.8%;">
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label style="width : 130%;">
                            <label style="margin-right : 1%;">IGV (18%):</label>
                            S/.<input type="number" step="any" style="width : 55%;" id="igvSubtotal" name="igvSubtotal" disabled="true">
                        </label>
                    </div>
                </div>

                <div class="row" style="margin-top : -0.8%;">
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label style="width : 130%;">
                            <label style="margin-right : 7.5%;">Total:</label>
                            S/.<input type="number" step="any" style="width : 55%;" id="idTotal" name="idTotal" disabled="true">
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="observaciones">Observaciones</label>
                        <input type="text" id="obervaciones" name="observaciones" maxlength="2000" class="form-control">
                        <input type="hidden" id="tipoDetalle" name="tipoDetalle" value="0" class="form-control">
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="float-right mt-3 mb-3">
                    <a href="/Ventas/pedidos">
                        <button class="bttn-unite bttn-md bttn-danger mr-2" type="button">Cancelar</button>
                    </a>
                    <button class="bttn-unite bttn-md bttn-success" type="submit">Guardar</button>
                </div>
            </div>
            {!!Form::close()!!}

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
            <script>
                $(document).ready(function() {
                    let tabla_modelos = $('#table_modelos').DataTable({
                        'columnDefs': [{
                            'targets': 0,
                            'checkboxes': {
                                'selectRow': true
                            }
                        }],
                        "language": {
                            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                        }
                    });
                    let modelos_seleccionados = [];
                    let modelo = <?php echo json_encode($modelos_inventario); ?>;
                    let today = new Date();

                    function formatDayMonth(val) {
                        if (val.toString().length < 2) {
                            return '0' + val.toString();
                        }
                        return val;
                    }
                    $('#inputFechaEntrega').attr({
                        'min': `${today.getFullYear()}-${formatDayMonth(today.getMonth() + 1)}-${formatDayMonth(today.getDate())}`
                    });

                    $("#agregar_modelos").click(function() {
                        // $("#modelos input[type=checkbox]").removeAttr("disabled");
                        let itemsAgregados = 0;
                        $.each(modelo, function(key, value) {
                            let contador = 0;
                            $('input[name^="lista_modelos"]').each(function() {
                                let codigo_temporal = $(this).val();
                                let seleccionado = $(this).is(':checked');
                                if (seleccionado && value.cod_producto_terminado == codigo_temporal && !(modelos_seleccionados.find(element => element == value.cod_producto_terminado))) {
                                    itemsAgregados++;
                                    let rowHTML = `<tr>
                                                        <td><img src="${value.imagen}" alt="Foto modelo" width="60"></td>
                                                        <td>${value.cod_producto_terminado}</td>
                                                        <td><input name="id_modelo[]" type="hidden" value="${contador}">${value.descripcion}</td>
                                                        <input name="codModelo[]" type="hidden" value="${value.cod_producto_terminado}">
                                                        <input name="descModelo[]" type="hidden" value="${value.cod_producto_terminado}">
                                                    `;
                                    let count = 0;
                                    value.tallas.forEach(t => {
                                        rowHTML += `<td>
                                                        <div style="width: 50px; display: flex; flex-direction: column; align-items: center;">
                                                            <span>T${t}</span>
                                                            <input name="cantidad-tallas-${value.cod_producto_terminado}" type="hidden" value="${value.tallas.length}">
                                                            <input type="number" step="1" id="cantidadTalla-${value.cod_producto_terminado}-${t}"  name="cantidad-por-talla-${value.cod_producto_terminado}-${count}" value="0" style="width: 50px;">
                                                        </div>
                                                    </td>`;
                                        count++;
                                    });
                                    for (let i = 0; i < 7 - value.tallas.length; i++) {
                                        rowHTML += "<td>-</td>";
                                    }
                                    rowHTML += `<td><input type="number" step="1" id="precio-modelo-${value.cod_producto_terminado}" name="cantidad-por-talla-${value.cod_producto_terminado}-sinIGV" value="${value.precio}" style="width: 50px;"></td>
                                                <td><input type="number" step="1" class ="totalModelo" id="cantidad-por-talla-${value.cod_producto_terminado}-total"  name="cantidad-por-talla-${value.cod_producto_terminado}-total" value=0 style="width: 100px;"></td>
                                                <td><button class="bttn-unite bttn-md bttn-danger" type="button" name="${value.cod_producto_terminado}"><i class="fas fa-trash-alt"></i></button></td>
                                            </tr>`;
                                    const tr = $(rowHTML);
                                    tabla_modelos.row.add(tr[0]).draw();
                                    modelos_seleccionados.push(value.cod_producto_terminado);
                                    let count2 = 0;
                                    value.tallas.forEach(t => {
                                        $(`#cantidadTalla-${value.cod_producto_terminado}-${t}`).change(function() {
                                            updateValues(value);
                                        });
                                    });
                                }
                                contador++;
                            });
                            $(`#precio-modelo-${value.cod_producto_terminado}`).change(function() {
                                console.log('cambiando precio', value.cod_producto_terminado);
                                updateValues(value);
                            });
                        });
                    });

                    function updateValues(value) {
                        let cantidad = 0;
                        value.tallas.forEach(tt => {
                            cantidad = parseFloat(cantidad) + parseFloat($(`#cantidadTalla-${value.cod_producto_terminado}-${tt}`).val());
                        });
                        let total = cantidad * $(`#precio-modelo-${value.cod_producto_terminado}`).val(); // value.precio;
                        $(`#cantidad-por-talla-${value.cod_producto_terminado}-total`).attr({
                            'value': total
                        });
                        let collection = document.getElementsByClassName("totalModelo");
                        let SubTOTAL = 0
                        for (let i = 0; i < collection.length; i++) {
                            SubTOTAL = parseFloat(SubTOTAL) + parseFloat(collection[i].value);
                        }
                        let cantIGV = (parseFloat(SubTOTAL)) * 0.18;
                        let TOTAL = SubTOTAL + cantIGV;
                        $("#subTotalval").attr({
                            'value': SubTOTAL
                        });
                        $("#igvSubtotal").attr({
                            'value': cantIGV
                        });
                        $("#idTotal").attr({
                            'value': TOTAL
                        });
                    }

                    $('#table_modelos').on('click', 'button', function() {
                        let index = modelos_seleccionados.indexOf($(this).attr('name'));
                        modelos_seleccionados.splice(index, 1);
                        tabla_modelos.row($(this).parents('tr')).remove().draw();

                        let collection = document.getElementsByClassName("totalModelo");
                        let SubTOTAL = 0


                        for (let i = 0; i < collection.length; i++) {
                            console.log('Monto', collection[i].value);
                            SubTOTAL = parseFloat(SubTOTAL) + parseFloat(collection[i].value);
                        }
                        //let SubTOTAL = (parseFloat(TOTAL) / parseFloat(118)) * 100;
                        let cantIGV = (parseFloat(SubTOTAL)) * 0.18;
                        let TOTAL = SubTOTAL + cantIGV;

                        document.getElementById("subTotalval").setAttribute('value', SubTOTAL);
                        document.getElementById("igvSubtotal").setAttribute('value', cantIGV);
                        document.getElementById("idTotal").setAttribute('value', TOTAL);


                    });


                });
            </script>


        </div>
    </div>
</div>

<style>
    .encabezado-tallas {
        padding: 0px !important;
    }
</style>



<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-agregar-modelo">
    <div class="modal-dialog modal-md modal-dialog-centered ">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar modelo</h4>
                <div class="text-right"><i data-dismiss="modal" aria-label="Close" class="fa fa-close"></i></div>
            </div>
            <div class="modal-body">
                <div id="modelos">
                    @foreach($modelos_inventario as $modelo)
                    <div style="display: flex; flex-direction: row;">
                        <input type="checkbox" data-idModelo="{{$modelo->cod_producto_terminado}}" name="lista_modelos[]" value="{{$modelo->cod_producto_terminado}}" class="mr-3">
                        <img src="{{$modelo->imagen}}" alt="Foto modelo" width="80" style="padding: 5px;">
                        <div style="display: flex; flex-direction: column; margin-left: 1rem;">
                            <span>{{$modelo->descripcion}}</span>
                            <span>{{$modelo->coleccion}} {{$modelo->linea}} {{$modelo->serie}}</span>
                            <span>S/ {{$modelo->precio}}</span>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="float-right" style="margin-top: 2rem; margin-bottom: 2rem;">
                    <button class="bttn-unite bttn-md bttn-danger mr-2" data-dismiss="modal">Cancelar</button>
                    <button class="bttn-unite bttn-md bttn-success" data-dismiss="modal" id="agregar_modelos">
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection