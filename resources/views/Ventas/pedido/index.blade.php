@extends ('layouts.admin')
@section ('contenido')

    <div class="preloader"></div>
    <div class="row">
        <div class="col-12">
            <h3 class="font-weight-bold">Listado de Pedidos
                <a href="pedidos/create">
                    <button class="bttn-unite bttn-md bttn-success float-right">Nueva Orden de Pedido</button>
                </a>
            </h3>
        </div>

        <div class="x_content table-responsive mt-4 col-12">
            <table id="example" class="display">
                <thead>
                <tr>
                    <th>Cód. Orden Pedido</th>
                    <th>Cliente</th>
                    <th>Fecha Pedido</th>
                    <th>Fecha Entrega</th>
                    <th>Costo Total</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ordenes as $orden)
                    <tr>
                        <td>{{$orden->codigo}}</td>
                        <td>{{$orden->nombres}}</td>
                        <td>{{$orden->fecha_pedido}}</td>
                        <td>{{$orden->fecha_entrega}}</td>
                        <td>{{$orden->costo_total}}</td>
                        <td>
                            <a class="recepcion-talla" id="" href="pedidos/entrega/{{$orden->id}}">
                                <button class="bttn-unite bttn-md bttn-primary"><i class="far fa-eye"></i></button>
                            </a>
                            <a class="generate_pdf_sale_order" id="{{$orden->id}}" href="#">
                                <button id="pdf_sale_order" class="bttn-unite bttn-md bttn-warning"><i
                                        class="fas fa-file-pdf"></i></button>
                            </a>

                            <a class="cancelar" href="pedidos/destroy/{{$orden->id}}" {{--data-target="#modal-cancelar-pedido" data-toggle="modal"--}}>
                                <button id="exportar_click_talla" class="bttn-unite bttn-md bttn-danger"><i
                                        class="fa fa-close"></i></button>
                            </a>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>


        </div>

        <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
             id="modal-cancelar-pedido">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Eliminar Orden de Pedido</h4>
                    </div>
                    <div class="modal-body">
                        <p>Confirme si desea Elimimar la Orden de Pedido</p>
                        <input type="text" style="display:none" name="codigo" value="0">
                        <input type="text" style="display:none" name="estado" value="0">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <style>
        .acciones {
            display: flex;
            gap: 10px;
        }
    </style>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> -->
    <script type="text/javascript">


        $(document).ready(function () {

            $('#example').on('click', 'a.generate_pdf_sale_order', function () {
                const id = $(this).attr('id')
                window.open('../Ventas/pdf/ordenPedidos/' + id)
            })


        })
    </script>
@endsection
