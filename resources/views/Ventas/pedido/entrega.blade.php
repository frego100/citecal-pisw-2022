@extends ('layouts.admin')
@section ('contenido')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h3 class="font-weight-bold">Entrega Orden de Pedido {{$idPedido}}

                    <div class="clearfix"></div>
                </h3>
            </div>
            @if (count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            {!!Form::open(array('url'=>'Ventas/pedidos/entrega/storeOrdenEntrega','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-6 col-xs-8">
                        {{--CODIGO CABECERA--}}
                        <input type="hidden" id="idDelPedido" name="idCabecera" value={{$idPedido}} class="form-control">
                        <label class="d-flex"><a class="col-md-4"> Cliente </a>
                            <select class="col-md-6 custom-select" id="cli" name="cliente" disabled="true">
                                <option value="1 " disabled selected>{{$orden_pedido[0]->nombres}}</option>
                            </select> </label>
                        <label class="d-flex"> <a class="col-md-4"> Fecha de Entrega </a>
                            <input type="text" name="fecha_tentativa" class=" col-md-6 form-control" id="" required disabled placeholder={{$orden_pedido[0]->fecha_entrega}}>
                        </label>
                    </div>
                </div>

                <div class="x_content table-responsive">
                    <table id="table_modelos" class="display">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Cod. Modelo</th>

                                @foreach([1, 2, 3, 4, 5, 6, 7] as $talla)
                                <th style="width: 40px; padding-left: 0;">T{{$talla}}</th>
                                @endforeach

                                <th>Cant. Pedida</th>
                                <th>Cant. Entregada</th>
                                <th>Cant. Faltante</th>
                                <th>Costo Total</th>
                                <th>% de Entrega</th>

                            </tr>
                        </thead>
                        <tbody id="modelos_selected">
                        </tbody>

                    </table>


                </div>

                <div class="row" style="margin-top : 5%;">
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label style="width : 130%;">
                            <label style="margin-right : 0.5%;">Sub Total:</label>
                            S/.<input type="number" step="any" style="width : 55%;" id="subTotalval" name="subTotalval" disabled="true" value="0">

                        </label>
                    </div>
                </div>

                <div class="row" style="margin-top : -0.8%;">
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label style="width : 130%;">
                            <label style="margin-right : 1%;">IGV (18%):</label>
                            S/.<input type="number" step="any" style="width : 55%;" id="igvSubtotal" name="igvSubtotal" disabled="true" value="0">
                        </label>
                    </div>
                </div>

                <div class="row" style="margin-top : -0.8%;">
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label style="width : 130%;">
                            <label style="margin-right : 7.5%;">Total:</label>
                            S/.<input value="0" type="text" step="any" style="width : 55%;" id="idTotal" name="idTotal" disabled="true">
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group  col-md-8 col-md-offset-8 col-xs-12"></div>
                    <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="observaciones">Observaciones</label>
                        <input type="text" id="observaciones" name="observaciones" maxlength="2000" class="form-control" disabled>
                        <input type="hidden" id="tipoDetalle" name="tipoDetalle" value="0" class="form-control">
                    </div>
                </div>

                <div class="ln_solid"></div>

                <div class="float-right mt-3 mb-3">
                    <a href="/Ventas/pedidos">
                        <button class="bttn-unite bttn-md bttn-danger mr-2" type="button">Cancelar</button>
                    </a>
                    <a href="/Ventas/pedidos">
                        <button class="bttn-unite bttn-md bttn-success" type="submit">Guardar</button>
                    </a>
                    <a href="/Ventas/pedidos">
                        <button class="bttn-unite bttn-md bttn-warning" type="submit">Entregar Todo</button>
                    </a>
                </div>

            </div>
            {!!Form::close()!!}

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script>
                $(document).ready(function() {
                    let tabla_modelos = $('#table_modelos').DataTable({
                        'columnDefs': [{
                            'targets': 0,
                            'checkboxes': {
                                'selectRow': true
                            }
                        }],
                        "language": {
                            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                        }
                    });

                    let pedidoCabecera = <?php echo json_encode($orden_pedido[0]); ?>;
                    let subTotal = pedidoCabecera.costo_total;
                    let igv = subTotal * 0.18;
                    let total = subTotal + igv;
                    $('#subTotalval').attr({
                        'value': subTotal
                    });
                    $('#igvSubtotal').attr({
                        'value': igv
                    });
                    $('#idTotal').attr({
                        'value': total
                    });
                    $('#observaciones').attr({
                        'value': pedidoCabecera.observaciones
                    });

                    let modeloDet = <?php echo json_encode($orden_pedidos_ventas_detalles); ?>;
                    let contador = 0;

                    $.each(modeloDet, function(key, value) {
                        let rowHTML = `<tr>
                                            <input name="idDetalle[]" type="hidden" value="${value.id}">
                                            <td><img src="${value.imagen}" alt="Foto modelo" width="60"></td>
                                            <td>${value.modelo_id}</td>
                                            <input name="codModelo[]" type="hidden" value="${value.modelo_id}">
                                            <input name="descModelo[]" type="hidden" value="${value.modelo_id}">
                                        `;
                        let count = 0;
                        let cantxTalla = [value.pedido_cantidad_t1, value.pedido_cantidad_t2, value.pedido_cantidad_t3, value.pedido_cantidad_t4, value.pedido_cantidad_t5, value.pedido_cantidad_t6, value.pedido_cantidad_t7];

                        let faltante = 0;
                        for (var i = 0; i < cantxTalla.length; i++) {
                            faltante = faltante + cantxTalla[i];
                        }
                        value.tallas.forEach(t => {
                            rowHTML += `<td>
                                            <div style="width: 50px; display: flex; flex-direction: column; align-items: center;">
                                                <span>T${t}</span>
                                                <input type="hidden" step="1" id="cantidadTalla-${value.id}-${t}-hidden"  name="cantidad-por-talla-${count}" value="${cantxTalla[count]}" style="width: 50px;">
                                                <input type="hidden" step="1"  name="cantidad-por-talla-original-${count}" value="${cantxTalla[count]}" style="width: 50px;">
                                                <input class="inputTalla" type="number" step="1" id="cantidadTalla-${value.id}-${t}"  name="cantidad-por-talla-${count}" placeholder="${cantxTalla[count]}" value="" style="width: 50px;">
                                            </div>
                                        </td>
                                        `;
                            count++;
                        });

                        for (let i = 0; i < 7 - value.tallas.length; i++) {
                            rowHTML += "<td>-</td>";
                        }
                        let porcentaje = (parseFloat(value.cantidad_total_pares) / parseFloat(100)) * (value.cantidad_total_pares - faltante);
                        rowHTML += `<td>
                                        <span style="text-align: center;" > ${value.cantidad_total_pares} </span>
                                        <input type="hidden"   name="cantidad-pedida-${contador}" value="${value.cantidad_total_pares}" >
                                        <input type="hidden" name="cantidad-tallas-${value.cod_producto_terminado}" value="${value.tallas.length}">
                                        </td>
                                        <td>
                                        <span style="text-align: center;" id="cantidad-por-talla-${value.id}-entregada" >${value.cantidad_total_pares - faltante} </span>
                                        <input class ="totalesXdetalle" type="hidden" id="cantidadEntrega-${value.id}-hidden"  name="cantidad-entregada-${contador}" value="${value.cantidad_total_pares - faltante}" ">

                                        </td>
                                        <td>
                                        <span class ="totalModelo" id="cantidad-por-talla-${value.id}-faltante"   ">${faltante}</span>
                                        <input class ="totalesXdetalle" type="hidden" id="cantidadEntrega-${value.id}-hidden"  name="cantidad-faltante-${contador}" value="${faltante}" ">
                                        </td>
                                        <td>
                                        <span  id="cantidad-por-talla-${value.id}-costo-total"   ">${value.valor_total}</span>
                                        <input class ="totalesXdetalle" type="hidden" step="1" id="cantidadCosto-${value.id}-hidden"  name="cantidad-costo-${contador}" value="${value.valor_total}" style="width: 50px;">
                                        </td>
                                        <td>
                                        <span class ="totalModelo" id="cantidad-por-talla-${value.id}-per-entrega"   ">${porcentaje}%</span>
                                        </td>
                                    </tr>`;

                        const tr = $(rowHTML);

                        tabla_modelos.row.add(tr[0]).draw();
                        let index = 0;
                        value.tallas.forEach(t => {

                            $(`#cantidadTalla-${value.id}-${t}`).change(function() {
                                valorInput = $(`#cantidadTalla-${value.id}-${t}`).val();

                                const tallaIndex = value.tallas.findIndex((element) => element == t);

                                if (valorInput > cantxTalla[tallaIndex] || valorInput < 0) {
                                    document.getElementById(`cantidadTalla-${value.id}-${t}`).setAttribute('style', 'color:red; width:50px;');
                                } else {
                                    document.getElementById(`cantidadTalla-${value.id}-${t}`).setAttribute('style', 'color:black; width:50px;');
                                    let valCant = cantxTalla[tallaIndex] - parseInt(valorInput);
                                    console.log('valora gardar', valCant)
                                    document.getElementById(`cantidadTalla-${value.id}-${t}-hidden`).setAttribute('value', valCant);
                                }

                                let cantidadEntrega = 0;
                                value.tallas.forEach(tt => {

                                    if ($(`#cantidadTalla-${value.id}-${tt}`).val() > 0) {
                                        cantidadEntrega = parseFloat(cantidadEntrega) + parseFloat($(`#cantidadTalla-${value.id}-${tt}`).val());
                                    }
                                });

                                document.getElementById(`cantidadEntrega-${value.id}-hidden`).setAttribute('value', cantidadEntrega);

                                let precioDetEntrega = parseFloat(value.precio) * cantidadEntrega; //precio de cada detalle a entregar
                                document.getElementById(`cantidadCosto-${value.id}-hidden`).setAttribute('value', precioDetEntrega);

                                let etregadaAct = value.cantidad_total_pares - faltante + cantidadEntrega;
                                document.getElementById(`cantidad-por-talla-${value.id}-entregada`).innerHTML = etregadaAct;
                                let faltanteAct = faltante - cantidadEntrega;
                                document.getElementById(`cantidad-por-talla-${value.id}-faltante`).innerHTML = faltanteAct;
                                let porcentajeAct = (parseFloat(etregadaAct) * parseFloat(100)) / (value.cantidad_total_pares);
                                document.getElementById(`cantidad-por-talla-${value.id}-per-entrega`).innerHTML = `${porcentajeAct} %`;
                            });
                        });
                        contador++;
                    });
                });
            </script>

        </div>
    </div>
    <style>
        .inputTalla::placeholder {
            color: #1D89FF;
        }
    </style>
</div>

@endsection
