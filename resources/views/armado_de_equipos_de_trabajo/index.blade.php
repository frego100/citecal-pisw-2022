
@extends ('layouts.admin')
@section ('contenido')
<!--
   Desarrollado por
   Jesús Albino Calderón
   México
   ITSZO && UNSA
-->
<head>
        <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

        <script>
                var obj = null;
        
                function viewHide(id) {
                    var targetId, srcElement, targetElement;
                    var targetElement = document.getElementById(id);
                    if (obj != null)
                        obj.style.display = 'none';
                    obj = targetElement;
                    targetElement.style.display = "";
                }
            </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>


<div>
    <h1 class="font-weight-bold">Armado de Equipos de Trabajos
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </h1>
    <h5>Orden Producción {{$id_orden_pedido_produccion}}</h5>

</div>
<div>

        <table>
                <tr>
                    <td>
                        <table class="armado" border="1px" >
                            <tr>
                                <td colspan="2">
                                    <button id="corte_cuero" Onclick="viewHide('corteCuero');">Corte Cuero</button>
                                    <button id="corte_forro" Onclick="viewHide('corteForro');">Corte Forro</button>
                                    <button id="corte_falso">Corte Falsas</button>
                                    <button id="corte_planilla">Corte Planillas</button>
                                    <button id="pintar">Pintar Bordes</button>
                                    <button id="debastar">Desbastar</button>
                                    <button id="requemar">Requemar y Marcar</button>
                                    <button id="capellanas">Capellanas</button>
                                    <button id="apardo">Aparado Plantillas</button>
                                </td>
                            </tr>
                            <tr>
                                <td class="txtformularios" width="60%">
                            <!------------------------------------------------------------------------------------>
                            <!--tabla encabezado de CC-->
                                    <table class="table table-borderless" width="100%">
                                        <thead>
                                            <tr>
                                                <!- encabezado de tabla ->
                                                    <td colspan="4">
                                                        <h6>C.C.</h6>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <th width="25%">Grupos</th>
                                                <th width="25%"></th>
                                                <th width="25%"></th>
                                                <th width="25%">Tiempo</th>
                                            </tr>
                                        </thead>
                                    </table>
                            <!------------------------------------------------------------------------------------>

                            <!--tabla prueba donde se muestran los datos de corte Cuero-->
                                        <table class="table table-borderless" id="corteCuero" style="display:none" width="100%">
                                        <tr>
                                            <td width="25%"><label><input name="regdom" type="radio" Onclick="viewHide('tabla6')" value="A">
                                                Seleecion A </label></td>
                                            <td width="25%">Seleecion A </td>
                                            <td width="25%"> Selct A </td>
                                            <td width="25%"> 6 </td>
                                        </tr>
                                        <tr>
                                            <td width="25%"><input name="regdom" type="radio" Onclick="viewHide('tabla7')" value="B">
                                                Selecci&oacute;n B </td>
                                            <td width="25%">Seleecion B </td>
                                            <td width="25%"> Selct B </td>
                                            <td width="25%"> 6 </td>
                                        </tr>
                                        <tr>
                                            <td width="25%"><input name="regdom" type="radio" Onclick="viewHide('tabla8');" value="C">
                                                Selecci&oacute;n C </td>
                                            <td width="25%">Seleecion C </td>
                                            <td width="25%"> Selct C </td>
                                            <td width="25%"> 6 </td>
                                        </tr>
                                        <tr>
                                            <td width="25%"><input name="regdom" type="radio" Onclick="viewHide('tabla9');" value="D">
                                                Seleccion D </td>
                                            <td width="25%">Seleecion D </td>
                                            <td width="25%"> Selct D </td>
                                            <td width="25%"> 6 </td>
                                        </tr>
                                    </table>
                            <!------------------------------------------------------------------------------------>
                           


                            <!------------------------------------------------------------------------------------>
                            <!--tabla prueba donde se muestran los datos de corte forro-->
                                <table class="table table-borderless" id="corteForro" style="display:none" width="100%">
                                <tr>
                                    <td width="25%"><label><input name="regdom" type="radio" Onclick="viewHide('tabla6')" value="A">
                                        Seleecion f </label></td>
                                    <td width="25%">Seleecion A </td>
                                    <td width="25%"> Selct A </td>
                                    <td width="25%"> 6 </td>
                                </tr>
                                <tr>
                                    <td width="25%"><input name="regdom" type="radio" Onclick="viewHide('tabla7')" value="B">
                                        Selecci&oacute;n B </td>
                                    <td width="25%">Seleecion B </td>
                                    <td width="25%"> Selct B </td>
                                    <td width="25%"> 6 </td>
                                </tr>
                                <tr>
                                    <td width="25%"><input name="regdom" type="radio" Onclick="viewHide('tabla8');" value="C">
                                        Selecci&oacute;n C </td>
                                    <td width="25%">Seleecion C </td>
                                    <td width="25%"> Selct C </td>
                                    <td width="25%"> 6 </td>
                                </tr>
                                <tr>
                                    <td width="25%"><input name="regdom" type="radio" Onclick="viewHide('tabla9');" value="D">
                                        Seleccion D </td>
                                    <td width="25%">Seleecion D </td>
                                    <td width="25%"> Selct D </td>
                                    <td width="25%"> 6 </td>
                                </tr>
                            </table>
                                    
                                </td>
                            <!------------------------------------------------------------------------------------>
                            
                                <td width="40%">
                                    
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <!-- encabezado de tabla -->
                                                <td colspan="4">
                                                    <h6>Datos de Modelo a Considerar</h6>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Clasificacón del Modelo</td>
                                                <td>A</td>
                                            </tr>
                                            <tr>
                                                <td>Material</td>
                                                <td>
                                                        <script type="text/javascript">
                                                            let materiales = @json($nombres_materiales);
                                                           
                                                     
                                                            var i=0
                                                            for (i=0;i<{{$tamaño_nombres_modelos}};i++){
                                                                
                                                             document.write (materiales[i] )
                                                             document.write("<br />")
                                                            }
                                                            </script>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>N° de Piezas</td>
                                                <td> {{$suma}} </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </th>
                            </tr>
                            </thead>
                            </tbody>
                        </table>
                    </td>
                            <!------------------------------------------------------------------------------------>
                </tr>

                            <!------------------------------------------------------------------------------------>
                            <!--barra de estado-->
                <tr>
                            @if($datos==0)
                            <td v>
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 0%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">0%</div>
                                </div>
                            </td>
                            @elseif($datos==1)
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 11.1% " aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">11.1%</div>
                                </div>
                            </td>
                            @elseif($datos==2)
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 22.2%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">22.2%</div>
                                </div>
                            </td>
                            @elseif($datos==3)
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 33.3%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">33.3%</div>
                                </div>
                            </td>
            
                            @elseif($datos==4)
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 44.4%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">44.4%</div>
                                </div>
                            </td>
            
                            @elseif($datos==5)
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 55.5%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">55.5%</div>
                                </div>
                            </td>
                            @elseif($datos==6)
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 66.6%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">66.6%</div>
                                </div>
                            </td>
                            @elseif($datos==7)
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 77.7%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">77.7%</div>
                                </div>
                            </td>
                            @elseif($datos==8)
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 88.8%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">88.8%</div>
                                </div>
                            </td>
                            @elseif($datos==9)
                            <td colspan="2">
                                <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100">100%</div>
                                </div>
                            </td>
                            @endif
                </tr>
                            <!------------------------------------------------------------------------------------>
                
                            <!------------------------------------------------------------------------------------>
                            <!--tabla donde se muestran los grupos seleccionados-->
                    <td colspan="2">
                        <h6>Equipo de Producción Armado</h6>
                        <table  width="100%" border="1">
                            <tr>
                                <thead>
                                    <th width="25%">ACTIVIDAD</th>
                                    <th width="25%">GRUPO</th>
                                    <th width="25%">TIEMPO</th>
                                    <th width="25%">CLASIFICACIÓN</th>
                                </thead>
                            </tr>
                        </table>
                        <table id=tabla6 style="display:none" width="100%" border="1">
                            <tr>
                                <td width="25%">corte A</td>
                                <td width="25%">jdsahkjh</td>
                                <td width="25%">59.4</td>
                                <td width="25%">A</td>
                            </tr>
                        </table >
                        <table id=tabla7 style="display:none" width="100%" border="1">
                            <tr>
                                    <td width="25%">corte B</td>
                                    <td width="25%">wdqdwq</td>
                                    <td width="25%">9.4</td>
                                    <td width="25%">B</td>
                                </tr>
                            </tr>
                        </table>
                        <table id=tabla8 style="display:none" width="100%" border="1">
                            <tr>
                                    <td width="25%">corte C</td>
                                    <td width="25%">das</td>
                                    <td width="25%">5.4</td>
                                    <td width="25%">C</td>
                                </tr>
                            </tr>
                        </table>
                        <table id=tabla9 style="display:none" width="100%" border="1">
                            <tr>
                                    <td width="25%">corte D</td>
                                    <td width="25%">fasd</td>
                                    <td width="25%">4</td>
                                    <td width="25%">D</td>
                                </tr>
                            </tr>
                        </table>
                    </td>
                    <!------------------------------------------------------------------------------------>
      
                
            </table>
            </td>
            </tr>
            </table>
    @endsection
