<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
    id="modal-conf-{{$modelos->cod_modelo}}">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo_elim">Acción no permitida</h4>
            </div>
            <div class="modal-body">
                <p>No puede ingresar a las combinaciones con un modelo desactivado. Primero active el modelo y podrá
                    acceder a sus combinaciones</p>
           
            </div>
            <div class="modal-footer">
              
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
    
</div>