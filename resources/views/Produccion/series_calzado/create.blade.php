@extends ('layouts.admin')
@section ('contenido')
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Nueva Serie de Tallas</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'Produccion/series_calzado','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}


                    <div class="row">
                      <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="nombreSerie">Nombre de Serie</label>
                         <input type="text" id="nombreSerie" name="nombreSerie" required="required" maxlength="30" class="form-control ">
                      </div>
                    </div>
          <div class="row">
                      <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="tallaInicial">Talla Inicial</label>
                         <input onkeypress="return isNumberKey(event)" maxlength="2" required="required" type="text" id="tallaInicial" name="tallaInicial" class="form-control ">
                      </div>
                    </div>
<div class="row">
                      <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                        <label for="tallaFinal">Talla Final</label>
                         <input onkeypress="return isNumberKey(event)" maxlength="2" type="text" id="tallaFinal" name="tallaFinal" required="required" class="form-control ">
                      </div>
                    </div>


                          <div class="ln_solid"></div>
                      <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                            <a href="{{ url('Produccion/series_calzado') }}"><button type="button"
                                class="bttn-unite bttn-md bttn-danger ">Cancelar</button></a>
                              <button type="submit" class="bttn-unite bttn-md bttn-success ">Guardar</button>
                            </div>
                          </div>
          {!!Form::close()!!}
        </div>
      </div>
    </div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
