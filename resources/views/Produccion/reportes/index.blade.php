@extends ('layouts.admin2')
@section ('contenido')



<div>
<h3 class="font-weight-bold">Produccion Mensual y datos generales {{date("Y")}}-{{date("n")}}-{{date("d")}}<a href="orden_produccion/create">
  </a></h3>
</div>
<div class="x_content table-responsive">

    <br>
    <br>
@if($primera_vez==0)
<table id="example2" class="display" style="width:100%">
    <thead>
      <tr>
        <th>Linea</th>
        @if($añoprevio!="")
        <th>Previo Octubre</th>
        <th>Previo Noviembre</th>
        <th>Previo Diciembre</th>
        @endif
        <th>Ene</th>
        <th>Feb</th>
        <th>Mar</th>
        <th>Abr</th>
        <th>May</th>
        <th>Jun</th>
        <th>Jul</th>
        <th>Ago</th>
        <th>Sep</th>
        <th>Oct</th>
        <th>Nov</th>
        <th>Dic</th>
        <th>Pares al mes</th>
        <th>Doc. al mes</th>
        <th>%</th>
        <th>Ver Modelos</th>


      </tr>
    </thead>
    <tbody>
<?php $ten=$tfeb=$tmar=$tabr=$tmay=$tjun=$tjul=$tago=$tset=$toct=$tnov=$tdic=$tpoct=$tpnov=$tpdic=0;
?>
@for($i =0;$i < sizeof($linea) ; $i++)

      <tr>
        <td>{{$linea[$i]->nombre_linea}}</td>
        @if($añoprevio!="")
        <td>{{$previo_octubre[$i]}}</td>
        <td>{{$previo_noviembre[$i]}}</td>
        <td>{{$previo_diciembre[$i]}}</td>
        <?php
        $tpoct=$tpoct+$previo_octubre[$i];
        $tpnov=$tpnov+$previo_noviembre[$i];
        $tpdic=$tpdic+$previo_diciembre[$i];
        ?>
        @endif
        <td>{{$enero[$i]}}</td>
        <td>{{$febrero[$i]}}</td>
        <td>{{$marzo[$i]}}</td>
        <td>{{$abril[$i]}}</td>
        <td>{{$mayo[$i]}}</td>
        <td>{{$junio[$i]}}</td>
        <td>{{$julio[$i]}}</td>
        <td>{{$agosto[$i]}}</td>
        <td>{{$setiembre[$i]}}</td>
        <td>{{$octubre[$i]}}</td>
      <!--  <td>Nov</td>-->

        <td>{{$noviembre[$i]}}</td>

        <td>{{$diciembre[$i]}}</td>


        <td>{{$totalpares[$i]}}</td>


        <td>{{$doc[$i]}}</td>

        <td>{{$porcentaje[$i]}}%</td>
        <?php
         $ten=$ten+$enero[$i];
         $tfeb=$tfeb+$febrero[$i];
         $tmar=$tmar+$marzo[$i];
         $tabr=$tabr+$abril[$i];
         $tmay=$tmay+$mayo[$i];
         $tjun=$tjun+$junio[$i];
         $tjul=$tjul+$julio[$i];
         $tago=$tago+$agosto[$i];
         $tset=$tset+$setiembre[$i];
         $toct=$toct+$octubre[$i];
         $tnov=$tnov+$noviembre[$i];
         $tdic=$tdic+$diciembre[$i];
        ?>
        <td>
          <a href="">
           <button class="bttn-unite bttn-md bttn-primary "><i class="fas fa-eye"></i></button>
         </a>
       </td>
      </tr>
    @endfor

    </tbody>
      <tfoot>
        <tr>
          <td>-</td>
          @if($añoprevio!="")
          <td>{{$tpoct}}</td>
          <td>{{$tpnov}}</td>
          <td>{{$tpdic}}</td>
          @endif
          <td>{{$ten}}</td>
          <td>{{$tfeb}}</td>
          <td>{{$tmar}}</td>
          <td>{{$tabr}}</td>
          <td>{{$tmay}}</td>
          <td>{{$tjun}}</td>
          <td>{{$tjul}}</td>
          <td>{{$tago}}</td>
          <td>{{$tset}}</td>
          <td>{{$toct}}</td>
          <td>{{$tnov}}</td>
          <td>{{$tdic}}</td>
          <td>{{$totalaso}}</td>
          <td>{{$totaldoc}}</td>
          <td>100%</td>
          <td>-</td>
        </tr>
      </tfoot>
      </table>
      <?php $prom=0; $mes=date("n");
        $tproduc;
        if($añoprevio!="")
        {
          $tproduc[0]=$tpoct;
          $tproduc[1]=$tpnov;
          $tproduc[2]=$tpdic;
          $tproduc[3]=$ten;
          $tproduc[4]=$tfeb;
          $tproduc[5]=$tmar;
          $tproduc[6]=$tabr;
          $tproduc[7]=$tmay;
          $tproduc[8]=$tjun;
          $tproduc[9]=$tjul;
          $tproduc[10]=$tago;
          $tproduc[11]=$tset;
          $tproduc[12]=$toct;
          $tproduc[13]=$tnov;
          $tproduc[14]=$tdic;
        }
        else {
          $tproduc[0]=$ten;
          $tproduc[1]=$tfeb;
          $tproduc[2]=$tmar;
          $tproduc[3]=$tabr;
          $tproduc[4]=$tmay;
          $tproduc[5]=$tjun;
          $tproduc[6]=$tjul;
          $tproduc[7]=$tago;
          $tproduc[8]=$tset;
          $tproduc[9]=$toct;
          $tproduc[10]=$tnov;
          $tproduc[11]=$tdic;
        }
        if($mes<=3)
        {
          if ($añoprevio!="") {
            $mes=$mes+2;
          }
          else {
            $mes=$mes-1;
          }
          for ($i=0; $i < $mes ; $i++) {
            $prom=$prom+($tproduc[$i]/3);
          }
        }
        else
        {
          $mes=$mes-1;
          for ($i=0; $i < $mes ; $i++) {
            $prom=$prom+($tproduc[$i]/3);
          }
        }
      ?>
      {!!Form::open(array('url'=>'Produccion/reportes','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
      <div class="form-group">
        <label for="">Promedio de Produccion:</label>
        <input type="text" disabled class="form-class" id="" value="{{$prom}}" placeholder="">
        <input type="text" style="display:none" name="promedio" class="form-class" id="" value="{{$prom}}" placeholder="">
      </div>
      <div class="">
          <button type="submit" class="bttn-unite bttn-md bttn-primary "> Actualizar </button>
      </div>
      {!!Form::close()!!}
@else
<p>No posee data previa por favor ingrese su produccion mensual</p>

@endif
</div>






@endsection
