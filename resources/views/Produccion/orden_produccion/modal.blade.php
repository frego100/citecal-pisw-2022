<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$op->cod_op}}">
	{{Form::Open(array('action'=>array('OrdenProduccionController@destroy',$op->cod_op
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Artículo</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea Eliminar el artículo</p>
				<input type="text" style="display:none" name="email" value="{{$op->cod_op}}">
        <input type="text" style="display:none" name="estado" value="0">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Reserve a Table</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form class="form-horizontal" role="form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>Number of Guests</h5>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="guests" id="guests" value="1"> 1
                          </label>
                        </div>
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="guests" id="guests" value="2"> 2
                          </label>
                        </div>
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="guests" id="guests" value="3"> 3
                          </label>
                        </div>
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="guests" id="guests" value="4"> 4
                          </label>
                        </div>
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="guests" id="guests" value="5"> 5
                          </label>
                        </div>
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="guests" id="guests" value="6"> 6
                          </label>
                        </div>
                    </div>
                </div>
            <hr>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>Section</h5>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <div class="btn-group" data-toggle="buttons">
                          <label class="btn btn-success">
                            <input type="radio" name="section" id="section" autocomplete="off"> Non-Smoking
                          </label>
                          <label class="btn btn-danger">
                            <input type="radio" name="section" id="section" autocomplete="off"> Smoking
                          </label>
                        </div>
                    </div>
                </div>
            <hr>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>Date and time</h5>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="input-group">
                                  <span class="input-group-addon">#</span>
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="input-group">
                                  <span class="input-group-addon">#</span>
                                  <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <hr>
                <div class="form-group">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 offset-lg-3 offset-md-3 offset-sm-3 offset-xs-3">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Reserve</button>
                    </div>
                </div>
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p><strong>Warning:</strong> Please <a href="tel:85212345678" class="alert-link">call</a> us to reserve for more than six guests</p>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
