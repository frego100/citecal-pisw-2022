@extends ('layouts.admin')
@section ('contenido')
<div align="center">
    <h2 class="font-weight-bold">Modelos Base

    </h2>
</div>
<div style="margin-top:2%;margin-bottom:3%;">
    <a href="ficha_calzado/create">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Agregar Modelo</button></a>
    <br>
</div>

<br>


<div class="x_content table-responsive">
    <table id="tablaModelos" class="display">
        <thead align="center">
            <tr>

                <th>Imagen</th>
                <th>Coleccion</th>
                <th>Linea</th>
                <th>Serie</th>
                <th>Código de Modelo</th>
                <th>Descripción de Modelo</th>
                <th>Combinaciones</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody align="center">
            @foreach ($LineasModelos as $modelos)
            <tr>
                <td width="10" height="10">
                    {{Html::image('photo/modelos/'.$modelos->RUC_empresa.'/'.$modelos->imagen,'alt',array('width' => 70, 'height' => 70 )) }}
                </td>
                <td> {{ $modelos->nombre_coleccion }} </td>
                <td> {{ $modelos->nombre_linea }} </td>
                <td> {{ $modelos->nombre_serie }}</td>
                <td> {{ $modelos->codigo }} </td>
                <td> {{ $modelos->descripcion }} </td>
                <td align="center" class="row" >
                <br>
                    <div class="form-group  col-md-1  col-xs-12" >

                    </div>
                    <div class="form-group  col-md-6  col-xs-12" style="margin-top: 5%;" >
                      ({{ $modelos->comb_aprobado }})aprobado<br>({{ $modelos->comb_proceso }})proceso
                    </div>
                    <div class="form-group  col-md-4  col-xs-12" style="margin-top: 5%;" >
                    <a class="a" id="{{ $modelos->cod_modelo}}"
                        href="/Produccion/combinacion_ficha/listado/{{$modelos->cod_modelo}}">
                        <button class="bttn-unite bttn-md bttn-primary"><i class="fa fa-palette"></i></button></a>
                    </div>
                </td>

                <td align="center">
                <a class="recepcion-talla" id="" href="#" data-target="#modal-delete-{{$modelos->cod_modelo}}"
                    data-toggle="modal">
                        <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                </td>


            </tr>
            @include('Produccion.ficha_modelo.modaleliminar')
            @endforeach
        </tbody>
    </table>
</div>


<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete">
    {{Form::Open(array('action'=>array('ModelosCalzadoController@destroy','2'),'method'=>'delete'))}}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo_elim">Eliminar Modelo</h4>
            </div>
            <div class="modal-body">
                <p>Confirme si desea Eliminar el Modelo</p>
                <input type="text" style="display:none" name="email" id="cod_elim" value="">
                <input type="text" style="display:none" name="estado" value="0">
            </div>
            <div class="modal-footer">
                <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
    {{Form::Close()}}
</div>

<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-active">
    {{Form::Open(array('action'=>array('ModelosCalzadoController@destroy',"3"   ),'method'=>'delete'))}}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo_act">Activar Modelo</h4>
            </div>
            <div class="modal-body">
                <p>Confirme si desea Activar el Modelo</p>
                <input type="text" style="display:none" name="email" id="cod_act">
                <input type="text" style="display:none" name="estado" value="1">
            </div>
            <div class="modal-footer">
                <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
    {{Form::Close()}}

</div>

<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-imagen">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo_imagen"></h4>
            </div>
            <div class="modal-body">
                <div class="border border-dark" align="center">
                    <img id="imagen_imagen" style="width:400px; height:400px" alt="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

<script type="text/javascript">
var data = <?php echo $LineasModelos;?>;
var t = $("#tabla").DataTable();
var t1 = $("#tabla").DataTable();
var tablaModelos = $("#tablaModelos").DataTable();
var listaModelos = <?php echo $LineasModelos; ?>;
console.log(listaModelos);

//USO DE AJAX PARA ABRIR UNA NUEVA VENTANA CON EL ID
$("#tablaModelos").on('click', 'a.combinacion', function() {

    var id = $(this).attr("id");
    console.log(id);


    $.ajax({
        url: "combinacion_calzado/" + id,
        success: function(html) {

            $.each(html, function(key, value) {
                auxiliar = 1;

            });
        }
    });

});


</script>
@endsection
