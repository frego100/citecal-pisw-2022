
<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-edit-{{$modelosC->cod_modelo}}">
    {{Form::Open(array('action'=>array('ModelosCalzadoController@update',$modelosC->cod_modelo),'files'=>true,'method'=>'patch'))}}
    <div class="modal-dialog"  style="max-width: 80%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo_mod">Modificar Modelo: </h4>
                <input type="hidden" id="{{$modelosC->cod_modelo}}" value="{{$modelosC->cod_modelo}}" name="codigo">
                <input type="hidden" id="{{$modelosC->cod_modelo}}" value="combinacion" name="urlCod">
                <input type="hidden" id="{{$modelosC->cod_modelo}}" value="1" name="bandera">
            </div>
            <div class="modal-body">
                <div class="x_content">

                <div class="row">
                        <div class="form-group  col-md-4  col-xs-12">
                            <div class="">
                                <label>Coleccion:</label>

                                <select name="coleccion" required='required' class="custom-select">
                                    <option value="" selected disabled>--- Seleccionar Coleccion ---</option>
                                    @foreach ($coleccion as $col)
                                      @if($col->codigo_coleccion==$modelosC->cod_coleccion)
                                      <option value="{{$col->codigo_coleccion}}" selected>
                                          {{$modelosC->nombre_coleccion}}
                                      </option>
                                      @else
                                      <option value="{{$col->codigo_coleccion}}">
                                          {{$col->nombre_coleccion}}
                                      </option>
                                      @endif
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="">
                                <label>Serie:</label>
                                <select name="serie" required='required' class="custom-select">
                                    <option value="" selected disabled>--- Seleccionar Serie ---</option>
                                    @foreach ($serie as $series)
                                      @if($series->cod_serie==$modelosC->cod_serie)
                                        <option value="{{$series->cod_serie}}" selected>
                                            {{$series->nombre_serie}}
                                        </option>
                                        @else
                                        <option value="{{$series->cod_serie}}">
                                            {{$series->nombre_serie}}
                                        </option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>

                            <br>
                            <div class="">
                                <label for="codigo_calzado">Codigo de Modelo</label>
                                <input type="text" id="codigo_modelo" name="codigo_modelo" required="required" value="{{$modelosC->codigo}}"
                                    maxlength="40" class="form-control ">
                            </div>

                            <div class="">
                                <label for="descripcion_modelo">Descripcion de Modelo</label>
                                <input type="text" id="descripcion_modelo" name="descripcion_modelo" required="required" value="{{$modelosC->descripcion}}"
                                    maxlength="50" class="form-control ">
                            </div>
                        </div>
                        <div class="form-group  col-md-4 offset-md-2 col-xs-12">
                            <div class="">
                                <label>Linea:</label>
                                <select name="linea" required='required' class="custom-select">
                                    <option value="" selected disabled>--- Seleccionar Linea ---</option>
                                    @foreach ($Lineas as $linea)
                                      @if($linea->cod_linea==$modelosC->cod_linea)
                                          <option value="{{$linea->cod_linea}}" selected>
                                              {{$linea->nombre_linea}}
                                          </option>
                                          @else
                                          <option value="{{$linea->cod_linea}}">
                                              {{$linea->nombre_linea}}
                                          </option>
                                      @endif
                                    <option value="{{$linea->cod_linea}}">{{$linea->nombre_linea}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <br>
                            <div class="row">
                                <div class="btn-small amber darken-s" style="margin-left:3%;">
                                    <label for="exampleFormControlFile1">Imagen de modelo:</label>
                                    <input type="file" class="form-control-file" id="photo" name="photo"
                                        onchange="vista_preliminar(event)">
                                </div>
                                <div>
                                    <img src="" alt="" id="img-foto-{{$modelosC->cod_modelo}}" width="110" height="110" style="margin-left:6%;">
                                    </img>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    {{Form::Close()}}

</div>
<!--
<script>
        let vista_preliminar = (event) => {
            let leer_img = new FileReader();
            let id_img = document.getElementById('img-foto-{{$modelosC->cod_modelo}}');

            leer_img.onload = () => {
                if (leer_img.readyState == 2) {
                    id_img.src = leer_img.result
                }
            }
            leer_img.readAsDataURL(event.target.files[0])
        }
</script>
    -->
