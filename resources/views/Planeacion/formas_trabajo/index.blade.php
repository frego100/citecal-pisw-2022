@extends ('layouts.admin')
@section ('contenido')
<script type="text/javascript">

</script>

<div class="preloader">
</div>
<div>
  <h2 class="font-weight-bold">Formas de trabajo</h2>
  <hr style="border:3px solid:blank">
</div>
<h4 class="font-weight-bold">Seleccion de personal del proceso</h4>
<hr>
<div class="row" id="posicion_de_actualizacion">

  <div class="col-sm-5">
    <div class="card" style="height: 5rem;">
      <div class="card-body">

        <div class="row">
          <div class="col-md-3">
            <label for="inputCity">Proceso</label>
          </div>
          <div class="col-md-9">
            <select id="proceso" class="custom-select">
              <option value="default" selected disabled>Selecciona proceso</option>
              @foreach($default_processes as $def_pro)
              <option value="{{$def_pro}}">{{$def_pro}}</option>
              @endforeach
            </select>
          </div>
        </div>

      </div>
    </div>
  </div>

</div>
<!--End Checks sobre los filtros-->
<br>
<div class="x_content table-responsive">
  <table id="tabla_personal_proceso" class="display">
    <thead>
      <tr>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>Linea</th>
        <th>Especialidad</th>
        <th>Eficiencia</th>
      </tr>

    </thead>
  </table>
</div>
<br>
<div id="contenedor_botones_01">
  <button id="crear_grupo_trabajo" class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Realizado</button>
</div>
<!-- Codigo de Mario_Huaypuna .............................................................. -->
<br><br>
<div>
  <hr style="border:3px solid:blank">
</div>
<h4 class="font-weight-bold">Registro de equipos armados</h4>
<hr>
<div class="x_content table-responsive">
  <!--Begin Buscar-->
  <!--End buscar-->
  <!--Begin Checks sobre los filtros-->
  <!--Begin table-->
  <div id="reporte_tiempos_modelos" class="x_content table-responsive">
    <table id="tabla_equipos_armados" class="display" style=" width:100%">
      <thead>
        <tr>
          <th>Codigo</th>
          <th>Proceso</th>
          <th>Especialidad</th>
          <th>Nro de Integrantes</th>
          <th>Fecha</th>
          <th>Acciones</th>
        </tr>
      </thead>
    </table>
  </div>
  <!--End table-->
  <!-- other table -->
  <br>
</div>

<script type="text/javascript">
  //modelo     
  $(document).ready(function($) {

    var pp = $("#tabla_personal_proceso").DataTable({
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json",
        select: {
          rows: " - %d trabajadores seleccionados"
        }
      },
      dom: 'Bfrtip',
      select: true,
      data: [],
      columns: [
        { "data": "DNI_trabajador"},
        { "data": "nombre" },
        { "data": "puesto" },
        { "data": "especialidad" },
        { "data": "eficiencia" }
      ],
      select: {       
        style: 'multi'
      },
      buttons: [{
          text: 'Todos',
          action: function() {
            pp.rows().select();
          }
        },
        {
          text: 'Ninguno',
          action: function() {
            pp.rows().deselect();
          }
        }
      ],
      responsive: true

    });

    //Funciones de Mario_Huaypuna -------------------------------------------------------------------------------
    var ea = $("#tabla_equipos_armados").DataTable({
 
        ajax: { 
          url: "/planeacion/formas_trabajo/obtener_equipos_armados",
          dataSrc: ""
        },
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
        },
        columns: [
            { data: "codigo_grupo_trabajo" },
            { data: "proceso" },
            { data: "especialidad" },
            { data: "num_integrantes" },
            { data: "fecha_creacion" },
            {
                data: null,
                className: "center",
                orderable: false,
                defaultContent: 
                  `
                    <button class="editar_grupo bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button>

                    <button class="eliminar_grupo bttn-unite bttn-md bttn-primary"><i class="fab fa-bitbucket"></i></button>

                    <button class="ver_grupo bttn-unite bttn-md bttn-success "><i class="fas fa-eye"></i></button>
                  `
            }
        ]      
    });

    // Edit record
    $('#tabla_equipos_armados').on('click', 'button.editar_grupo', function (e) {
        e.preventDefault();
        var unEquipo = ea.row( $(this).closest('tr') ).data();
        //console.log( unEquipo.num_integrantes );

        // recargar tabla personal proceso
        $.ajax({
        url: "/planeacion/formas_trabajo/act_personal_proceso", 
        success: function(trabajadores) {
          pp.clear().draw();
          pp.rows.add(trabajadores).draw();
        }

        });
        //obtener listado de trabajadores de un grupo especifico y seleccionarlos en la tabla de personal proceso
        $.ajax({
        url: "/planeacion/formas_trabajo/obtener_trabajadores/"+ unEquipo.codigo_grupo_trabajo, 
        success: function(trabajadoresDeUnGrupo) {
          //console.log( trabajadoresDeUnGrupo );
          seleccionarTrabajadores( trabajadoresDeUnGrupo );
          var contenthtml = 
            `<button id="actualizar_grupo_trabajo" class="bttn-unite bttn-md bttn-success float-right mr-sm-3">Actualizar</button>
            <button id="cancelar_operacion" class="bttn-unite bttn-md bttn-primary float-right mr-sm-3">Cancelar</button>`
          $('#contenedor_botones_01').html(contenthtml);
        },
        error: function (){
          alert("No se pudo obtener trabajadores");
        }

        });
        //animacion para subir el scroll hacia la tabla de personal proceso
        $('html,body').animate({scrollTop: $('#posicion_de_actualizacion').offset().top}, 'slow');
        // inhabilitar las opciones en la tabla de equipos armados
        //ea.buttons().disable();
        //$('button.editar_grupo').prop('disabled', true);
        //$( 'button.editar_grupo' ).fadeTo( "slow", 0.4 );
        $( "button.editar_grupo" ).fadeOut( "slow" );
        $( "button.eliminar_grupo" ).fadeOut( "slow" );
        
        //$('button.editar_grupo').stop( true, true ).fadeOut();  
        //botones.disable(); //inactivar todos los botones de tabla equipos armados
    });

    $(document).on('click', '#exportar_reporte_tiempos_modelos', function() {
      //window.open('/Ventas/Reportes/ReporteClientes/exportar/'+data)
    });

    $('#contenedor_botones_01').on('click', '#cancelar_operacion', function() {
      //pp.rows().deselect();
      //$('#contenedor_botones_01').html(`<button id="crear_grupo_trabajo" class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Realizado</button>`);
      location.reload();
    });

    $('#contenedor_botones_01').on('click', '#actualizar_grupo_trabajo', function() {
      
    });

    $(document).on('click', '#crear_grupo_trabajo', function() {
      var trabajadores_seleccionados = pp.rows( { selected: true } ).data() ;
      console.log(trabajadores_seleccionados);
      console.log(trabajadores_seleccionados.length );
    });

    $("#proceso").change(function() {

      let selected_process = $("#proceso").val();

      $.ajax({
        url: "/planeacion/formas_trabajo/act_personal_proceso",
        success: function(trabajadores) {
          pp.clear().draw();
          pp.rows.add(trabajadores).draw();

        }
      })

    });

    function cargarRegistroEquiposArmados(){
      ea.ajax.reload();
    }

    function seleccionarTrabajadores( trabajadoresDeUnGrupo ){
      for( i=0; i < trabajadoresDeUnGrupo.length ; i++){
        //buscar en la primera columna el codigo de cierto trabajador
        var indext = pp.column( 0 ).data().indexOf( trabajadoresDeUnGrupo[i].codigo_grupo_trabajador );
        if( indext >= 0 ){
          pp.row( indext ).select();
        }    
      } 
    }

  });
</script>

@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>

