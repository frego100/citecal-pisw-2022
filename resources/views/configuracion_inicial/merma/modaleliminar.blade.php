<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
    id="modal-eliminar-{{ $merma->id }}">
    {{     Form::Open(['action' => ['AlmacenController@destroy', $merma->id], 'method' => 'delete']) }}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Desactivar Almacen</h4>
            </div>
            <div class="modal-body">
                <p>Confirme si desea Eliminar la merma?</p>
                <input type="text" style="display:none" name="email" value="{{ $merma->id }}">
                <input type="text" style="display:none" name="estado" value="0">
            </div>
            <div class="modal-footer">
                <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
    {{ Form::Close() }}

</div>
