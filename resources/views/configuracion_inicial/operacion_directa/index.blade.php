@extends ('layouts.admin')
@section('contenido')
    <div class="preloader">
    </div>
    <div style="text-align: center; margin: 2% 0 2% 0">
        <h3 class="font-weight-bold">Operaciones Directas de la Empresa
        </h3>

        <div>
            <a href="operacion_directa/create">
                <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"
                    style="text-align: center; margin: 0 0 2% 0">Nueva Operacion</button></a>
        </div>
        <div class="x_content table-responsive">
            <table id="operacion_mano" class="display">
                <thead>
                    <tr align="center">
                        <th>Proceso</th>
                        <th>Operacion</th>
                        <th>Tipo Operacion</th>
                        <th>Unidad Medida</th>
                        <th>Costo</th>
                        <th>Beneficio</th>
                        <th>Costo por Par</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody align="center">
                    @foreach ($operaciones as $op)
                        <tr>
                            <td>{{ $op->nombre }}</td>
                            <td>{{ $op->operacion_nombre }}</td>
                            <td>{{ $op->nombre_pago }}</td>
                            <td>{{ $op->unidad }}</td>
                            <td>S/. {{ $op->costo }}</td>
                            @if ($op->beneficio == null)
                                <td>NA</td>
                            @else
                                <td>S/. {{ $op->beneficio }}</td>
                            @endif
                            <td>S/. {{ $op->costo_par }}</td>
                            <td><a class="editar" id="{{ $op->cod_operacion_d }}" href="#"
                                    data-target="#modal-edit-{{ $op->cod_operacion_d }}" data-toggle="modal"><button
                                        class="bttn-unite bttn-md bttn-warning "><i class="far fa-edit"></i></button></a>
                            </td>
                            <td> <a class="" id="" href="#"
                                    data-target="#modal-delete-{{ $op->cod_operacion_d }}" data-toggle="modal">
                                    <button class="bttn-unite bttn-md bttn-danger"><i
                                            class="fas fa-trash-alt"></i></button></a></td>
                        </tr>
                        @include('configuracion_inicial.operacion_directa.modaleliminar')
                        @include('configuracion_inicial.operacion_directa.modaleditar')
                    @endforeach
                </tbody>
            </table>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>


        <script type="text/javascript">
            var tipo = <?php echo $tipo; ?>;
            var empresaP = <?php echo $empresaP; ?>;
            var proceso = <?php echo $proceso; ?>;


            $("#operacion_mano").on('click', 'a.editar', function() {
                let idtipo = 0;
                var id = $(this).attr("id");
                $("#costos" + id).keyup(function() {
                    console.log(id)
                    let tipoOperacion = $("#tipo_operacio" + id).val();
                    //DESTAJO

                    if ($("#tipo_operacio" + id).val() == 1) {
                        $("#beneficio").val(0);
                        destajo();

                    }
                    //JORNAL
                    else if ($("#tipo_operacio" + id).val() == 2) {

                        $("#beneficio" + id).val(0);
                        jornal();

                    }
                    //PLANTILLA
                    else if ($("#tipo_operacio" + id).val() == 3) {
                        let costos = $("#costos" + id).val();
                        $("#beneficio" + id).val(costos * (empresaP[0].porcentaje_beneficio / 100));
                        planilla();
                    }


                });

                $("#otros_costos" + id).keyup(function() {

                    //DESTAJO
                    if ($("#tipo_operacio" + id).val() == 1) {
                        $("#beneficio").val(0);
                        destajo();

                    }
                    //JORNAL
                    else if ($("#tipo_operacio" + id).val() == 2) {
                        jornal();
                        $("#beneficio").val(0);

                    }
                    //PLANTILLA
                    else if ($("#tipo_operacio" + id).val() == 3) {
                        let costos = $("#costos" + id).val();

                        planilla();
                    }


                });

                function destajo() {

                    var costos = $("#costos" + id).val();

                    let otros_costos = $("#otros_costos" + id).val();
                    let costo_par_total = (costos / 12) + (otros_costos / empresaP[0].produccion_promedio)
                    $("#costo_par" + id).val((costo_par_total).toFixed(4));
                }

                function jornal() {

                    let costos = $("#costos" + id).val();

                    let otros_costos = $("#otros_costos" + id).val();

                    let costo_par_total = (costos * 4 / empresaP[0].produccion_promedio) + (otros_costos /
                        empresaP[0].produccion_promedio)
                    console.log(costo_par_total)
                    $("#costo_par" + id).val((costo_par_total).toFixed(4));
                }

                function planilla() {

                    let costos = $("#costos" + id).val();
                    let otros_costos = $("#otros_costos" + id).val();
                    $("#beneficio" + id).val((costos * (empresaP[0].porcentaje_beneficio / 100)).toFixed(4));
                    var beneficio = $("#beneficio" + id).val() * 1;
                    var partei = costos * 1 + beneficio * 1;
                    var parteii = empresaP[0].produccion_promedio;
                    let otros_costos_t = (otros_costos * 1 / empresaP[0].produccion_promedio * 1)
                    $("#costo_par" + id).val((partei / parteii + otros_costos_t).toFixed(4));
                }


                $("#tipo_operacio" + id).change(function() {
                    valorid = this.value;
                    idtipo = this.value;

                    $.each(tipo, function(key, valu) {

                        if (valu.cod_pago == valorid) {

                            $("#unidad_medida").val(valu.descrip_unidad_medida + " (" + valu.unidad +
                                ")");
                            if (valorid == 3) {
                                $("#beneficio" + id).val($("#costos" + id).val() * (empresaP[0]
                                    .porcentaje_beneficio / 100));
                                planilla();

                            } else if (valorid == 2) {
                                $("#beneficio" + id).val(0);
                                jornal();
                            } else if (valorid == 1) {
                                $("#beneficio" + id).val(0);
                                destajo();
                            }
                        }
                    });
                });
            });
        </script>
    @endsection
