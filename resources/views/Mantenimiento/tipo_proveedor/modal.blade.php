<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-modificar-{{$clas->cod_tipo_proveedor}}">
	{{Form::Open(array('action'=>array('TipoProveedorController@update',$clas->cod_tipo_proveedor
  ),'method'=>'put'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modificar Tipo de Proveedor</h4>
			</div>
			<div class="modal-body">
        <input type="text" name="cod" style="display:none" value="{{$clas->cod_tipo_proveedor}}">
        <div class="form-group row"  >
            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre:</label>
            <div class="col-md-6">
                <input id="nombre" type="text" class="form-control" name="descripcion" value="{{$clas->descrip_tipo_proveedor}}" required>
            </div>
        </div>
			<div class="form-group row">
					<label for="password" class="col-md-4 col-form-label text-md-right">Rubro</label>

					<div class="col-md-6">
						<select name="rubro" class="custom-select">
							<option value=""  disabled>Rubro</option>
							@foreach ($rubro as $cat)
								@if($cat->nom_subcategoria == $clas->rubro_proveedor)
								<option value="{{$cat->nom_subcategoria}}" selected>{{$cat->nom_subcategoria}}</option>
								@else
								<option value="{{$cat->nom_subcategoria}}" >{{$cat->nom_subcategoria}}</option>
								@endif
							@endforeach
						</select>
					</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-slant bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-slant bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
