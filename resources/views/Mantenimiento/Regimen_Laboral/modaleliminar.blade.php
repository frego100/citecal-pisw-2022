<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$lab->cod_regimen_laboral}}">
	{{Form::Open(array('action'=>array('RegimenLaboralController@destroy',$lab->cod_regimen_laboral
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desactivar regimen laboral</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_regimen_eliminar" value="{{$lab->cod_regimen_laboral}}">
				<input type="text" style="display:none" name="accion" value="0">
				<p>Esta seguro que desea desactivar el regimen laboral: {{$lab->descrip_regimen_laboral}}</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
