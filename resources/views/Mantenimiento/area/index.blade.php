@extends ('layouts.app')
@section ('content')
<div class="preloader">

</div>
<div class="container">
  <div>
  <h3 class="font-weight-bold">Listado de Areas <a href="area/create"><button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nueva Area</button></a></h3>
  </div>

  <div class="x_content table-responsive">
    <table id="example" class="display">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Editar</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($areas as $ar)
        @if($ar->estado_area==0)
        <tr class="bg-danger">
        @endif
          <td>{{$ar->descrip_area}}</td>
          <td>
             <a href="" data-target="#modal-edit-{{$ar->cod_area}}" data-toggle="modal">
              <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
          </td>
          <td>
            @if($ar->estado_area==1)
             <a href="" data-target="#modal-delete-{{$ar->cod_area}}" data-toggle="modal">
              <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
            @else
            <a href="" data-target="#modal-active-{{$ar->cod_area}}" data-toggle="modal">
             <button class="bttn-unite bttn-md bttn-success"><i class="fas fa-check-circle"></i></button></a>
            @endif
          </td>
        </tr>
        @include('Mantenimiento.area.modaleditar')
        @include('Mantenimiento.area.modaleliminar')
        @include('Mantenimiento.area.modalactivar')
        @endforeach
      </tbody>
    </table>
  </div>
</div>


@endsection
