@extends ('layouts.app')
@section ('content')
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Nuevo Modelo de Calzado</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'Mantenimiento/noticias/store','files'=>true,'method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
                <div class="row">
                  <div class="form-group col-md-6">
                    <div class="">
                      <label for="codigo_calzado">Codigo del evento</label>
                       <input type="text"  name="cod_evento" required="required" maxlength="15" class="form-control ">
                    </div>
                    <div class="">
                      <label for="codigo_calzado">Nombre del evento</label>
                       <input type="text"  name="nombre_evento" required="required" maxlength="250" class="form-control ">
                    </div>
                    <div class="">
                      <label for="codigo_calzado">Descripcion del evento</label>
                       <input type="text"  name="descrip_evento" required="required" maxlength="500" class="form-control ">
                    </div>
                    <div class="">
                      <label>Categoria:</label>
                      <select name="categoria" required='required' class="custom-select">
                      <option value="" selected disabled>--- Seleccionar Categoria ---</option>
                      @foreach ($categorias as $cat)
                       <option value="{{$cat->cod_categoria}}" >{{$cat->nombre_categoria}}</option>
                      @endforeach
                      </select>
                    </div>
                    <div class="">
                      <label for="exampleFormControlFile1">Imagen de evento:</label>
                      <input type="file" class="form-control-file"  name="photo" required>
                    </div>
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                  </div>
                </div>
          {!!Form::close()!!}
        </div>
      </div>
@endsection
