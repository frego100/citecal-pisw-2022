@extends ('layouts.admin')
@section ('contenido')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Registrar Trabajador</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'recursos_humanos/trabajador','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}


<div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                      <div class="input-group  col-md-6">
                        <select name="area" class="custom-select" required>
                          <option value="" selected disabled>Area</option>
                          @foreach ($areas as $subcat)
                            <option value="{{$subcat->cod_area}}" >{{$subcat->descrip_area}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">DNI del Trabajador:</label>
                         <input type="text" pattern=".{8,}" title="Longitud incorrecta" id="d" name="dni" required="required" onkeypress="return isNumberKey(event)" maxlength="8" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Nombre del Trabajador:</label>
                         <input type="text" maxlength="200" pattern="[A-Za-z ñÑ]{2,}" id="nom" name="nombre" required="required" title="Solo letras" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Apellido Paterno:</label>
                         <input type="text" maxlength="200" id="app" name="apellidop" pattern="[A-Za-zñÑ]{2,}" title="Solo letras" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Apellido Materno:</label>
                         <input type="text" maxlength="200" id="apm" name="apellidom" pattern="[A-Za-zñÑ]{2,}" title="Solo letras"  required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Telefono de contacto:</label>
                         <input type="text" id="tipo" name="telefono" pattern=".{9,}" onkeypress="return isNumberKey(event)" maxlength="9" title="Solo Numeros"  required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                          <label for="total_orden_compra">Fecha de Nacimiento:</label>
                           <input type="date" id="tipo" required="required" name="fecha" class="form-control ">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                      <div class="input-group  col-md-6">
                        <select name="ciudad" id="s_ciudad" class="custom-select" required>
                          <option value="" selected disabled>Ciudad</option>
                          @foreach ($ciudad as $ciu)
                            <option value="{{$ciu->cod_ciudad}}" >{{$ciu->nomb_ciudad}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-group  col-md-6" style="margin-top:10px">
                        <select name="distrito" id="s_distrito" class="custom-select" required>
                          <option value="" selected disabled>Distrito</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                          <label for="total_orden_compra">Direccion:</label>
                            <textarea class="form-control" rows="3" id="comment" name="direccion" required="required"></textarea>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-group  col-md-6">
                        <select name="sexo" class="custom-select" required>
                          <option value="" selected disabled>Sexo</option>
                            <option value="H" >Hombre</option>
                            <option value="M" >Mujer</option>
                        </select>
                      </div>

                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                          <label for="total_orden_compra">Fecha de Ingreso:</label>
                           <input type="date" id="tipo" required="required" name="f_ingreso" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-md-6 col-xs-12">
                        <select name="grado_instruccion" class="custom-select" required>
                          <option value="" selected disabled>Grado de Instruccion</option>
                          <option value="NA">Ninguna</option>
                          <option value="PI">Primaria Incompleta</option>
                          <option value="PC">Primaria Completa</option>
                          <option value="SI">Secundaria Incompleta</option>
                          <option value="SC">Secundaria Completa</option>
                          <option value="UN">Estudio Superior</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Puesto:</label>
                         <input maxlength="900" type="text" id="tipo" name="puesto" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                           <select name="tipo" class="custom-select" required>
                             <option value="" selected disabled>Tipo de Trabajador</option>
                             @foreach ($tipo_trabajador as $tip)
                               <option value="{{$tip->cod_tipo_trabajador}}" >{{$tip->descrip_tipo_trabajador}}</option>
                             @endforeach
                           </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Correo Electronico:</label>
                         <input maxlength="30" type="email" id="tipo" name="correo" required="required" class="form-control ">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group  ">
                    <label for="total_orden_compra">Ficha del trabajador:</label>
                      <textarea class="form-control" rows="5" id="comment" name="experiencia" required="required"></textarea>
                </div>
                          <div class="ln_solid"></div>
                      <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                            </div>
                          </div>
          {!!Form::close()!!}
        </div>
      </div>
    </div>
	<script>
  $( document ).ready(function() {
    $("#s_ciudad").change(function(){
      $('#s_distrito').empty()
      $('#s_distrito').append('<option selected="selected" disabled value="">Distrito</option>');
      var ciudad=$("#s_ciudad").val();
      var opciones="";
      @foreach ($distrito as $dist)
        var cod_ciudad="{{$dist->cod_ciudad}}"
        if(ciudad==cod_ciudad)
        {
          $("#s_distrito").append("<option value={{$dist->cod_distrito}}>{{$dist->nomb_distrito}}</option>");
        }
      @endforeach
    })
  });
  function isNumberKey(evt)
  {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
    return false;
    return true;
  }


	</script>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");

</script>
@endpush
@endsection
