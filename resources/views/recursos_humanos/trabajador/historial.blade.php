@extends ('layouts.admin')
@section ('contenido')
<div>
  <h1 class="asd">Trabajador N° {{$var}}</h1>
</div>
  <hr class=" my-4">
<div id="detalle">
  <div class="table-responsive" >
    <h2>Detalle del Trabajador</h2>
    <table id="example" class="display">
      <thead>
        <tr>
          <th>Fecha</th>
          <th>Descripcion</th>
        </tr>
      </thead>
      <tbody>
        @foreach($detalle as $trab)
            <tr>
              <td>{{$trab->fecha}}</td>
              <td>{{$trab->descripcion}}</td>
            </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
<a href="{!! url('recursos_humanos/trabajador') !!}" >
  <button class="bttn-unite bttn-md bttn-danger ">Atras</button></a>
@endsection
