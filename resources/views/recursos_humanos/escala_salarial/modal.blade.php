<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$esc->cod_trabajador}}">
	{{Form::Open(array('action'=>array('EscalaSalarialController@update',$esc->cod_trabajador
  ),'method'=>'patch'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Editar Trabajador</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="cod_editar" value="{{$esc->cod_trabajador}}">
        <div class="form-group  ">
          <label for="total_orden_compra">DNI del trabajador:</label>
           <input type="text" class="form-control" disabled value="{{$esc->DNI_trabajador}}">
        </div>
        <div class="form-group">
          <label for="">Sueldo Mensual</label>
          <input type="text" id="tipo" name="sueldo" onkeypress="return isNumberKey(event)" title="Solo Numeros" value="{{$esc->sueldo_mensual}}"  required="required" class="form-control ">
        </div>
        <div class="form-group">
          <label for="">Regimen Laboral</label>
          <select class="custom-select" required name="regimen">
            <option value="" disabled>Escoger Regimen Laboral</option>
            @foreach($regimen_renta as $reg)
              @if($reg->cod_regimen_laboral==$esc->cod_regimen_laboral)
                <option value="{{$esc->cod_regimen_laboral}}" selected>{{$esc->descrip_regimen_laboral}}</option>
              @else
                <option value="{{$reg->cod_regimen_laboral}}">{{$reg->descrip_regimen_laboral}}</option>
              @endif
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="">Tipo de seguro</label>
          <select class="custom-select" required name="seguro">
            <option value="" disabled>Tipo de seguro</option>
            @foreach($seguro as $reg)
              @if($reg->cod==$esc->tipo_seguro)
                <option value="{{$reg->cod}}" selected>{{$reg->nombre}}</option>
              @else
                <option value="{{$reg->cod}}">{{$reg->nombre}}</option>
              @endif
            @endforeach
          </select>
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
