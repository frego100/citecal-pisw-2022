@extends ('layouts.admin')
@section ('contenido')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Registrar en planilla</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'recursos_humanos/escala_salarial','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
<div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                      <div class="form-group  col-md-6">
                        <label for="">Trabajadores en planilla:</label>
                        <select name="trabajador" id="trab" class="custom-select" required>
                          <option value="" selected disabled>Trabajadores</option>
                          @foreach ($trabajador as $trab)
                            <option value="{{$trab->DNI_trabajador}}" >{{$trab->apellido_paterno}} {{$trab->apellido_materno}}, {{$trab->nombres}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="">Regimen de Laboral:</label>
                        <select name="regimen" class="custom-select" required>
                          <option value="" selected disabled>Regimen Laboral</option>
                          @foreach ($regimen_renta as $reg)
                            <option value="{{$reg->cod_regimen_laboral}}" >{{$reg->descrip_regimen_laboral}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="row">
                      <div class="form-group  col-md-6">
                        <label for="">Tipo de seguro:</label>
                        <select name="seguro" id="s_ciudad" class="custom-select" required>
                          <option value="" selected disabled>Tipo de Seguro</option>
                          @foreach($seguro as $seg)
                            <option value="{{$seg->cod}}">{{$seg->nombre}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group  col-md-6 col-md-offset-4 col-xs-12">
                        <label for="total_orden_compra">Sueldo Mensual en planilla:</label>
                        <input type="text" id="tipo" name="sueldo" onkeypress="return isNumberKey(event)" title="Solo Numeros"  required="required" class="form-control ">
                      </div>
                    </div>
                  </div>
                </div>
                          <div class="ln_solid"></div>
                      <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                            </div>
                          </div>
          {!!Form::close()!!}
        </div>
      </div>
    </div>
	<script>

  function isNumberKey(evt)
  {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
    return false;
    return true;
  }
  $("#trab").change(function(){
    let cod=$("#trab").val();
    validar_existencia(cod)
  })
  function validar_existencia(cod)
  {
    $.ajax({
      url:"/verificar/trabajadores/"+cod,
      success:function(data){
        if(data==1)
        {
          $("#trab").val("")
          alert("Ya registro un costo a este trabajador")
        }
      }
    })
  }

	</script>
@endsection
