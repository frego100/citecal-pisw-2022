<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class CatalogoPreciosModel extends Model
{
  protected $table='catalogo_precios';

protected $primaryKey="cod_articulo";

public $timestamps=false;


protected $fillable=['costo_venta','utilidad','precio','usuario'];

protected $guarded=[];
}
