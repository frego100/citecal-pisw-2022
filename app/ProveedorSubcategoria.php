<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ProveedorSubcategoria extends Model
{
    protected $table='proveedor_subcategoria';

    protected $primaryKey="cod_provcat";
  
    public $timestamps=false;
  
  
    protected $fillable=['cod_provcat','RUC_proveedor','cod_subcategoria'];
  
    protected $guarded=[];
}
