<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class TallasPares extends Model
{
  protected $table='talla_pares';


  public $timestamps=false;


  protected $fillable=['cod_op','cod_serie','talla','pares','RUC_empresa'];

  protected $guarded=[];
}
