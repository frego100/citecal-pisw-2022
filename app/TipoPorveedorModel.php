<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class TipoPorveedorModel extends Model
{
  protected $table='tipo_proveedor';

  protected $primaryKey="cod_tipo_proveedor";

  public $timestamps=false;


  protected $fillable=['descrip_tipo_proveedor', 'rubo_proveedor'];

  protected $guarded=[];
}
