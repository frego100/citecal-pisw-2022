<?php

namespace erpCite\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteVentasFormRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request
     *
     * @return bool
     */

    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request
     *
     * @return array
     */

    public function rules()
    {
        return [
            'tipo_documento'=>'required',
            'numero_documento'=>'required',
            'nombres'=>'required|max:60',
            'apellidos'=>'required|max:60',
            'pais_id'=>'required',
            'departamento_id'=>'required',
            'direccion_1'=>'required|max:120',
            'direccion_2'=>'max:120',
            'celular_1'=>'required',
            'celular_2'=>'',
            'correo'=>'required|max:80'
        ];
    }

}
