<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\DetalleCostoModeloMano;
use erpCite\CostoModelo;
use DB;

class CostosDirectosObraController extends Controller
{
  public function __construct()
  {
    $this->middleware('costo');
  }
  public function index( $var)
  {
    $model=explode("+",$var);
    $empresa=Auth::user()->RUC_empresa;
    $empresas=DB::table('empresa')
    ->where('RUC_empresa','=',$empresa)
    ->select('cod_regimen_laboral')
    ->get();
    $empresas[0]->cod_regimen_laboral;
    $beneficios=DB::table('escala_salarial')
    ->join('trabajador','escala_salarial.cod_trabajador','=','trabajador.DNI_trabajador')
    ->where('escala_salarial.RUC_empresa','=',$empresa)
    ->where('escala_salarial.cod_regimen_laboral','=',$empresas[0]->cod_regimen_laboral)
    ->where('trabajador.estado_trabajador','=',1)
    ->where('escala_salarial.estado','=',1)
    ->avg('tasa_beneficio');
    if($beneficios=="")
    {
      $beneficios=0;
    }
    $produccion_promedio=DB::Table('datos_generales')
    ->where('RUC_empresa','=',$empresa)
    ->sum('produccion_promedio');
    $codigo_costo=DB::table('costo_modelo')
    ->where('costo_modelo.RUC_empresa','=',$empresa)
    ->where('costo_modelo.modelo_serie','=',$model[1])
    ->get();
    $detalle=[];
      for($i=0;$i<count($codigo_costo);$i++)
      {
        $detalle=DB::table('detalle_costo_modelo_manoobra')
        ->join('area','detalle_costo_modelo_manoobra.cod_area','=','area.cod_area')
        ->join('tipo_trabajador','detalle_costo_modelo_manoobra.cod_tipo_trabajador','=','tipo_trabajador.cod_tipo_trabajador')
        ->where('detalle_costo_modelo_manoobra.cod_costo_modelo','=',$codigo_costo[$i]->cod_costo_modelo)
        ->get();
      }
    $tipo_trabajador=DB::table('tipo_trabajador')
    ->where('tipo_trabajador.estado_tipo_trabajador','=','1')
    ->get();
    $area=DB::table("area")
    ->where('descrip_area','LIKE','P-%')
    ->get();
    return view('costos.directos.costos_directos_mano.index',["codigo_costo"=>$codigo_costo,"detalle"=>$detalle
    ,"area"=>$area,"var"=>$model[0]
    ,"tipo_trabajador"=>$tipo_trabajador
    ,"beneficios"=>$beneficios
  ,"produccion_promedio"=>$produccion_promedio]);
  }
  public function store()
  {
    $areatabla=DB::table("area")
    ->where('descrip_area','LIKE','P-%')
    ->get();
    $identificador=Input::get('costo_codigo');
    $seriemodelo=DB::Table('costo_modelo')
    ->where('cod_costo_modelo','=',$identificador)
    ->get();
    $operacion=Input::get('operacion');
    $areas=Input::get('area');
    $tipo_trabajador=Input::get('tipo');
    $costo=Input::get('costo');
    $beneficio=Input::get('beneficio');
    $costo_por_par=Input::get('costo_par');
    $total=Input::get('total_materiales');
    $otros=Input::get('otros');
    $codareas;
    for($i=0;$i<count($areas);$i++)
    {
      for($j=0;$j<count($areatabla);$j++)
      {
          if($areatabla[$j]->descrip_area==$areas[$i])
          {
            $codareas[$i]=$areatabla[$j]->cod_area;
            break;
          }
      }
    }
    for($in=0;$in<count($operacion);$in++)
    {
      $articulo=new DetalleCostoModeloMano;
      $articulo->cod_detalle_costo_mano=$seriemodelo[0]->modelo_serie."-".rand(100000,999999);
      $articulo->cod_costo_modelo=$identificador;
      $articulo->cod_tipo_trabajador=$tipo_trabajador[$in];
      $articulo->cod_area=$codareas[$in];
      $articulo->operacion=$operacion[$in];
      $articulo->costo=$costo[$in];
      $articulo->beneficio=$beneficio[$in];
      $articulo->costo_por_par=$costo_por_par[$in];
      $articulo->otros_costos=$otros[$in];
      $articulo->save();
    }
    $act=CostoModelo::where('cod_costo_modelo',$identificador)
    ->update(['total_obra'=>$total]);
    session()->flash('success','Costo de Materiales Registrado Satisfactoriamente');
    return Redirect::to('costo/vermodelos');
  }
  public function update()
  {
    $areatabla=DB::table("area")
    ->where('descrip_area','LIKE','P-%')
    ->get();
    $identificador=Input::get('costo_codigo');
    $seriemodelo=DB::Table('costo_modelo')
    ->where('cod_costo_modelo','=',$identificador)
    ->get();
    $operacion=Input::get('operacion');
    $estado=Input::get('estado');
    $areas=Input::get('area');
    $tipo_trabajador=Input::get('tipo');
    $costo=Input::get('costo');
    $beneficio=Input::get('beneficio');
    $costo_por_par=Input::get('costo_par');
    $total=Input::get('total_materiales');
    $otro=input::Get('otros');
    $codareas;
    for($i=0;$i<count($areas);$i++)
    {
      for($j=0;$j<count($areatabla);$j++)
      {
          if($areatabla[$j]->descrip_area==$areas[$i])
          {
            $codareas[$i]=$areatabla[$j]->cod_area;
            break;
          }
      }
    }
    $detalle=DB::table('detalle_costo_modelo_manoobra')
    ->where('detalle_costo_modelo_manoobra.cod_costo_modelo','=',$identificador)
    ->get();
    for($i=0;$i<count($detalle);$i++)
    {
      $f=0;
      for($j=0;$j<count($operacion);$j++)
      {
          if($detalle[$i]->cod_detalle_costo_mano==$estado[$j])
          {
            $f=1;
            $act=DetalleCostoModeloMano::where('cod_costo_modelo',$identificador)
            ->where('cod_detalle_costo_mano',$estado[$j])
            ->update(['cod_tipo_trabajador'=>$tipo_trabajador[$j],
                      'operacion'=>$operacion[$j],
                      'costo'=>$costo[$j],
                      'beneficio'=>$beneficio[$j],
                      'costo_por_par'=>$costo_por_par[$j],
                      'otros_costos'=>$otro[$j],
                    'estado_trabajador'=>1]);
            break;
          }
      }
      if($f==0)
      {
        $act=DetalleCostoModeloMano::where('cod_detalle_costo_mano',$detalle[$i]->cod_detalle_costo_mano)
        ->delete();
      }
    }
    for($i=0;$i<count($estado);$i++)
    {
      if($estado[$i]=="nuevo")
      {
        $articulo=new DetalleCostoModeloMano;
        $articulo->cod_detalle_costo_mano=$seriemodelo[0]->modelo_serie."-".rand(100000,999999);
        $articulo->cod_costo_modelo=$identificador;
        $articulo->cod_tipo_trabajador=$tipo_trabajador[$i];
        $articulo->cod_area=$codareas[$i];
        $articulo->operacion=$operacion[$i];
        $articulo->costo=$costo[$i];
        $articulo->beneficio=$beneficio[$i];
        $articulo->costo_por_par=$costo_por_par[$i];
        $articulo->otros_costos=$otro[$i];
        $articulo->save();
      }
    }
    $modelo=DB::table('costo_modelo')
    ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
    ->where('cod_costo_modelo',$identificador)
    ->select('serie_modelo.estado')
    ->get();
    $act=CostoModelo::where('cod_costo_modelo',$identificador)
    ->update(['total_obra'=>$total,'estado'=>$modelo[0]->estado]);
    session()->flash('success','Costo de Materiales Actualizado Satisfactoriamente');
    return Redirect::to('costo/vermodelos');
  }
}
