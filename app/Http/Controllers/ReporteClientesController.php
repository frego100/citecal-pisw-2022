<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\Http\Requests\ClasificacionFormRequest;
use DB;
use erpCite\ClienteModel;


class ReporteClientesController extends Controller
{
  public function __construct()
  {
    $this->middleware('jefe');
  }
  public function index(Request $request)
  {
    if ($request) {
      //$clientes = ClienteModel::all();

      $clientes = ClienteModel::where('RUC_empresa', Auth::user()->RUC_empresa)
      ->orderBy('nombre', 'asc')
      ->get();

      //$orden_pedidos = OrdenPedidoModel::where('RUC_empresa', Auth::user()->RUC_empresa)->get();
      
      //$detalle_orden_pedidos = DetalleOrdenPedidoModel::all();

      //$serieModelos = SerieModeloModel::where('RUC_empresa', Auth::user()->RUC_empresa)->get();

      //$modelos = Modelo::where('RUC_empresa', Auth::user()->RUC_empresa)
      //->where("estado_modelo","3")->get();

      $reporte_clientes = ClienteModel::select('nombre as cliente',
      DB::raw('COUNT(*) as frecuencia_pedidos'),
      DB::raw('SUM(orden_pedido.total_pedido) as ingreso_total_pedidos'),
      DB::raw('SUM(orden_pedido.deuda) as deuda'))
      ->join('orden_pedido', 'cliente.codigo', '=', 'orden_pedido.codigo_cliente')
      ->where('cliente.RUC_empresa', Auth::user()->RUC_empresa)
      ->groupBy('codigo','nombre')
      ->get();
      /*
      $detalle_orden_pedidos = DB::table('detalle_orden_pedido as dop')
      ->where('dop.estado_unidad_medida','=',1)
      ->orderBy('unidad_medida.descrip_unidad_medida','asc')->get();
      */
      //return view('Ventas.reportes.clientes.index',["clientes"=>$clientes, "orden_pedidos"=>$orden_pedidos,
      //"detalle_orden_pedidos"=> $detalle_orden_pedidos, "serieModelos"=>$serieModelos , "modelos"=>$modelos, "reporte_clientes"=>$reporte_clientes ]);
      return view('Ventas.reportes.clientes.index',["clientes"=>$clientes, "reporte_clientes"=>$reporte_clientes ]);
    }
  }
  public function create(Request $request)
  {
    /*
    if($request)
    {
      return view("Ventas.reporte_clientes.create");
    }
    */

  }
  public function store()
  {
    /*
    $empresa=Auth::user()->RUC_empresa;
    $sigla=DB::table('empresa')->where('RUC_empresa',$empresa)->get();
    $identificador=rand(10000,99999);
    $siglax = $sigla[0]->siglas;
    $res=$siglax.'-'.$identificador;

    $cliente=new ClienteModel;
    $cliente->codigo=$res;
    $cliente->documento=Input::get('documento');
    $cliente->nombre=Input::get('nombre');
    $cliente->destino=Input::get('destino');
    $cliente->telefono=Input::get('telefono');
    $cliente->direccion=Input::get('direccion');
    $cliente->RUC_empresa=$empresa;
    $cliente->save();
    session()->flash('success','Cliente Registrado');
    return Redirect::to('Ventas/reporte_clientes');
    */
  }
  public function update()
  {
    /*
    $cod=Input::get('codigo');
    $documento=Input::get('documento');
    $nombre=Input::get('nombre');
    $destino=Input::get('destino');
    $telefono=Input::get('telefono');
    $direccion=Input::get('direccion');
    $act=ClienteModel::where('codigo',$cod)
    ->update(['documento'=>$documento,
    'nombre'=>$nombre,
    'destino'=>$destino,
    'telefono'=>$telefono,
    'direccion'=>$direccion]);
    session()->flash('success','Datos de Cliente Actualizado');
    return Redirect::to('Ventas/cl');
    */
  }
  public function destroy()
  {
    /*
    $cod=Input::get('codigo');
    $estado=Input::get('estado');
    if($estado==0)
    {
      $mensaje="Desactivado";
    }
    else{$mensaje="Activado";}
    $act=ClienteModel::where('codigo',$cod)
    ->update(['estado'=>$estado]);
    session()->flash('success','Cliente '.$mensaje);
    return Redirect::to('Ventas/reporte_clientes');
    */
  }
  public function obtener_modelo($var)
  {
    
    return $var;
  }
}
