<?php

namespace erpCite\Http\Controllers;

use DB;
use erpCite\InventarioModel;
use erpCite\SalidaProductoModel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class Modelo
{
    public $idModelo = 0;
    public $imagen = "";
    public $coleccion = "";
    public $linea = "";
    public $serie = "";
    public $codModelo = "";
    public $descModelo = "";
    public $estado = 1;
    public $tallas = [];

    public function __construct(int $idModelo, string $imagen, string $coleccion, string $linea, string $serie, string $codModelo, string $descModelo, array $tallas)
    {
        $this->idModelo = $idModelo;
        $this->imagen = $imagen;
        $this->coleccion = $coleccion;
        $this->linea = $linea;
        $this->serie = $serie;
        $this->codModelo = $codModelo;
        $this->descModelo = $descModelo;
        $this->tallas = $tallas;
    }
}

class InventarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('jefe');
    }

    public function index(Request $request)
    {
        if ($request) {
            $inventario = DB::table('inventario_producto_terminado')
                ->get();

            $modelos = [];
            $iter = 0;
            foreach ($inventario as $modelo) {
                // Trae detales de modelo de Desarrollo de producto y Costos (precio)
                $desarrollo = DB::table('desarrollo_producto_ventas_tmp')
                    ->where('desarrollo_producto_ventas_tmp.cod_desarrollo_producto', '=', $modelo->cod_desarrollo_producto)
                    ->first();

                $tallas = explode("-", $desarrollo->tallas);
                $j = 1;
                $modelo->tallas = [];
                foreach ($tallas as $talla) {
                    $modelo->tallas = Arr::add($modelo->tallas, $j - 1, array($talla, $modelo->{'cantidad_T' . $j}));
                    $j++;
                }
                $modelo->imagen = $desarrollo->imagen;
                $modelos = Arr::add(
                    $modelos,
                    $iter,
                    new Modelo(
                        $modelo->cod_producto_terminado,
                        $modelo->imagen, // "/images/modelo-test-001.png", //falta
                        $desarrollo->coleccion,
                        $desarrollo->linea,
                        $desarrollo->serie,
                        $modelo->codigo,
                        $modelo->descripcion,
                        $modelo->tallas
                    )
                );
                $iter++;
            }

            $desarrollo = array(
                "Colección 2022 - Lo nuevo",
                "Bota Dama",
                "Serie Dama Lima"
            );
            // Fin -> Trae detales de modelo de Desarrollo de producto y Costos (precio)

            return view('Ventas.inventario.index', [
                "modelos" => $modelos,
                "producto" => $desarrollo
            ]);
        }
    }

    public function show()
    {
        //$modelos = $this->buildFakeDataModelos();
        $modelos = $this->getDataDesarrollo();

        $almacenes = DB::table('almacen')
            ->where('almacen.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('almacen.estado_almacen', '=', 1)
            ->get();
        return view("Ventas.inventario.ingresoVarios", ["modelos" => $modelos, "almacenes" => $almacenes]);
    }

    public function obtenerModelos()
    {
        $modelos = $this->getDataDesarrollo();
        return $modelos;
    }

    public function store(Request $request)
    {

        $almacen = Input::get('almacen');
        $modelos = input::get('id_modelo');
        $descripciones = Input::get('descModelo');
        $desarrollos = Input::get('desarrollo');
        $codigos = Input::get('codModelo');

        $cantidad_modelo_tallas = [];
        for ($i = 0; $i < sizeof($modelos); $i++) {
            $cantidad_tmp = Input::get('cantidad-tallas-' . $modelos[$i]);
            $nuevo_modelo = new InventarioModel;
            $nuevo_modelo->cod_producto_terminado = DB::table('inventario_producto_terminado')->count() + 1;
            $nuevo_modelo->cod_desarrollo_producto = $desarrollos[$i]; //este viene de la tabla  de Desarrollo de Producto
            $nuevo_modelo->cod_almacen = $almacen;
            $nuevo_modelo->codigo = $codigos[$i];
            $nuevo_modelo->descripcion = $descripciones[$i];
            for ($j = 0; $j < $cantidad_tmp; $j++) {
                $k = $j + 1;
                $nuevo_modelo->{'cantidad_T' . $k} = Input::get('cantidad-por-talla-' . $modelos[$i] . '-' . $j);
            }
            $nuevo_modelo->save();
        }
        session()->flash('success', 'Modelos agregados correctamente.');

        return Redirect::to('Ventas/inventario');
    }

    public function entradaSalidaIndividual()
    {
        $tipo_consulta = input::get('tipo_consulta');
        $modelo = input::get('id_modelo');
        $cantidad_tallas_modelo = input::get('cantidad_tallas_modelo');
        $cantidades_ingreso_salida = [];

        //SalidaModel
        $salida_producto = new SalidaProductoModel;
        $salida_producto->cod_salida = DB::table('salida_producto')->count();
        $salida_producto->cod_producto_terminado = $modelo;
        $salida_producto->motivo_salida = "No definido."; //Input::get('motivo');

        //Modelo  actual
        $actual = DB::table('inventario_producto_terminado')
            ->where(
                'inventario_producto_terminado.cod_producto_terminado',
                '=',
                $modelo
            )->get()->first();

        for ($i = 0; $i < $cantidad_tallas_modelo; $i++) {
            $j = $i + 1;
            $cantidad_tmp = 0;
            $valor = Input::get('cantidad-talla-' . $i);
            if ($tipo_consulta == 0) { //salida
                $salida_producto->{'cantidad_T' . $j} = $valor;
                $cantidad_tmp = intval($actual->{'cantidad_T' . $j}) - intval($valor);
            } else if ($tipo_consulta == 1) { //ingreso
                $cantidad_tmp = intval($actual->{'cantidad_T' . $j}) + intval($valor);
            }
            $cantidades_ingreso_salida = Arr::add($cantidades_ingreso_salida, 'cantidad_T' . $j, $cantidad_tmp);
        }

        $act = InventarioModel::where('cod_producto_terminado', $modelo)
            ->update($cantidades_ingreso_salida);

        if ($tipo_consulta == 0) {
            $salida_producto->save();
            session()->flash('success', 'Salida de productos satisfactoria');
        } else if ($tipo_consulta == 1) {
            session()->flash('success', 'Ingreso de productos satisfactorio');
        }

        return Redirect::to('Ventas/inventario');
    }

    function getCodigoDesarrolloProducto($m)
    {
        return $m->cod_desarrollo_producto;
    }

    public function getDataDesarrollo()
    {
        $inventario = DB::table('inventario_producto_terminado')
            ->get();
        $registrosModelos = DB::table('desarrollo_producto_ventas_tmp')
            ->where('desarrollo_producto_ventas_tmp.estado', '=', 1)
            ->get();

        error_log(json_encode($registrosModelos));
        $modelos = [];
        foreach ($registrosModelos as $modelo) {
            error_log("busqueda");
            $existe = false;
            foreach ($inventario as $iModel) {
                if ($iModel->cod_desarrollo_producto == $modelo->cod_desarrollo_producto) {
                    $existe = true;
                    break;
                }
            }
            if (!$existe) {
                $tallas = explode("-", $modelo->tallas);
                $modelos = Arr::add(
                    $modelos,
                    $modelo->cod_desarrollo_producto,
                    new Modelo(
                        $modelo->cod_desarrollo_producto,
                        $modelo->imagen, // "/images/modelo-test-001.png",
                        $modelo->coleccion,
                        $modelo->linea,
                        $modelo->serie,
                        $modelo->cod_modelo,
                        $modelo->desc_modelo,
                        $tallas
                    )
                );
            }
        }
        return $modelos;
    }
}
