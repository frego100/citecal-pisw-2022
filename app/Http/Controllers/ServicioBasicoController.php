<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\GastoServiciosBasicosModel;
use erpCite\Http\Requests\GastosFormRequest;
use DB;

class ServicioBasicoController extends Controller
{
    public function __construct(){
        $this->middleware('jefe');
    }

    public function index(Request $request)
    {
        if ($request) {
            $mes_actual=date("m");
            $serviciobasico=DB::table('gasto_servicios_basicos')
            ->join('area','gasto_servicios_basicos.cod_area','=','area.cod_area')
            ->where('estado_gasto_basico','=','1')
            ->where('gasto_servicios_basicos.RUC_empresa',Auth::user()->RUC_empresa)
            ->whereMonth('gasto_servicios_basicos.fecha_creacion', '=', $mes_actual)
            ->get();
            return view('costos/indirectos/servicio_basico/index',['serviciobasico'=>$serviciobasico]);
        }
    }

    public function create(Request $request){
        if($request)
        {
          $area=DB::table('area')
          ->where('descrip_area','=','General')
          ->orderBy('descrip_area','asc')
          ->get();
          return view('costos.indirectos.servicio_basico.create',['area'=>$area]);
        }
    }
    public function store()
    {

        //Se Registra el campo detalle_orden_compra
        $identificador=rand(100000,999999);
        $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
        $siglax = $sigla[0]->siglas;
        $res=$siglax.'-'.$identificador;
        $idempresa=Auth::user()->RUC_empresa;
        $serviciobasico=new GastoServiciosBasicosModel;
        $serviciobasico->cod_gasto_servicio_basico=$res;
        $serviciobasico->cod_area=Input::get('cod_area');
        $serviciobasico->descrip_servicio_basico=Input::get('descrip_servicio_basico');
        $serviciobasico->gasto_mensual=Input::get('gasto_mensual');
        $serviciobasico->RUC_empresa=$idempresa;
        $serviciobasico->estado_gasto_basico=1;
        $serviciobasico->fecha_creacion=Input::get('fecha_creacion');
        $serviciobasico->save();
        session()->flash('success','Gasto Servicios Basicos ingresado satisfactoriamente');
        return Redirect::to('costo/indirectos/servicio_basico');
    }
    public function show()
    {
        return view ("costos/indirectos/servicio_basico/index");
    }
    public function edit($id)
    {
        return Redirect::to('costos/indirectos/servicio_basico');
    }
    public function update(ManoObraFormRequest $request,$id)
    {
        /*$categoria=Categoria::findOrFail($id);
        $categoria->nombre=$request->get('nombre');
        $categoria->descripcion=$request->get('descripcion');
        $categoria->update();
        return Redirect::to('almacen/categoria');*/
        return Redirect::to('costos/indirectos/servicio_basico');
    }
    public function destroy($id)
    {
        $email=Input::get('email');
        $estado=Input::get('estado_gasto_basico');
        if($estado==0){$mensaje=" Eliminado";}
        $act=GastoServiciosBasicosModel::where('cod_gasto_servicio_basico',$email)
        ->delete();
        session()->flash('success','Registro'.$mensaje);
        return Redirect::to('costo/indirectos/servicio_basico');
    }
}
