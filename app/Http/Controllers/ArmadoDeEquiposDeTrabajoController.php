<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ArmadoDeEquiposDeTrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $id_orden_pedido_produccion=$id;
        /*para poner los botones dependiendo de los procesos que se necesiten */
        $procesos= DB::table('grupo_trabajo')->select('proceso')->distinct()
        ->where('codigo_orden_pedido_produccion', $id_orden_pedido_produccion)
        ->get();
        
        /*Suma de las cantidades de las  tallas */
        $cantidadesModelo = DB::table('orden_pedido_produccion')->select('cantidades')
        ->where('codigo_orden_pedido_produccion', $id_orden_pedido_produccion)
        ->get();

        $cantidad_modelos=count($cantidadesModelo);
        $cantidadesModelo= json_decode($cantidadesModelo);
        $tallas_pedidas;
        $suma_tallas;
        $cantidad_tallas;
    
        for ($i=0; $i < $cantidad_modelos; $i++) {
          $suma=0;
          $subCantidad[$i] =$cantidadesModelo[$i]->cantidades;
          $cantidades=explode(",",$cantidadesModelo[$i]->cantidades);
          $cantidades_tallas[$i]=count($cantidades);
          for ($j=0; $j < count($cantidades); $j++) {
            $suma=$suma+(int)$cantidades[$j];
            $tallas_pedidas[$i][$j]=$cantidades[$j];
          }
          $suma_cantidades[$i]=$suma;
     }
     /*-------------------------------------------------------------------------- */

        /*para encontrar el codigo del de la serie del articulo*/
        $codigo_serie = DB::table('orden_pedido_produccion')->select('codigo_serie_articulo')
        ->where('codigo_orden_pedido_produccion', $id_orden_pedido_produccion)
        ->get();
        $codigo_serie = explode('"', $codigo_serie);
        $codigo_serie=$codigo_serie[3];
        
        /*para encontrar el codigo del modelo */
        $codigo_modelo = DB::table('serie_modelo')->select('codigo_serie')
        ->where('codigo', $codigo_serie)
        ->get();
        $codigo_modelo = explode(':', $codigo_modelo);
        $codigo_modelo=$codigo_modelo[1];
        $codigo_modelo = explode('}', $codigo_modelo);
        $codigo_modelo=$codigo_modelo[0];

        $codigo_costo_modelo = DB::table('detalle_costo_modelo_materiales')->select('cod_material')
        ->where('cod_costo_modelo', $codigo_modelo)
        ->get();

        $codigo_costo_modelo = explode('"', $codigo_costo_modelo);
        $nombres_modelos= array();
        $nombres_materiales= array();
        $tamaño=count($codigo_costo_modelo);
        
        
        for ($i = 3; $i < $tamaño; $i++){
            array_push($nombres_modelos,$codigo_costo_modelo[$i]);
            $i++;
            $i++;
            $i++;     
       }

       $tamaño_nombres_modelos=count($nombres_modelos);
       
       for ($i = 0; $i < $tamaño_nombres_modelos; $i++){
        
        $nombre = DB::table('material')->select('descrip_material')
            ->where('cod_material', $nombres_modelos[$i] )
            ->get();  
            $nombre = explode('"', $nombre);

            array_push($nombres_materiales,$nombre[3]);
        }

        $datos=5;
        return view('armado_de_equipos_de_trabajo.index',compact("id_orden_pedido_produccion", "procesos", "datos","suma","nombres_materiales","tamaño_nombres_modelos"));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
