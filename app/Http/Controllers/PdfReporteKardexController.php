<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class PdfReporteKardexController extends Controller
{
  public function reporte_tienda(Request $request)
  {
    if ($request) {
      $accion=Input::Get('accion');
      $area=Input::Get('area_reporte');
      $material=Input::Get('material_reporte');
      $fecha_inicio=Input::Get('fecha_inicio');
      $fecha_final=Input::Get('fecha_final');
      $subcategoria=Input::get('subcategoria');
      $almacen=Input::Get('almacen_reporte');
      $pdf=\App::make('dompdf.wrapper');
            //$pdf->setPaper('a4','landscape');
      $pdf->loadHTML($this->convert_data_tienda($almacen,$accion,$area,$material,$fecha_inicio,$fecha_final,$subcategoria));
      return $pdf->stream();
    }
  }
  public function store(Request $request){
    if ($request) {
      $accion=Input::Get('accion');
      $area=Input::Get('area_reporte');
      $material=Input::Get('material_reporte');
      $fecha_inicio=Input::Get('fecha_inicio');
      $fecha_final=Input::Get('fecha_final');
      $subcategoria=Input::get('subcategoria');
      $almacen=Input::Get('almacen_reporte');
      $pdf=\App::make('dompdf.wrapper');
            //$pdf->setPaper('a4','landscape');
      $pdf->loadHTML($this->convert_data($almacen,$accion,$area,$material,$fecha_inicio,$fecha_final,$subcategoria));
      return $pdf->stream();
    }
  }
  function get_data_tienda($almacen,$accion,$area,$material,$fecha_inicio,$fecha_final,$subcategoria)
  {
    $fechas= strtotime($fecha_inicio);
    $fechas= date('Y-m-d H:i:s',$fechas);
    $fechas_final= strtotime($fecha_final.' +1 day');
    $fechas_final= date('Y-m-d H:i:s',$fechas_final);
    switch ($accion) {
      case '0':
      $orden_data=DB::table('detalle_kardex_tienda')
      ->join('kardex_tienda','detalle_kardex_tienda.cod_kardex_tienda','=','kardex_tienda.cod_kardex_tienda')
      ->join('almacen','kardex_tienda.codigo_almacen','=','almacen.cod_almacen')
      ->join('area','detalle_kardex_tienda.cod_area','=','area.cod_area')
      ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where(function ($query) use($almacen){
        if($almacen!="")
        {
          $query->where('almacen.cod_almacen','=',$almacen);
        }
      })
      ->where(function ($query) use ($material,$subcategoria){
        if($subcategoria!="")
        {
          $query->where('subcategoria.cod_subcategoria','=',$subcategoria);
        }
        else {
          if($material!="")
          {
            $query->where('kardex_tienda.codigo_material','=',$material);
          }
        }
      })
      ->where(function ($query) use ($area){
        if($area!="")
        {
          $query->where('detalle_kardex_tienda.cod_area','=',$area);
        }
      })
      ->Where(function ($query) use ($fechas,$fechas_final) {
        $query->orwhere([['detalle_kardex_tienda.fecha_ingreso', '>=', $fechas],['detalle_kardex_tienda.fecha_ingreso', '<=', $fechas_final]])
        ->orwhere([['detalle_kardex_tienda.fecha_salida', '>=', $fechas],['detalle_kardex_tienda.fecha_salida', '<=', $fechas_final]])
        ->orwhere([['detalle_kardex_tienda.fecha_devolucion', '>=', $fechas],['detalle_kardex_tienda.fecha_devolucion', '<=', $fechas_final]]);
      })
      ->orderBy('material.descrip_material','asc')
      ->orderBy('detalle_kardex_tienda.fecha_ingreso','desc')
      ->orderBy('detalle_kardex_tienda.fecha_salida','asc')
      ->orderBy('detalle_kardex_tienda.fecha_devolucion','asc')
      ->get();
      break;
      case '1':
      $orden_data=DB::table('detalle_kardex_tienda')
      ->join('kardex_tienda','detalle_kardex_tienda.cod_kardex_tienda','=','kardex_tienda.cod_kardex_tienda')
      ->join('almacen','kardex_tienda.codigo_almacen','=','almacen.cod_almacen')
      ->join('area','detalle_kardex_tienda.cod_area','=','area.cod_area')
      ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where(function ($query) use($almacen){
        if($almacen!="")
        {
          $query->where('almacen.cod_almacen','=',$almacen);
        }
      })
      ->where(function ($query) use ($material,$subcategoria){
        if($subcategoria!="")
        {
          $query->where('subcategoria.cod_subcategoria','=',$subcategoria);
        }
        else {
          if($material!="")
          {
            $query->where('kardex_tienda.codigo_material','=',$material);
          }
        }
      })
      ->where(function ($query) use ($area){
        if($area!="")
        {
          $query->where('detalle_kardex_tienda.cod_area','=',$area);
        }
      })
      ->Where(function ($query) use ($fechas,$fechas_final) {
        $query->orwhere([['detalle_kardex_tienda.fecha_ingreso', '>=', $fechas],['detalle_kardex_tienda.fecha_ingreso', '<=', $fechas_final]]);
      })
      ->orderBy('material.descrip_material','asc')
      ->orderBy('detalle_kardex_tienda.fecha_ingreso','desc')
      ->orderBy('detalle_kardex_tienda.fecha_salida','asc')
      ->orderBy('detalle_kardex_tienda.fecha_devolucion','asc')
      ->get();
      break;
      case '2':
      $orden_data=DB::table('detalle_kardex_tienda')
      ->join('kardex_tienda','detalle_kardex_tienda.cod_kardex_tienda','=','kardex_tienda.cod_kardex_tienda')
      ->join('almacen','kardex_tienda.codigo_almacen','=','almacen.cod_almacen')
      ->join('area','detalle_kardex_tienda.cod_area','=','area.cod_area')
      ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where(function ($query) use($almacen){
        if($almacen!="")
        {
          $query->where('almacen.cod_almacen','=',$almacen);
        }
      })
      ->where(function ($query) use ($material,$subcategoria){
        if($subcategoria!="")
        {
          $query->where('subcategoria.cod_subcategoria','=',$subcategoria);
        }
        else {
          if($material!="")
          {
            $query->where('kardex_tienda.codigo_material','=',$material);
          }
        }
      })
      ->where(function ($query) use ($area){
        if($area!="")
        {
          $query->where('detalle_kardex_tienda.cod_area','=',$area);
        }
      })
      ->Where(function ($query) use ($fechas,$fechas_final) {
        $query->orwhere([['detalle_kardex_tienda.fecha_salida', '>=', $fechas],['detalle_kardex_tienda.fecha_salida', '<=', $fechas_final]]);
      })
      ->orderBy('material.descrip_material','asc')
      ->orderBy('detalle_kardex_tienda.fecha_ingreso','desc')
      ->orderBy('detalle_kardex_tienda.fecha_salida','asc')
      ->orderBy('detalle_kardex_tienda.fecha_devolucion','asc')
      ->get();
      break;
      case '3':
      $orden_data=DB::table('detalle_kardex_tienda')
      ->join('kardex_tienda','detalle_kardex_tienda.cod_kardex_material','=','kardex_tienda.cod_kardex_material')
      ->join('almacen','kardex_tienda.codigo_almacen','=','almacen.cod_almacen')
      ->join('area','detalle_kardex_tienda.cod_area','=','area.cod_area')
      ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where(function ($query) use($almacen){
        if($almacen!="")
        {
          $query->where('almacen.cod_almacen','=',$almacen);
        }
      })
      ->where(function ($query) use ($material,$subcategoria){
        if($subcategoria!="")
        {
          $query->where('subcategoria.cod_subcategoria','=',$subcategoria);
        }
        else {
          if($material!="")
          {
            $query->where('kardex_tienda.codigo_material','=',$material);
          }
        }
      })
      ->where(function ($query) use ($area){
        if($area!="")
        {
          $query->where('detalle_kardex_tienda.cod_area','=',$area);
        }
      })
      ->Where(function ($query) use ($fechas,$fechas_final) {
        $query->orwhere([['detalle_kardex_tienda.fecha_devolucion', '>=', $fechas],['detalle_kardex_tienda.fecha_devolucion', '<=', $fechas_final]]);
      })
      ->orderBy('material.descrip_material','asc')
      ->orderBy('detalle_kardex_tienda.fecha_ingreso','desc')
      ->orderBy('detalle_kardex_tienda.fecha_salida','asc')
      ->orderBy('detalle_kardex_tienda.fecha_devolucion','asc')
      ->get();
      break;
      default:
        // code...
        break;
    }

    return $orden_data;
  }
  function get_data($almacen,$accion,$area,$material,$fecha_inicio,$fecha_final,$subcategoria)
  {
    $fechas= strtotime($fecha_inicio);
    $fechas= date('Y-m-d H:i:s',$fechas);
    $fechas_final= strtotime($fecha_final.' +1 day');
    $fechas_final= date('Y-m-d H:i:s',$fechas_final);
    switch ($accion) {
      case '0':
      $orden_data=DB::table('detalle_kardex_material')
      ->join('kardex_material','detalle_kardex_material.cod_kardex_material','=','kardex_material.cod_kardex_material')
      ->join('almacen','kardex_material.cod_almacen','=','almacen.cod_almacen')
      ->join('area','detalle_kardex_material.cod_area','=','area.cod_area')
      ->join('material','detalle_kardex_material.cod_kardex_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where(function ($query) use($almacen){
        if($almacen!="")
        {
          $query->where('almacen.cod_almacen','=',$almacen);
        }
      })
      ->where(function ($query) use ($material,$subcategoria){
        if($subcategoria!="")
        {
          $query->where('subcategoria.cod_subcategoria','=',$subcategoria);
        }
        else {
          if($material!="")
          {
            $query->where('detalle_kardex_material.cod_kardex_material','=',$material);
          }
        }
      })
      ->where(function ($query) use ($area){
        if($area!="")
        {
          $query->where('detalle_kardex_material.cod_area','=',$area);
        }
      })
      ->Where(function ($query) use ($fechas,$fechas_final) {
        $query->orwhere([['detalle_kardex_material.fecha_ingreso', '>=', $fechas],['detalle_kardex_material.fecha_ingreso', '<=', $fechas_final]])
        ->orwhere([['detalle_kardex_material.fecha_salida', '>=', $fechas],['detalle_kardex_material.fecha_salida', '<=', $fechas_final]])
        ->orwhere([['detalle_kardex_material.fecha_devolucion', '>=', $fechas],['detalle_kardex_material.fecha_devolucion', '<=', $fechas_final]]);
      })
      ->orderBy('material.descrip_material','asc')
      ->orderBy('detalle_kardex_material.fecha_ingreso','desc')
      ->orderBy('detalle_kardex_material.fecha_salida','asc')
      ->orderBy('detalle_kardex_material.fecha_devolucion','asc')
      ->get();
      break;
      case '1':
      $orden_data=DB::table('detalle_kardex_material')
      ->join('kardex_material','detalle_kardex_material.cod_kardex_material','=','kardex_material.cod_kardex_material')
      ->join('almacen','kardex_material.cod_almacen','=','almacen.cod_almacen')
      ->join('area','detalle_kardex_material.cod_area','=','area.cod_area')
      ->join('material','detalle_kardex_material.cod_kardex_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where(function ($query) use($almacen){
        if($almacen!="")
        {
          $query->where('almacen.cod_almacen','=',$almacen);
        }
      })
      ->where(function ($query) use ($material,$subcategoria){
        if($subcategoria!="")
        {
          $query->where('subcategoria.cod_subcategoria','=',$subcategoria);
        }
        else {
          if($material!="")
          {
            $query->where('detalle_kardex_material.cod_kardex_material','=',$material);
          }
        }
      })
      ->where(function ($query) use ($area){
        if($area!="")
        {
          $query->where('detalle_kardex_material.cod_area','=',$area);
        }
      })
      ->Where(function ($query) use ($fechas,$fechas_final) {
        $query->orwhere([['detalle_kardex_material.fecha_ingreso', '>=', $fechas],['detalle_kardex_material.fecha_ingreso', '<=', $fechas_final]]);
      })
      ->orderBy('material.descrip_material','asc')
      ->orderBy('detalle_kardex_material.fecha_ingreso','desc')
      ->orderBy('detalle_kardex_material.fecha_salida','asc')
      ->orderBy('detalle_kardex_material.fecha_devolucion','asc')
      ->get();
      break;
      case '2':
      $orden_data=DB::table('detalle_kardex_material')
      ->join('kardex_material','detalle_kardex_material.cod_kardex_material','=','kardex_material.cod_kardex_material')
      ->join('almacen','kardex_material.cod_almacen','=','almacen.cod_almacen')
      ->join('area','detalle_kardex_material.cod_area','=','area.cod_area')
      ->join('material','detalle_kardex_material.cod_kardex_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where(function ($query) use($almacen){
        if($almacen!="")
        {
          $query->where('almacen.cod_almacen','=',$almacen);
        }
      })
      ->where(function ($query) use ($material,$subcategoria){
        if($subcategoria!="")
        {
          $query->where('subcategoria.cod_subcategoria','=',$subcategoria);
        }
        else {
          if($material!="")
          {
            $query->where('detalle_kardex_material.cod_kardex_material','=',$material);
          }
        }
      })
      ->where(function ($query) use ($area){
        if($area!="")
        {
          $query->where('detalle_kardex_material.cod_area','=',$area);
        }
      })
      ->Where(function ($query) use ($fechas,$fechas_final) {
        $query->orwhere([['detalle_kardex_material.fecha_salida', '>=', $fechas],['detalle_kardex_material.fecha_salida', '<=', $fechas_final]]);
      })
      ->orderBy('material.descrip_material','asc')
      ->orderBy('detalle_kardex_material.fecha_ingreso','desc')
      ->orderBy('detalle_kardex_material.fecha_salida','asc')
      ->orderBy('detalle_kardex_material.fecha_devolucion','asc')
      ->get();
      break;
      case '3':
      $orden_data=DB::table('detalle_kardex_material')
      ->join('kardex_material','detalle_kardex_material.cod_kardex_material','=','kardex_material.cod_kardex_material')
      ->join('almacen','kardex_material.cod_almacen','=','almacen.cod_almacen')
      ->join('area','detalle_kardex_material.cod_area','=','area.cod_area')
      ->join('material','detalle_kardex_material.cod_kardex_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where(function ($query) use($almacen){
        if($almacen!="")
        {
          $query->where('almacen.cod_almacen','=',$almacen);
        }
      })
      ->where(function ($query) use ($material,$subcategoria){
        if($subcategoria!="")
        {
          $query->where('subcategoria.cod_subcategoria','=',$subcategoria);
        }
        else {
          if($material!="")
          {
            $query->where('detalle_kardex_material.cod_kardex_material','=',$material);
          }
        }
      })
      ->where(function ($query) use ($area){
        if($area!="")
        {
          $query->where('detalle_kardex_material.cod_area','=',$area);
        }
      })
      ->Where(function ($query) use ($fechas,$fechas_final) {
        $query->orwhere([['detalle_kardex_material.fecha_devolucion', '>=', $fechas],['detalle_kardex_material.fecha_devolucion', '<=', $fechas_final]]);
      })
      ->orderBy('material.descrip_material','asc')
      ->orderBy('detalle_kardex_material.fecha_ingreso','desc')
      ->orderBy('detalle_kardex_material.fecha_salida','asc')
      ->orderBy('detalle_kardex_material.fecha_devolucion','asc')
      ->get();
      break;
      default:
        // code...
        break;
    }

    return $orden_data;
  }
  function get_cabecera()
  {
    $cabecera=DB::table('empresa')
    ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
    ->get();
    return $cabecera;
  }
  function convert_data_tienda($almacen,$accion,$area,$material,$fecha_inicio,$fecha_final,$subcategoria)
  {
    $mensaje_accion="";
    $mensaje_subcategoria="";
    switch ($accion) {
      case '0':
        $mensaje_accion="TODO";
      break;
      case '1':
      $mensaje_accion="Ingreso";
      break;
      case '2':
      $mensaje_accion="Salida";
      break;
      case '3':
      $mensaje_accion="Devolucion";
      break;
      default:
        // code...
        break;
    }
    $sub=DB::Table('subcategoria')
    ->where('estado_subcategoria','=','1')
    ->get();
    foreach ($sub as $key) {
      if($subcategoria==$key->cod_subcategoria)
      {
        $mensaje_subcategoria=$key->nom_subcategoria;
      }
    }
    if($almacen!=null)
    {
      $alm=DB::Table('almacen')
    ->where('cod_almacen','=',$almacen)
    ->get();
    $mensaje_almacen=$alm[0]->nom_almacen;
    }
    else
    {
      $mensaje_almacen="TODO";
    }
    
    
    $total=0;
    $cingreso=0;
    $csalida=0;
    $detalle=$this->get_data_tienda($almacen,$accion,$area,$material,$fecha_inicio,$fecha_final,$subcategoria);
    $img=$this->get_cabecera();
    $photo="";
    $output='<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {

          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body>';
    foreach ($img as $i) {
        if($i->imagen!="")
        {
          $photo=$i->imagen;
        }
    }
    if ($photo=="") {
      $output.='
      <h3>Reporte de Movimientos Fecha: '.$fecha_inicio.' - '.$fecha_final.'</h3>
      ';
    }
    else {
      $output.='
      <header>
      <div class="row">
        <div class="col-md-12">
          <img src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
        </div>
      </div>
      </header>
      <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
      <h3>Reporte de Movimientos Fecha: '.$fecha_inicio.' - '.$fecha_final.'</h3>
      <h4>Accion: '.$mensaje_accion.'</h4>
      <h4>Subcategoria: '.$mensaje_subcategoria.'</h4>
      <h4>Almacen: '.$mensaje_almacen.'</h4>
      <br>
      <br>
      ';
    }
    $output.='
      <table  style="border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
          <tr>
            <th style="border-collapse: collapse; border: 1px solid black;">Material</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Fecha de Ingreso</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Fecha de Salida</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Fecha de Devolucion</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Cantidad ingresada</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Cantidad Salida</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Stock Actualizado</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Area Entregada</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Personal</th>
          </tr>

    ';
    foreach ($detalle as $dat) {
        if($dat->t_moneda==0)
        {
          $cingreso=$cingreso+($dat->cantidad_ingresada*$dat->costo_sin_igv_material);
          $csalida=$csalida+($dat->cantidad_salida*$dat->costo_sin_igv_material);
        }
        else
        {
          $cingreso=$cingreso+($dat->cantidad_ingresada*($dat->costo_sin_igv_material*3.3));
          $csalida=$csalida+($dat->cantidad_salida*($dat->costo_sin_igv_material*3.3));
        }
        $output.='
        <tr>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_material.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->fecha_ingreso.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->fecha_salida.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->fecha_devolucion.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad_ingresada.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad_salida.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->stock.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_unidad_medida.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_area.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->trasladador_material.'</td>
        </tr>
        ';
    }

    $output.='<tr><td colspan="4">Total de Inversion:</td><td>S/.'.number_format($cingreso, 2, '.', '').'</td><td>S/.'.number_format($csalida, 2, '.', '').'</td><td></td><td></td><td></td><td></td></tr></table>';

    $output.=' </body></html>
    ';
    return $output;
  }
  function convert_data($almacen,$accion,$area,$material,$fecha_inicio,$fecha_final,$subcategoria)
  {
    $mensaje_accion="";
    $mensaje_subcategoria="";
    switch ($accion) {
      case '0':
        $mensaje_accion="TODO";
      break;
      case '1':
      $mensaje_accion="Ingreso";
      break;
      case '2':
      $mensaje_accion="Salida";
      break;
      case '3':
      $mensaje_accion="Devolucion";
      break;
      default:
        // code...
        break;
    }
    $sub=DB::Table('subcategoria')
    ->where('estado_subcategoria','=','1')
    ->get();
    foreach ($sub as $key) {
      if($subcategoria==$key->cod_subcategoria)
      {
        $mensaje_subcategoria=$key->nom_subcategoria;
      }
    }
    $alm=DB::Table('almacen')
    ->where('cod_almacen','=',$almacen)
    ->get();
    $mensaje_almacen=$alm[0]->nom_almacen;
    $total=0;
    $cingreso=0;
    $csalida=0;
    $detalle=$this->get_data($almacen,$accion,$area,$material,$fecha_inicio,$fecha_final,$subcategoria);
    $img=$this->get_cabecera();
    $photo="";
    $output='<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {

          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body>';
    foreach ($img as $i) {
        if($i->imagen!="")
        {
          $photo=$i->imagen;
        }
    }
    if ($photo=="") {
      $output.='
      <h3>Reporte de Movimientos Fecha: '.$fecha_inicio.' - '.$fecha_final.'</h3>
      ';
    }
    else {
      $output.='
      <header>
      <div class="row">
        <div class="col-md-12">
          <img src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
        </div>
      </div>
      </header>
      <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
      <h3>Reporte de Movimientos Fecha: '.$fecha_inicio.' - '.$fecha_final.'</h3>
      <h4>Accion: '.$mensaje_accion.'</h4>
      <h4>Subcategoria: '.$mensaje_subcategoria.'</h4>
      <h4>Almacen: '.$mensaje_almacen.'</h4>
      <br>
      <br>
      ';
    }
    $output.='
      <table  style="border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
          <tr>
            <th style="border-collapse: collapse; border: 1px solid black;">Material</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Fecha de Ingreso</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Fecha de Salida</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Fecha de Devolucion</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Cantidad ingresada</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Cantidad Salida</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Stock Actualizado</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Area Entregada</th>
            <th style="border-collapse: collapse; border: 1px solid black;">Personal</th>
          </tr>

    ';
    foreach ($detalle as $dat) {
        if($dat->t_moneda==0)
        {
          $cingreso=$cingreso+($dat->cantidad_ingresada*$dat->costo_sin_igv_material);
          $csalida=$csalida+($dat->cantidad_salida*$dat->costo_sin_igv_material);
        }
        else
        {
          $cingreso=$cingreso+($dat->cantidad_ingresada*($dat->costo_sin_igv_material*3.3));
          $csalida=$csalida+($dat->cantidad_salida*($dat->costo_sin_igv_material*3.3));
        }
        $output.='
        <tr>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_material.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->fecha_ingreso.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->fecha_salida.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->fecha_devolucion.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad_ingresada.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->cantidad_salida.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->stock.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_unidad_medida.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_area.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->trasladador_material.'</td>
        </tr>
        ';
    }

    $output.='<tr><td colspan="4">Total de Inversion:</td><td>S/.'.number_format($cingreso, 2, '.', '').'</td><td>S/.'.number_format($csalida, 2, '.', '').'</td><td></td><td></td><td></td><td></td></tr></table>';

    $output.=' </body></html>
    ';
    return $output;
  }
}
