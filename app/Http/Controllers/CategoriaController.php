<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\CategoriaModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
class CategoriaController extends Controller
{
  public function __construct()
{
  $this->middleware('admin');
}
public function index(Request $request)
{
  if ($request) {
    $clasificacion=DB::table('categoria')
    ->get();
    return view('Mantenimiento.Categoria.index',["clasificacion"=>$clasificacion]);
  }
}
public function create(Request $request)
{
  if($request)
  {
    return view("Mantenimiento.Categoria.create");
  }

}
public function store()
{
  $identificador=rand(100,999);
  $subcategoria=new CategoriaModel;
  $subcategoria->cod_categoria=$identificador;
  $subcategoria->nom_categoria=Input::get('categoria');
  $subcategoria->save();
  session()->flash('success','Categoría Registrado');
  return Redirect::to('Mantenimiento/Categoria');
}
public function show()
{
  return view('Mantenimiento.Categoria.index');
}
public function edit($id)
{
  return Redirect::to('Mantenimiento/Categoria');
}
public function update()
{
  $cod=Input::get('cod_categoria_editar');
  $descrip_nueva=Input::get('descripcion');
  $act=CategoriaModel::where('cod_categoria',$cod)
  ->update(['nom_categoria'=>$descrip_nueva]);
    session()->flash('success','Categoria Actualizada');
  return Redirect::to('Mantenimiento/Categoria');
}
public function destroy($id)
{
  $cod=Input::get('cod_categoria_eliminar');
  $accion=Input::get('accion');
  if($accion==0)
  {
    $mensaje="Desactivado";
  }
  else {
    $mensaje="Activado";
  }
  $act=CategoriaModel::where('cod_categoria',$cod)
  ->update(['estado_categoria'=>$accion]);
    session()->flash('success','Categoria '.$mensaje);
  return Redirect::to('Mantenimiento/Categoria');
}
}
