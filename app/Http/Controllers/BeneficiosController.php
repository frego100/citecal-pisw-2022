<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\EscalaSalarialModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class BeneficiosController extends Controller
{
  public function __construct()
  {
    $this->middleware('rhumanos');
  }
  public function index(Request $request)
  {
    if ($request) {
      $empresa=Auth::user()->RUC_empresa;
      $micro=DB::table("escala_salarial")
      ->join("trabajador","escala_salarial.cod_trabajador","=","trabajador.DNI_trabajador")
      ->where("escala_salarial.RUC_empresa",'=',$empresa)
      ->where("trabajador.estado_trabajador","=","1")
      ->where("escala_salarial.cod_regimen_laboral","=","1")
      ->where("escala_salarial.estado","=","1")
      ->get();
      $pequeña=DB::table("escala_salarial")
      ->join("trabajador","escala_salarial.cod_trabajador","=","trabajador.DNI_trabajador")
      ->where("escala_salarial.RUC_empresa",'=',$empresa)
      ->where("trabajador.estado_trabajador","=","1")
      ->where("escala_salarial.cod_regimen_laboral","=","2")
      ->where("escala_salarial.estado","=","1")
      ->get();
      $general=DB::table("escala_salarial")
      ->join("trabajador","escala_salarial.cod_trabajador","=","trabajador.DNI_trabajador")
      ->where("escala_salarial.RUC_empresa",'=',$empresa)
      ->where("trabajador.estado_trabajador","=","1")
      ->where("escala_salarial.cod_regimen_laboral","=","3")
      ->where("escala_salarial.estado","=","1")
      ->get();
      return view('recursos_humanos.beneficios.index',["micro"=>$micro,"pequeña"=>$pequeña,"general"=>$general]);
    }
  }
  public function create(Request $request)
  {


  }
  public function store()
  {


  }
  public function show()
  {
  }
  public function edit($id)
  {

  }
  public function update()
  {

  }
  public function destroy($id)
  {

  }
}
