<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\InicioModel;
class InicioController extends Controller
{
    public function store(Request $request){
        $empresa=$request['empresa'];
        $nombre=$request['nombre'];
        $telefono=$request['telefono'];
        $contacto=new InicioModel;
        $contacto->empresa_contacto=$empresa;
        $contacto->nombre_contacto=$nombre;
        $contacto->telefono_contacto=$telefono;
        $contacto->save();
        return "Enviado Satisfactoriamente";
    }
}
