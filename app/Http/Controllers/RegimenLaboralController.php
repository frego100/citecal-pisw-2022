<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\RegimenLaboralModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class RegimenLaboralController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request)
    {
        if($request)
        {
            $laboral=DB::table('regimen_laboral')->get();
            return view('Mantenimiento.Regimen_Laboral.index',["laboral"=>$laboral]);
        }
    }
    public function create(Request $request)
    {
        if($request)
        {
            return view("Mantenimiento.Regimen_Laboral.create");
        }
    }
    public function store()
    {
        $identificador=rand(10000,99999);
        $laborals=new RegimenLaboralModel;
        $laborals->cod_regimen_laboral=$identificador;
        $laborals->descrip_regimen_laboral=Input::get('descripcion');
        $laborals->estado_regimen_laboral=1;
        $laborals->save();
        session()->flash('success','Régimen Laboral Registrado');
        return Redirect::to('Mantenimiento/Regimen_Laboral');
    }
    public function show()
    {
        return view('Mantenimiento.Regimen_Laboral.index');
    }
    public function edit($id)
    {
        return Redirect::to('Mantenimiento/Regimen_Laboral');
    }
    public function update()
    {
      $cod=Input::get('cod_regimen_editar');
      $descripcion=Input::get('descripcion');
        $act=RegimenLaboralModel::where('cod_regimen_laboral',$cod)
        ->update(['descrip_regimen_laboral'=>$descripcion]);
        session()->flash('success','Régimen Laboral Actualizado');
        return Redirect::to('Mantenimiento/Regimen_Laboral');
    }
    public function destroy()
    {
      $cod=Input::get('cod_regimen_eliminar');
      $accion=Input::get('accion');
      if($accion==0)
      {
        $mensaje="Desactivado";
      }
      else {
        $mensaje="Activado";
      }
        $act=RegimenLaboralModel::where('cod_regimen_laboral',$cod)
        ->update(['estado_regimen_laboral'=>$accion]);
        session()->flash('success','Régimen Laboral '.$mensaje);
        return Redirect::to('Mantenimiento/Regimen_Laboral');
    }
}
