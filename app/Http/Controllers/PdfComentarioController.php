<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
class PdfComentarioController extends Controller
{
  public function index(Request $request){
    if ($request) {
      $query=trim($request->get('codigo'));
      $pdf=\App::make('dompdf.wrapper');
      $pdf->loadHTML($this->convert_data($query));
      return $pdf->stream();
    }
  }
  function get_data($query)
  {
    $orden_data=DB::table('comentarios')
    ->join('empresa','comentarios.RUC_empresa','=','empresa.RUC_empresa')
    ->where('estado_comentario','=','1')
    ->get();
    return $orden_data;
  }

  function convert_data($query)
  {
    $detalle=$this->get_data($query);
  	$output='
    <html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {

          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {
          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body>
  	<header><h1>Comentarios de las Empresas</h1></header>
    <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
    <tr>
      <th style="border-collapse: collapse; border: 1px solid black;">Empresa</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Titulo</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Comentario</th>
    </tr>
  ';
    foreach ($detalle as $dat) {
      $output.='
      <tr>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->razon_social.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->titulo.'</td>
        <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->comentario.'</td>
      </tr>
      ';
    }
    $output.='</table>
<br>
<br>
<footer><img src="photo/pie2.png" width="100%" height="100%"/></footer></body></html>
';
      return $output;
  }


}
