<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\MaterialSuministroModel;
use erpCite\Http\Requests\MaterialSuministroFormRequest;
use DB;

class MaterialSuministroController extends Controller
{
    public function __construct(){
        $this->middleware('jefe');
    }

    public function index(Request $request){
        if ($request) {
          $mes_actual=date("m");
            $materialsuministro=DB::table('gasto_suministro')
            ->join('area','gasto_suministro.cod_area','=','area.cod_area')
            ->join('material','gasto_suministro.cod_material','=','material.cod_material')
            ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
            ->where('gasto_suministro.RUC_empresa',Auth::user()->RUC_empresa)
            //->orderBy('area.descrip_area', 'asc')
            ->get();
        return view("costos/indirectos/material_suministro/index",["materialsuministro"=>$materialsuministro]);
        }
    }

    public function create(Request $request){
        if($request)
        {
          $mes_actual=date("m");
          $detalle=DB::table('gasto_suministro')
          ->join('area','gasto_suministro.cod_area','=','area.cod_area')
          ->join('material','gasto_suministro.cod_material','=','material.cod_material')
          ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
          ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
          ->where('gasto_suministro.RUC_empresa','=',Auth::user()->RUC_empresa)
          ->get();
        $area=DB::table('area')
        ->orderBy('descrip_area','asc')->get();
        $tipo_gasto=DB::table('tipo_gasto')->get();
        $categorias=DB::table('categoria')->where('cod_categoria','=',634)->where('estado_categoria','=',1)->orderBy('nom_categoria','asc')->get();
        $subcategorias=DB::table('subcategoria')->where('estado_subcategoria','=',1)->orderBy('nom_subcategoria','asc')->get();
        $materiales=DB::table('material')
        ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
        ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
        ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
        ->where('material.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->where('material.estado_material','=','1')
        ->orderBy('material.descrip_material','asc')
        ->get();
        $cambio=DB::table("configuracion_valores")
        ->where('codigo_valor','=',1)
        ->get();
        return view('costos/indirectos/material_suministro/create',['subcategorias'=>$subcategorias,
        'categorias'=>$categorias,'area'=>$area,'tipo_gasto'=>$tipo_gasto,'materiales'=>$materiales,
        'detalle'=>$detalle,'cambio'=>$cambio]);
        }
    }
  public function store()
  {
    $empresa=Auth::user()->RUC_empresa;
    $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
    $siglax = $sigla[0]->siglas;
    $areatabla=DB::table("area")
    ->orderBy('descrip_area','asc')
    ->get();
    $codigo_material=Input::get("id");
    $areas=Input::get('area');
    $consumo=Input::get('cantidad');
    $valor=Input::get('costo');
    $meses=Input::Get('meses');
    $gasto_mensual=Input::Get('porcentaje');
    $codareas;
    for($i=0;$i<count($areas);$i++)
    {
      for($j=0;$j<count($areatabla);$j++)
      {
          if($areatabla[$j]->descrip_area==$areas[$i])
          {
            $codareas[$i]=$areatabla[$j]->cod_area;
            break;
          }
      }
    }

    for($in=0;$in<count($codigo_material);$in++)
    {
      $articulo=new MaterialSuministroModel;
      $articulo->codigo_suministro=$siglax.'-'.rand(100000,999999);
      $articulo->cod_material=$codigo_material[$in];
      $articulo->consumo=$consumo[$in];
      $articulo->meses_duracion=$meses[$in];
      $articulo->cod_area=$codareas[$in];
      $articulo->gasto_mensual_suministro=$gasto_mensual[$in];
      $articulo->estado_suministro=1;
      $articulo->RUC_empresa=$empresa;
      $articulo->save();
    }

    session()->flash('success','Material Indirecto o Suministro registrado');
    return Redirect::to('costo/indirectos/material_suministro');
  }
    public function show(){
        return view('costos/indirectos/material_suministro/index');
    }
    public function edit($id)
  {
    return Redirect::to('costos/indirectos/material_suministro');
  }
  public function update()
  {
      return Redirect::to('costos/indirectos/material_suministro');
  }
  public function updateall()
  {
    $empresa=Auth::user()->RUC_empresa;
    $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
    $siglax = $sigla[0]->siglas;
    $areatabla=DB::table("area")
    ->orderBy('descrip_area','asc')
    ->get();
    $codigo_material=Input::get("id");
    $areas=Input::get('area');
    $consumo=Input::get('cantidad');
    $valor=Input::get('costo');
    $meses=Input::Get('meses');
    $gasto_mensual=Input::Get('porcentaje');
    $codareas;
    for($i=0;$i<count($areas);$i++)
    {
      for($j=0;$j<count($areatabla);$j++)
      {
          if($areatabla[$j]->descrip_area==$areas[$i])
          {
            $codareas[$i]=$areatabla[$j]->cod_area;
            break;
          }
      }
    }
    $detalle=DB::table('gasto_suministro')
    ->join('material','gasto_suministro.cod_material','=','material.cod_material')
    ->where('gasto_suministro.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->get();
    for($i=0;$i<count($detalle);$i++)
    {
      $f=0;
      for($j=0;$j<count($codigo_material);$j++)
      {
          if($detalle[$i]->cod_material==$codigo_material[$j] && $detalle[$i]->cod_area==$codareas[$j])
          {
            $f=1;
            $act=MaterialSuministroModel::
            where('cod_material',$codigo_material[$j])
            ->where('cod_area',$codareas[$j])
            ->update(['consumo'=>$consumo[$j],
                      'meses_duracion'=>$meses[$j],
                      'gasto_mensual_suministro'=>$gasto_mensual[$j],
                      'estado_suministro'=>1]);
            break;
          }
      }
      if($f==0)
      {
        $act=MaterialSuministroModel::where('cod_material',$detalle[$i]->cod_material)->where('cod_area',$detalle[$i]->cod_area)
        ->delete();
      }
    }
    for($i=0;$i<count($codigo_material);$i++)
    {
      $f=0;
      for($j=0;$j<count($detalle);$j++)
      {
          if($detalle[$j]->cod_material==$codigo_material[$i] && $detalle[$j]->cod_area==$codareas[$i])
          {
            $f=1;
            break;
          }
      }
      if($f==0)
      {
        $articulo=new MaterialSuministroModel;
        $articulo->codigo_suministro=$siglax.'-'.rand(100000,999999);
        $articulo->cod_material=$codigo_material[$i];
        $articulo->consumo=$consumo[$i];
        $articulo->meses_duracion=$meses[$i];
        $articulo->cod_area=$codareas[$i];
        $articulo->gasto_mensual_suministro=$gasto_mensual[$i];
        $articulo->estado_suministro=1;
        $articulo->RUC_empresa=$empresa;
        $articulo->save();
      }
    }
    session()->flash('success','Material Indirecto o Suministro registrados o editados');
    return Redirect::to('costo/indirectos/material_suministro');
  }
  public function destroy()
  {
    $emailmatesu=Input::get('emailmatesu');
    $estado_suministro=Input::get('estado_suministro');
    if($estado_suministro==0){$mensaje="Eliminado";}
    $act=MaterialSuministroModel::where('codigo_suministro',$emailmatesu)
    ->delete([]);
      session()->flash('success','Suministro '.$mensaje);
    return Redirect::to('costo/indirectos/material_suministro');
  }
}
