<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\DetalleOrdenCompra;
use erpCite\OrdenCompra;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;
class OrdenCompraViewController extends Controller
{
  public function __construct()
  {
    $this->middleware('logistica');
  }
  public function index($var)
  {
    if ($var) {
      $empresa=  $idempresa=Auth::user()->RUC_empresa;
      $cabecera=DB::Table('orden_compra')
      ->join('proveedor','orden_compra.RUC_proveedor','=','proveedor.RUC_proveedor')
      ->where('cod_orden_compra','=',$var)
      ->get();
      $detalleorden=DB::table('detalle_orden_compra')
      ->join('material','detalle_orden_compra.cod_material','=','material.cod_material')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
      ->where('subcategoria.cod_categoria','=','969')
      ->where('detalle_orden_compra.cod_orden_compra','=',$var)->get();
      $detalleordeninsu=DB::table('detalle_orden_compra')
      ->join('material','detalle_orden_compra.cod_material','=','material.cod_material')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
      ->where('subcategoria.cod_categoria','=','306')->where('detalle_orden_compra.cod_orden_compra','=',$var)->get();
      $detalleordensumi=DB::table('detalle_orden_compra')
      ->join('material','detalle_orden_compra.cod_material','=','material.cod_material')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
      ->where('subcategoria.cod_categoria','=','634')
      ->where('detalle_orden_compra.cod_orden_compra','=',$var)->get();
      return view('logistica.orden_compra_vista.index',['var'=>$var,'detalleorden'=>$detalleorden,'detalleordeninsu'=>$detalleordeninsu,'detalleordensumi'=>$detalleordensumi,"cabecera"=>$cabecera]);
    }
  }
}
