<?php

namespace erpCite\Http\Controllers;

use erpCite\ClienteVentasModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientesVentasController extends Controller
{

    public function index(Request $request)
    {
        if ($request) {
            $clienteventas = DB::table('cliente_ventas')
                ->join('pais', 'cliente_ventas.pais_id', '=', 'pais.pais_id')
                ->join('departamento', 'cliente_ventas.departamento_id', '=', 'departamento.departamento_id')
                ->get();
            $pais = DB::table('pais')->get();
            $departamentos = DB::table('departamento')->get();
            return view('Ventas.cliente.index', ["clientes" => $clienteventas, "pais" => $pais, "departamentos" => $departamentos]);
        }
    }

    public function show(ClienteVentasModel $clienteVentasModel)
    {
        return $clienteVentasModel;
    }

    public function store()
    {
        $clienteV = new ClienteVentasModel;
        // 0 Para DNI  1 Para RUC
        $clienteV->tipo_documento = Input::get('tipo_documento');
        $clienteV->numero_documento = Input::get('numero_documento');
        $clienteV->nombres = Input::get('nombres');
        $clienteV->apellidos = Input::get('apellidos');
        $clienteV->pais_id = Input::get('pais_id');
        $clienteV->departamento_id = Input::get('departamento_id');
        $clienteV->direccion_1 = Input::get('direccion_1');
        $clienteV->direccion_2 = Input::get('direccion_2');
        $clienteV->celular_1 = Input::get('celular_1');
        $clienteV->celular_2 = Input::get('celular_2');
        $clienteV->correo = Input::get('correo');

        $clienteV->save();
        session()->flash('success', 'Cliente Ventas Registrado');
        return Redirect::to('Ventas/clientes');
    }

    public function update()
    {
        $id = Input::get('client_id_edit');
        $tipoD = Input::get('tipo_documento');
        $numeroD = Input::get('numero_documento_edit_cli');
        $nombres = Input::get('nombres_edit_cli');
        $apellidos = Input::get('apellidos_edit_cli');
        $paisId = Input::get('pais_id_edit_cli');
        $departamentoId = Input::get('departamento_id_edit_cli');
        $direccion1 = Input::get('direccion_1_edit_cli');
        $direccion2 = Input::get('direccion_2_edit_cli');
        $celular1 = Input::get('celular_1_edit_cli');
        $celular2 = Input::get('celular_2_edit_cli');
        $correo = Input::get('correo_edit_cli');

        ClienteVentasModel::where('cliente_id', $id)->update([
            'tipo_documento' => $tipoD,
            'numero_documento' => $numeroD,
            'nombres' => $nombres,
            'apellidos' => $apellidos,
            'pais_id' => $paisId,
            'departamento_id' => $departamentoId,
            'direccion_1' => $direccion1,
            'direccion_2' => $direccion2,
            'celular_1' => $celular1,
            'celular_2' => $celular2,
            'correo' => $correo,
        ]);

        session()->flash('success', 'Cliente Ventas Actualizado');
        return Redirect::to('Ventas/clientes');
    }

    public function destroy()
    {
        $id = Input::get('client_id_delete');
        ClienteVentasModel::where('cliente_id', $id)
            ->delete();
        session()->flash('success', 'Cliente Ventas Eliminado');
        return Redirect::to('Ventas/clientes');
    }

}
