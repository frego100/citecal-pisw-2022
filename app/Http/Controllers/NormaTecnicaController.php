<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\CostoModelo;
use erpCite\DetalleCostoModeloMaterial;
use erpCite\DetalleCostoModeloMano;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class NormaTecnicaController extends Controller
{
  public function __construct()
  {
    $this->middleware('costo');
  }
  public function index($var)
  {
    if ($var) {
      $model=explode("+",$var);
      $datos=DB::table('costo_modelo')
      ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
      ->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
      ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
      ->join('empresa','costo_modelo.RUC_empresa','=','empresa.RUC_empresa')
      ->join('linea','modelo.cod_linea','=','linea.cod_linea')
      ->join('capellada','modelo.cod_capellada','=','capellada.cod_capellada')
      ->join('forro','modelo.cod_forro','=','forro.cod_forro')
      ->join('piso','modelo.cod_piso','=','piso.cod_piso')
      ->join('plantilla','modelo.cod_plantilla','=','plantilla.cod_plantilla')
      ->where('serie_modelo.codigo_modelo','=',$model[0])
      ->where('serie_modelo.codigo_serie','=',$model[1])
      ->get();
        $modelo=DB::table("serie_modelo")
        ->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
        ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
        ->join('empresa','serie_modelo.RUC_empresa','=','empresa.RUC_empresa')
        ->join('linea','modelo.cod_linea','=','linea.cod_linea')
        ->join('capellada','modelo.cod_capellada','=','capellada.cod_capellada')
        ->join('forro','modelo.cod_forro','=','forro.cod_forro')
        ->join('piso','modelo.cod_piso','=','piso.cod_piso')
        ->join('plantilla','modelo.cod_plantilla','=','plantilla.cod_plantilla')
        ->where('serie_modelo.codigo_modelo','=',$model[0])
        ->where('serie_modelo.codigo_serie','=',$model[1])
        ->get();
        $empresa=Auth::user()->RUC_empresa;
        $costeo=DB::Table("costo_modelo")
        ->join("serie_modelo","costo_modelo.modelo_serie","=","serie_modelo.codigo")
        ->join("serie","serie_modelo.codigo_serie","=","serie.cod_serie")
        ->where("costo_modelo.RUC_empresa","=",$empresa)
        ->where("costo_modelo.estado","=",1)
        ->orderBy("serie_modelo.codigo_modelo","asc")
        ->get();
      return view('costos.directos.costos_directos_norma_tecnica.index',["datos"=>$datos,"modelo"=>$modelo,"codigo"=>$model[2],"costos"=>$costeo]);

    }
  }
  public function create(Request $request)
  {
    if($request)
    {
      return view("costos");
    }

  }
  public function store()
  {
    $base=Input::Get("costeo");
    //Se Registra el campo costo_modelo
    $identificador=rand(100000,999999);
    $idempresa=Auth::user()->RUC_empresa;
    $articulo=new CostoModelo;
    $articulo->cod_costo_modelo=$identificador;
    $articulo->RUC_empresa=$idempresa;
    $articulo->estado=1;
    $articulo->modelo_serie=Input::get('cod_modelo');
    $articulo->save();
    if($base!="ninguna")
    {
      $detallepadre=DB::table("detalle_costo_modelo_materiales")
      ->join("costo_modelo","detalle_costo_modelo_materiales.cod_costo_modelo","=","costo_modelo.cod_costo_modelo")
      ->where("costo_modelo.cod_costo_modelo",'=',$base)
      ->where("detalle_costo_modelo_materiales.estado_material_costos",'=',1)
      ->get();
      $total=0;
      for ($i=0; $i < count($detallepadre); $i++) {
        $articulo=new DetalleCostoModeloMaterial;
        $articulo->cod_detalle_costo_material=rand(100000,999999);
        $articulo->cod_costo_modelo=$identificador;
        $articulo->cod_material=$detallepadre[$i]->cod_material;
        $articulo->cod_area=$detallepadre[$i]->cod_area;
        $articulo->consumo_por_par=$detallepadre[$i]->consumo_por_par;
        $articulo->valor_unitario=$detallepadre[$i]->valor_unitario;
        $articulo->total=$detallepadre[$i]->total;
        $articulo->estado_material_costos=1;
        $articulo->save();
        $total=$total+$detallepadre[$i]->total;
      }
      $act=CostoModelo::where('cod_costo_modelo',$identificador)
      ->update(['total_materiales'=>$total]);
      $detallepadre=DB::table("detalle_costo_modelo_manoobra")
      ->join("costo_modelo","detalle_costo_modelo_manoobra.cod_costo_modelo","=","costo_modelo.cod_costo_modelo")
      ->where("costo_modelo.cod_costo_modelo",'=',$base)
      ->where("detalle_costo_modelo_manoobra.estado_trabajador","=",1)
      ->get();
      echo $detallepadre;
      $total=0;
      for ($i=0; $i < count($detallepadre); $i++) {
        $articulo=new DetalleCostoModeloMano;
        $articulo->cod_detalle_costo_mano=rand(100000,999999);
        $articulo->cod_costo_modelo=$identificador;
        $articulo->cod_tipo_trabajador=$detallepadre[$i]->cod_tipo_trabajador;
        $articulo->cod_area=$detallepadre[$i]->cod_area;
        $articulo->operacion=$detallepadre[$i]->operacion;
        $articulo->costo=$detallepadre[$i]->costo;
        $articulo->beneficio=$detallepadre[$i]->beneficio;
        $articulo->costo_por_par=$detallepadre[$i]->costo_por_par;
        $articulo->save();
        $total=$total+$detallepadre[$i]->costo_por_par;
      }
      $act=CostoModelo::where('cod_costo_modelo',$identificador)
      ->update(['total_obra'=>$total]);
    }
    session()->flash('success','Ficha Tecnica Registrada Satisfactoriamente');
    return Redirect::to('costo/vermodelos');
  }
  public function show()
  {
    return view('costos');
  }
  public function edit($id)
  {
    return Redirect::to('costo/vermodelos');
  }
  public function update()
  {
    $codigo_costo=Input::get('cod_costo_modelo');
    $marca=Input::get('marca');
    $capellada=Input::get('capellada');
    $forro=Input::get('forro');
    $piso=Input::get('piso');
    $plantilla=Input::get('plantilla');
    $linea=Input::get('linea');
    $serie=Input::get('serie');
    $act=CostoModelo::where('cod_costo_modelo',$codigo_costo)
    ->update(['marca'=>$marca,
              'cod_capellada'=>$capellada,
              'cod_forro'=>$forro,
              'cod_piso'=>$piso,
              'cod_plantilla'=>$plantilla,
              'linea'=>$linea,
              'serie'=>$serie]);
      session()->flash('success','Ficha Tecnica Actualizada');
    return Redirect::to('costo/vermodelos');
  }
  public function destroy()
  {
    return Redirect::to('costo/vermodelos');
  }
}
