<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;
use erpCite\User;
class UsuarioController extends Controller
{
  public function __construct()
  {
    $this->middleware('admin');
  }
  public function index($var)
  {
    if ($var) {
      $usuarios=DB::table('users')
      ->where('RUC_empresa','=',$var)
      ->get();
      return view('User.index',['var'=>$var,'usuarios'=>$usuarios]);
    }
  }
  public function store()
  {
      $email=Input::get('email');
      $estado=Input::get('estado');
      if($estado==0){$mensaje="Desactivado";}
      else{$mensaje="Activado";}
      $act=User::where('email',$email)
      ->update(['estado'=>$estado]);
        session()->flash('success','Usuario '.$mensaje);
    return Redirect::to('Admin/');
  }

}
