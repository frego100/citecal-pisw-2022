<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\KardexDetalle;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use erpCite\kardex;
use erpCite\KardexTienda;
use Illuminate\Support\Facades\Auth;
use erpCite\Charts\historial;
class HistorialController extends Controller
{
  public function __construct()
  {
    $this->middleware('logistica');
  }
  public function index($var)
  {
    if ($var) {
      $empresa=Auth::user()->RUC_empresa;
      $material=DB::table('material')->where('cod_material','=',$var)->select('descrip_material')->get();
      $detalle=DB::table('detalle_kardex_material')->where('cod_kardex_material','=',$var)->get();
      $detalleingreso=DB::table('detalle_kardex_material')->where('fecha_ingreso','!=','NULL')->where('cod_kardex_material','=',$var)->get();
      $detallesalida=DB::table('detalle_kardex_material')->where('fecha_salida','!=','NULL')->where('cod_kardex_material','=',$var)->get();
      $detalledevolucion=DB::table('detalle_kardex_material')->where('fecha_devolucion','!=','NULL')->where('cod_kardex_material','=',$var)->get();
      $almacenes=DB::Table('almacen')
      ->where('RUC_empresa','=',$empresa)
      ->where('tipo_almacen','=',0)
      ->where('estado_almacen','=',1)
      ->get();
      $ubicacion=DB::table('kardex_material')
      ->where('cod_kardex_material','=',$var)
      ->get();
      return view('logistica.historial.index',["ubicacion"=>$ubicacion,"almacenes"=>$almacenes,"var"=>$var,"detalleingreso"=>$detalleingreso,"detallesalida"=>$detallesalida,"detalledevolucion"=>$detalledevolucion,"detalle"=>$detalle,"nombre_mat"=>$material]);
    }
  }
  public function cambiar_ubicacion()
  {
    $material=Input::Get('cod_material');
    $almacen=Input::get('almacen');
    $ubicacion=Input::Get('ubicacion');
    $plantas=DB::table('material')
    ->where('cod_material','=',$material)
    ->get();
    if($plantas[0]->t_compra==1)
    {
      $array=explode("-",$plantas[0]->descrip_material);
      $descripcion=$array[0];

      $plantas=DB::table('kardex_material')
      ->join('material','kardex_material.cod_material','=','material.cod_material')
      ->where('material.descrip_material','LIKE',$descripcion.'-%')
      ->get();
      for ($i=0; $i < count($plantas) ; $i++) {
        $act= kardex::where('cod_kardex_material',$plantas[$i]->cod_material)
        ->update(['lugar_almacenaje'=>$ubicacion,'cod_almacen'=>$almacen]);
      }
    }
    else {
      $act= kardex::where('cod_kardex_material',$material)
      ->update(['lugar_almacenaje'=>$ubicacion,'cod_almacen'=>$almacen]);
    }
    session()->flash('success','Ubicacion Actualizada');
    return Redirect::to('logistica/kardex');
  }
  public function cambiar_ubicacion_tienda()
  {
    $material=Input::Get('cod_material');
    $almacen=Input::get('almacen');
    $ubicacion=Input::Get('ubicacion');
    $plantas=DB::table('kardex_tienda')
    ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
    ->where('kardex_tienda.cod_kardex_tienda','=',$material)
    ->select('material.descrip_material','material.t_compra')
    ->get();
    if($plantas[0]->t_compra==1)
    {
      $array=explode("-",$plantas[0]->descrip_material);

      $descripcion=$array[0];
      $plantas=DB::table('kardex_tienda')
      ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
      ->where('material.descrip_material','LIKE',$descripcion.'-%')
      ->select('kardex_tienda.cod_kardex_tienda','kardex_tienda.lugar_tienda','kardex_tienda.codigo_almacen')
      ->orderBy('kardex_tienda.cod_kardex_tienda','asc')
      ->get();
      for ($i=0; $i < count($plantas) ; $i++) {
        if($almacen==$plantas[$i]->codigo_almacen)
        {
          $act= KardexTienda::where('cod_kardex_tienda',$plantas[$i]->cod_kardex_tienda)
          ->update(['lugar_tienda'=>$ubicacion]);
        }
      }
    }
    else {
      $act= KardexTienda::where('cod_kardex_tienda',$material)
      ->update(['lugar_tienda'=>$ubicacion]);
    }
    session()->flash('success','Ubicacion Actualizada');
    return Redirect::to('logistica/tiendas');
  }
  public function tiendas_historia($var)
  {
    if ($var) {
      $empresa=Auth::user()->RUC_empresa;
      $inicio=0;
      for($i=0;$i<strlen($var);$i++)
      {
        if($var[$i]>="0" && $var[$i]<="9")
        {

        }
        else
        {
          $inicio=$i;
        break;
        }
      }
      $codigo_material=substr($var,$inicio);
      $material=DB::table('material')->where('cod_material','=',$codigo_material)->select('descrip_material')->get();
      $detalle=DB::table('detalle_kardex_tienda')->where('cod_kardex_tienda','=',$var)->get();
      $detalleingreso=DB::table('detalle_kardex_tienda')->where('fecha_ingreso','!=','NULL')->where('cod_kardex_tienda','=',$var)->get();
      $detallesalida=DB::table('detalle_kardex_tienda')->where('fecha_salida','!=','NULL')->where('cod_kardex_tienda','=',$var)->get();
      $detalledevolucion=DB::table('detalle_kardex_tienda')->where('fecha_devolucion','!=','NULL')->where('cod_kardex_tienda','=',$var)->get();
      $almacenes=DB::Table('almacen')
      ->where('RUC_empresa','=',$empresa)
      ->where('estado_almacen','=',1)
      ->where('tipo_almacen','=','1')
      ->get();
      $ubicacion=DB::table('kardex_tienda')
      ->where('cod_kardex_tienda','=',$var)
      ->get();
      return view('logistica.historial.tienda',["t_historia"=>1,"ubicacion"=>$ubicacion,"almacenes"=>$almacenes,"var"=>$var,"detalleingreso"=>$detalleingreso,"detallesalida"=>$detallesalida,"detalledevolucion"=>$detalledevolucion,"detalle"=>$detalle,"nombre_mat"=>$material]);
    }
  }
}
