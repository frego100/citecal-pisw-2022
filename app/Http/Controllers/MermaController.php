<?php

namespace erpCite\Http\Controllers;

use erpCite\Merma;
use erpCite\SubcategoriaModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class MermaController extends Controller
{
    public function index()
    {
        $ruc_empresa = Auth::user()->RUC_empresa;
        $mermas = Merma::where('empresa_id',$ruc_empresa)->get();
        $subcategorias = SubcategoriaModel::where('cod_categoria','!=','999')->orderBy('nom_subcategoria','asc')->get();
       // dd($subcategorias);
        return view('configuracion_inicial.merma.index',['mermas'=>$mermas,'subcategorias'=>$subcategorias]);
    }
    public function create(Request $request){
        DB::beginTransaction();

        try {
            //dd(Auth::user()->RUC_empresa);
            $merma = Merma::create([
                'tipo_merma'=>$request->tipo_merma,
                'subcategoria_id'=>$request->subcategoria_id,
                'porcentaje_merma'=>$request->porcentaje_merma,
                'empresa_id'=>Auth::user()->RUC_empresa,
                'estado_registro'=>'A',
            ]);

            DB::commit();
            return Redirect::to('configuracion/merma');
        } catch (Exception $e) {
            DB::rollback();
            return Redirect::to('configuracion/merma');
        }
    }
}
