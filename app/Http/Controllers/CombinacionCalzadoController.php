<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use erpCite\Modelo;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use erpCite\ModeloCombinacion;


class CombinacionCalzadoController extends Controller
{
    public function __construct()
    {
      $this->middleware('desarrollo');
    }
    public function index($id)
    {

    $LineasModelos=DB::table('modelo')
    ->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where('modelo.cod_modelo','=',$id)
    ->join(
      'coleccion',
      'modelo.cod_coleccion',
      '=',
      'coleccion.codigo_coleccion'
  )
  ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')
  ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
    //->join('material','modelo.cod_horma','=','material.cod_material')
    //->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
    //->where(function($query){
    //  $query->orwhere('modelo.estado_modelo','=','0')
    //  ->orWhere('modelo.estado_modelo','=','1');
    //})
    ->get();
    $id_modelo=$id;
    $serie = DB::table('serie')
    ->where('RUC_empresa', Auth::user()->RUC_empresa)
    ->where('estado_serie', '=', 1)
    ->get();
    $listaCombinacion=DB::table('combinacion_modelo')
    ->where('combinacion_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where('combinacion_modelo.cod_modelo','=',$id)
    ->get();
    $seriemodelo=DB::Table('serie_modelo')
    ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
    ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where('serie_modelo.codigo_modelo','=',$id)
    ->where('serie_modelo.estado','=','1')
    ->get();
    $todaserie=DB::table('serie')
    ->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->get();
    $capellada=DB::table('capellada')
    ->get();
    $Lineas=DB::table('linea')
    ->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->where('estado_linea','=',1)
    ->get();
    $coleccion=DB::Table('coleccion')
    ->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->where('estado_coleccion','=',1)
    ->get();
    $horma=DB::table('material')
    ->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->where('cod_subcategoria','=','969')
    ->where('t_compra','=','1')
    ->orderBy('descrip_material','asc')
    ->get();
    return view('Produccion.combinacion_calzado.index', compact('id_modelo','horma','listaCombinacion','todaserie','seriemodelo','serie','capellada','Lineas', 'coleccion', 'LineasModelos'));
             //return view('Produccion.modelo.index');
    }

    public function create($id)
    {
      $coleccion=DB::Table('coleccion')
      ->where('RUC_empresa',Auth::user()->RUC_empresa)
      ->where('estado_coleccion','=',1)
      ->get();
      $Lineas=DB::table('linea')
      ->where('RUC_empresa',Auth::user()->RUC_empresa)
      ->where('estado_linea','=',1)
      ->get();
      $serie=DB::table('serie')
      ->where('RUC_empresa',Auth::user()->RUC_empresa)
      ->where('estado_serie','=',1)
      ->get();
      $capellada=DB::table('capellada')
      ->get();
      $plantilla=DB::table('plantilla')
      ->get();
      $forroF=DB::table('forro')
      ->get();
      $piso=DB::table('piso')
      ->get();
      $horma=DB::table('material')
      ->where('RUC_empresa',Auth::user()->RUC_empresa)
      ->where('cod_subcategoria','=','969')
      ->where('t_compra','=','1')
      ->orderBy('descrip_material','asc')
      ->get();
      return view('Produccion.combinacion_calzado.create', compact('plantilla','horma','piso','forroF','capellada','serie','Lineas', 'coleccion'));
    }
    public function store(Request $data)
    {

        return Redirect::to('Produccion/modelos_calzado');


    }
    public function show($id)

    {

        $empresa=Auth::user()->RUC_empresa;
        $cod_modelo_comb=Input::get('codCom');
        $descripcion=Input::get('descripCom');
        $cod_modelo=Input::get('codigo');
        $ModeloCombinacionCalzado=new ModeloCombinacion;
        if(!file_exists('photo/modelos/'.$empresa))
        {
        mkdir('photo/modelos/'.$empresa);
        }
        $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
        $var=Input::get('codigo');
        $siglax = $sigla[0]->siglas;
        $res=$siglax.'-'.$var.'-'.$cod_modelo_comb.$empresa;
        $photo="";
        $destination='photo/modelos/'.$empresa;
        $file= Input::file('photo');
        $extension=$file->getClientOriginalExtension();
        $filename=$res.Input::get('photo').".".$extension;
        $file->move($destination,$filename);
        $photo=$filename;
        $ModeloCombinacionCalzado->codigo_comb=$cod_modelo_comb;
        $ModeloCombinacionCalzado->descripcion=$descripcion;
        $ModeloCombinacionCalzado->image=$photo;
        $ModeloCombinacionCalzado->cod_modelo=$cod_modelo;
        $ModeloCombinacionCalzado->estado_modelo=1;
        $ModeloCombinacionCalzado->aprobado=0;
        $ModeloCombinacionCalzado->modelo_base=0;
        $ModeloCombinacionCalzado->costo_mano_directa=0;
        $ModeloCombinacionCalzado->costo_material_directo=0;
        $ModeloCombinacionCalzado->RUC_empresa=$empresa;
        $ModeloCombinacionCalzado->save();

        $modelo_actualizado=Modelo::where('cod_modelo',$cod_modelo)->first();
        $modelo_actualizado=Modelo::where('cod_modelo',$cod_modelo)
        ->update(['num_combinacion'=>($modelo_actualizado->num_combinacion)+1,
            'comb_proceso'=>($modelo_actualizado->comb_proceso)+1,
          ]);

        //dd(Input::get('coleccion'));
        //this.index($id);
        return Redirect::to('/Produccion/combinacion_calzado/listado/'.$cod_modelo);
        /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
    }
    public function edit($id)
    {
     /* return Redirect::to('logistica/clasificacion');*/
    }
    public function update(Request $data)
      {
        //Codigo de modelo PK
        $cod_modelo=Input::get('codigo');
        //codigo de combinacion a editar PK
        $cod_modelo_combinacion=Input::get('codigo_combinacion');
        //Codigo ingresado por teclado por el usuario
        $codigo_combinacion=Input::get('codCom');
        //Descripcion digitada por el cliente
        $descripcion_combinacion=Input::get('descripCom');

        $sigla = DB::table('empresa')
        ->where('RUC_empresa', Auth::user()->RUC_empresa)
        ->get();
        $var = Input::get('codigo_modelo');
        $siglax = $sigla[0]->siglas;
        $empresa = Auth::user()->RUC_empresa;
        $res=$siglax.'-'.$var.'-'.$cod_modelo_combinacion.$empresa;

        if (!file_exists('photo/modelos/' . $empresa)) {
            mkdir('photo/modelos/' . $empresa);
        }
        $photo = '';
        $destination = 'photo/modelos/' . $empresa;
        $file = $data->photo;
        if($file!=""){
            $extension = $file->getClientOriginalExtension();
            $filename = $res . Input::get('photo') . '.' . $extension;
            $file->move($destination, $filename);
            $photo = $filename;

            $modelo_comb_actualizado=ModeloCombinacion::where('cod_combinacion',$cod_modelo_combinacion)
            ->update(["codigo_comb"=>$codigo_combinacion,
              'descripcion'=>$descripcion_combinacion,
              'image'=> $photo]);

        }else{

            $modelo_comb_actualizado=ModeloCombinacion::where('cod_combinacion',$cod_modelo_combinacion)
            ->update(['codigo_comb'=>$codigo_combinacion,
              'descripcion'=>$descripcion_combinacion,
              ]);

        }

        session()->flash('success','Combinación de Modelo Actualizado');
        return Redirect::to('/Produccion/combinacion_calzado/listado/'.$cod_modelo);
      }
    public function destroy()
    {
      $email=Input::get('email');
      $estado=Input::get('estado');
      $cod_modelo=Input::get('codigo_modelo');
      if($estado==0){$mensaje="Desactivado";}
      else{
        $mensaje="Activado";
      }
      $act=ModeloCombinacion::where('cod_combinacion',$email)
      ->update(['estado_modelo'=>$estado]);
        session()->flash('success','Codigo de Combinación de Modelo '.$mensaje);
      return Redirect::to('/Produccion/combinacion_calzado/listado/'.$cod_modelo);
    }
      public function ficha_producto()
      {
        $cambio=DB::table("configuracion_valores")
        ->where('codigo_valor','=',1)
        ->get();
        $modelos=DB::table('serie_modelo')
        ->join("modelo",'serie_modelo.codigo_modelo','=','modelo.cod_modelo')
        ->join('material','modelo.cod_horma','=','material.cod_material')
        ->join('linea','modelo.cod_linea','=','linea.cod_linea')
        ->join('coleccion','modelo.cod_coleccion','=','codigo_coleccion')
        ->join("serie",'serie_modelo.codigo_serie','=','serie.cod_serie')
        ->leftJoin('costo_modelo','serie_modelo.codigo','=','costo_modelo.modelo_serie')
        ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->where(function($query){
          $query->orWhere('serie_modelo.estado','=','1');
        })
        ->whereNull('costo_modelo.estado')
        ->select('serie_modelo.codigo','costo_modelo.estado','costo_modelo.cod_costo_modelo','modelo.imagen','serie_modelo.codigo_modelo','serie.nombre_serie','linea.nombre_linea','coleccion.nombre_coleccion','material.descrip_material','serie_modelo.RUC_empresa','serie.tallaInicial','serie.tallaFinal')
        ->get();
        $categorias=DB::table('categoria')
        ->where('estado_categoria','=','1')
        ->select('nom_categoria','cod_categoria')
        ->orderBy('nom_categoria','asc')
        ->get();
        $subcategoria=DB::table('subcategoria')
        ->where('estado_subcategoria','=','1')
        ->select('nom_subcategoria','cod_subcategoria','cod_categoria')
        ->orderBy('nom_subcategoria','asc')
        ->get();
        $costeo=DB::table('costo_modelo')
        ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
        ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
        ->where('costo_modelo.estado','=','1')
        ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->select('serie_modelo.codigo','serie_modelo.codigo_modelo','serie.nombre_serie')
        ->get();
        return view("Produccion.modelos_calzado.costeo",["cambio"=>$cambio,"categoria"=>$categorias,"subcategoria"=>$subcategoria,"costeo"=>$costeo,"modelos"=>$modelos]);
      }
      public function detalle_costeo_modelo($var)
      {
        $detalle_costeo_mat=DB::table('costo_modelo')
        ->join('detalle_costo_modelo_materiales','costo_modelo.cod_costo_modelo','=','detalle_costo_modelo_materiales.cod_costo_modelo')
        ->join('material','detalle_costo_modelo_materiales.cod_material','=','material.cod_material')
        ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
        ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
        ->where('modelo_serie','=',$var)
        ->select('detalle_costo_modelo_materiales.cod_area','detalle_costo_modelo_materiales.cod_material','material.descrip_material','material.t_moneda','unidad_compra.descrip_unidad_compra'
        ,'detalle_costo_modelo_materiales.consumo_por_par','unidad_medida.descrip_unidad_medida','detalle_costo_modelo_materiales.total','material.factor_equivalencia','material.costo_sin_igv_material')
        ->get();
        $detalle_costeo_ob=DB::table('costo_modelo')
        ->join('detalle_costo_modelo_manoobra','costo_modelo.cod_costo_modelo','=','detalle_costo_modelo_manoobra.cod_costo_modelo')
        ->where('modelo_serie','=',$var)
        ->select('detalle_costo_modelo_manoobra.cod_area','detalle_costo_modelo_manoobra.operacion','detalle_costo_modelo_manoobra.cod_detalle_costo_mano')
        ->get();
        return array($detalle_costeo_mat,$detalle_costeo_ob);
      }
      public function obt_material($var)
      {
        $empresa=Auth::user()->RUC_empresa;
        $materiales=DB::table('material')
        ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
        ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
        ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
        ->where('material.RUC_empresa','=',$empresa)
        ->where('material.estado_material','=','1')
        ->where('material.cod_subcategoria','=',$var)
        ->orderBy('material.descrip_material','asc')
        ->select('material.cod_material','material.cod_subcategoria','material.t_compra','material.descrip_material'
        ,'material.costo_sin_igv_material','material.factor_equivalencia','unidad_compra.descrip_unidad_compra','unidad_medida.descrip_unidad_medida')
        ->get();
        return $materiales;
      }
      public function guardar_modelo(Request $request)
      {
        if($request["cod_modelo"]!=null && is_array($request["cod"]))
        {
          $reg=CostoModelo::where('modelo_serie',$request["cod_modelo"])->get();
          $codigo=$request["cod"];
          $costos=$request["costo"];
          $cantidades=$request["cantidad"];
          $totales=$request["total"];
          $areas=$request["area"];
          $areatabla=DB::table("area")
          ->where('descrip_area','LIKE','P-%')
          ->get();
          $codareas;
          for($i=0;$i<count($areas);$i++)
          {
            for($j=0;$j<count($areatabla);$j++)
            {
                if($areatabla[$j]->descrip_area==$areas[$i])
                {
                  $codareas[$i]=$areatabla[$j]->cod_area;
                  break;
                }
            }
          }
          if (count($reg)>0) {
            $reg_guardado=DetalleCostoModeloMaterial::where('cod_costo_modelo',$reg[0]->cod_costo_modelo)->select('cod_material','cod_area')->get();
            for ($i=0; $i < count($reg_guardado); $i++) {
              $flag=true;
              for ($j=0; $j < count($codigo); $j++) {
                if ($reg_guardado[$i]->cod_material==$codigo[$j] && $reg_guardado[$i]->cod_area==$codareas[$j]) {

                  DetalleCostoModeloMaterial::where('cod_costo_modelo',$reg[0]->cod_costo_modelo)
                  ->where('cod_material',$codigo[$j])
                  ->where('cod_area',$codareas[$j])
                  ->update([
                    "consumo_por_par"=>$cantidades[$j],
                    "valor_unitario"=>$costos[$j],
                    "total"=>$totales[$j]
                  ]);
                  $flag=false;
                  break;
                }

              }
              if ($flag) {
                DetalleCostoModeloMaterial::where('cod_costo_modelo',$reg[0]->cod_costo_modelo)
                ->where('cod_material',$reg_guardado[$i]->cod_material)
                ->where('cod_area',$reg_guardado[$i]->cod_area)
                ->delete();
              }
            }
            for ($i=0; $i < count($codigo); $i++) {
              $flag=true;
              for ($j=0; $j < count($reg_guardado); $j++) {
                if ($reg_guardado[$j]->cod_material==$codigo[$i] && $reg_guardado[$j]->cod_area==$codareas[$i]) {
                  $flag=false;
                  break;
                }
              }
              if ($flag) {
                $detalle_material=new DetalleCostoModeloMaterial;
                $detalle_material->cod_detalle_costo_material=$request["cod_modelo"]."-".rand(100000,999999);
                $detalle_material->cod_costo_modelo=$reg[0]->cod_costo_modelo;
                $detalle_material->cod_material=$codigo[$i];
                $detalle_material->cod_area=$codareas[$i];
                $detalle_material->consumo_por_par=$cantidades[$i];
                $detalle_material->valor_unitario=$costos[$i];
                $detalle_material->total=$totales[$i];
                $detalle_material->estado_material_costos=1;
                $detalle_material->save();
              }
            }
            $suma=DetalleCostoModeloMaterial::where('cod_costo_modelo',$reg[0]->cod_costo_modelo)
            ->sum('total');
            CostoModelo::where('cod_costo_modelo',$reg[0]->cod_costo_modelo)
            ->update(['total_materiales'=>$suma]);
            return "S";
          }
          else {
            $empresa=Auth::user()->RUC_empresa;
            $sigla=DB::table('empresa')->where('RUC_empresa',$empresa)->get();
            $var=rand(100000,999999);
            $siglax = $sigla[0]->siglas;
            $res=$siglax.'-'.$var;
            $costo_modelo=new CostoModelo;
            $costo_modelo->cod_costo_modelo=$res;
            $costo_modelo->RUC_empresa=$empresa;
            $costo_modelo->total_materiales=0;
            $costo_modelo->total_obra=0;
            $costo_modelo->estado=null;
            $costo_modelo->modelo_serie=$request["cod_modelo"];
            try {
              $costo_modelo->save();
              $total=0;
              for ($i=0; $i < count($codigo); $i++) {
                $detalle_material=new DetalleCostoModeloMaterial;
                $detalle_material->cod_detalle_costo_material=$request["cod_modelo"]."-".rand(100000,999999);
                $detalle_material->cod_costo_modelo=$res;
                $detalle_material->cod_material=$codigo[$i];
                $detalle_material->cod_area=$codareas[$i];
                $detalle_material->consumo_por_par=$cantidades[$i];
                $detalle_material->valor_unitario=$costos[$i];
                $detalle_material->total=$totales[$i];
                $total=$total+$totales[$i];
                $detalle_material->estado_material_costos=1;
                $detalle_material->save();
              }
              CostoModelo::where('cod_costo_modelo',$res)
              ->update(['total_materiales'=>$total]);
              return "S";
            } catch (\Exception $e) {
              return "N";
            }
          }
        }
      }
      public function guardar_mano(Request $request)
      {
        if($request["cod_modelo"]!=null && is_array($request["estado"]))
        {
          $reg=CostoModelo::where('modelo_serie',$request["cod_modelo"])->get();
          $cod_costo=$request["cod_modelo"];
          $costos=$request["costo"];
          $operacion=$request["operacion"];
          $estado=$request["estado"];
          $areas=$request["area"];
          $areatabla=DB::table("area")
          ->where('descrip_area','LIKE','P-%')
          ->get();
          $codareas;
          for($i=0;$i<count($areas);$i++)
          {
            for($j=0;$j<count($areatabla);$j++)
            {
                if($areatabla[$j]->descrip_area==$areas[$i])
                {
                  $codareas[$i]=$areatabla[$j]->cod_area;
                  break;
                }
            }
          }
          if (count($reg)>0) {
            $reg_guardado=DetalleCostoModeloMano::where('cod_costo_modelo',$reg[0]->cod_costo_modelo)->select('cod_detalle_costo_mano','operacion','cod_area')->get();
            for ($i=0; $i < count($reg_guardado); $i++) {
              $flag=true;
              for ($j=0; $j < count($operacion); $j++) {
                if ($reg_guardado[$i]->cod_detalle_costo_mano==$estado[$j]) {
                  DetalleCostoModeloMano::where('cod_detalle_costo_mano',$reg_guardado[$i]->cod_detalle_costo_mano)
                  ->update([
                    "operacion"=>$operacion[$j]
                  ]);
                  $flag=false;
                  break;
                }
              }
              if ($flag) {
                DetalleCostoModeloMano::where('cod_detalle_costo_mano',$reg_guardado[$i]->cod_detalle_costo_mano)
                ->delete();
              }
            }
            for ($i=0; $i < count($operacion); $i++) {
              if ($estado[$i]=="nuevo") {
                if($operacion[$i]!="")
                {
                  $articulo=new DetalleCostoModeloMano;
                  $articulo->cod_detalle_costo_mano=$request["cod_modelo"]."-".rand(100000,999999);
                  $articulo->cod_costo_modelo=$reg[0]->cod_costo_modelo;
                  $articulo->cod_tipo_trabajador=3;
                  $articulo->cod_area=$codareas[$i];
                  $articulo->operacion=$operacion[$i];
                  $articulo->costo=0;
                  $articulo->beneficio=0;
                  $articulo->costo_por_par=0;
                  $articulo->otros_costos=0;
                  $articulo->save();
                }

              }
            }
            return "S";
          }
          else {
            $empresa=Auth::user()->RUC_empresa;
            $sigla=DB::table('empresa')->where('RUC_empresa',$empresa)->get();
            $var=rand(100000,999999);
            $siglax = $sigla[0]->siglas;
            $res=$siglax.'-'.$var;
            $costo_modelo=new CostoModelo;
            $costo_modelo->cod_costo_modelo=$res;
            $costo_modelo->RUC_empresa=$empresa;
            $costo_modelo->total_materiales=0;
            $costo_modelo->total_obra=0;
            $costo_modelo->estado=null;
            $costo_modelo->modelo_serie=$request["cod_modelo"];
            try {
              $costo_modelo->save();
              for ($i=0; $i < count($operacion); $i++) {
                if($operacion[$i]!="")
                {
                  $articulo=new DetalleCostoModeloMano;
                  $articulo->cod_detalle_costo_mano=$request["cod_modelo"]."-".rand(100000,999999);
                  $articulo->cod_costo_modelo=$res;
                  $articulo->cod_tipo_trabajador=3;
                  $articulo->cod_area=$codareas[$i];
                  $articulo->operacion=$operacion[$i];
                  $articulo->costo=0;
                  $articulo->beneficio=0;
                  $articulo->costo_por_par=0;
                  $articulo->otros_costos=0;
                  $articulo->save();
                }
              }
              return "S";
            } catch (\Exception $e) {
              return $e;
            }
          }
        }

      }

      //PDF
      public function descarga($var)
      {
        $query=trim($var);
        $pdf=\App::make('dompdf.wrapper');
        $pdf->setPaper('a4','landscape');
        $pdf->loadHTML($this->convert_data($query));
        return $pdf->stream();
      }
      //OBTENER DATA
      function get_data_cabecera($query)
      {
        $orden_data=CostoModelo::where('modelo_serie',$query)
        ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
        ->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
        ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
        ->join('linea','modelo.cod_linea','=','linea.cod_linea')
        ->join('coleccion','modelo.cod_coleccion','=','coleccion.codigo_coleccion')
        ->join('material','modelo.cod_horma','=','material.cod_material')
        ->select('serie_modelo.codigo','costo_modelo.estado','costo_modelo.cod_costo_modelo','modelo.imagen','serie_modelo.codigo_modelo','serie.nombre_serie','linea.nombre_linea','coleccion.nombre_coleccion','material.descrip_material','serie_modelo.RUC_empresa','serie.tallaInicial','serie.tallaFinal')
        ->get();
        return $orden_data;
      }
      function get_data_materiales($query)
      {
        $orden_data=DetalleCostoModeloMaterial::where('cod_costo_modelo',$query)
        ->join('material','detalle_costo_modelo_materiales.cod_material','=','material.cod_material')
        ->join('unidad_compra','material.unidad_compra','=','unidad_compra.cod_unidad_medida')
        ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
        ->join('area','detalle_costo_modelo_materiales.cod_area','=','area.cod_area')
        ->select('detalle_costo_modelo_materiales.cod_area','detalle_costo_modelo_materiales.cod_material','material.descrip_material','material.t_moneda','unidad_compra.descrip_unidad_compra'
        ,'detalle_costo_modelo_materiales.consumo_por_par','area.descrip_area','unidad_medida.descrip_unidad_medida','detalle_costo_modelo_materiales.total','material.factor_equivalencia','material.costo_sin_igv_material')
        ->orderBy('descrip_area','asc')
        ->get();
        return $orden_data;
      }
      function get_data_mano($query)
      {
        $orden_data=DetalleCostoModeloMano::where('cod_costo_modelo',$query)
        ->join('area','detalle_costo_modelo_manoobra.cod_area','=','area.cod_area')
        ->select('area.descrip_area','detalle_costo_modelo_manoobra.operacion','detalle_costo_modelo_manoobra.cod_detalle_costo_mano')
        ->get();
        return $orden_data;
      }
      function get_imagen()
      {
        $idempresa=Auth::user()->RUC_empresa;
        $imagen=DB::table('empresa')->where('RUC_empresa','=',$idempresa)->limit(1)->get();
        return $imagen;
      }
      //DISEÑAR PDF
      function convert_data($query)
      {
        $cabecera=$this->get_data_cabecera($query);
        if (count($cabecera)>0) {
            $detalle=$this->get_data_materiales($cabecera[0]->cod_costo_modelo);
            $detalle2=$this->get_data_mano($cabecera[0]->cod_costo_modelo);
            $img=$this->get_imagen();
            $photo="";
            foreach ($img as $i) {
                if($i->imagen!="")
                {
                  $photo=$i->imagen;
                }
            }
            $output='<html><head><style>
            @page {
                  margin: 0cm 0cm;
            }
            body {
                  margin-top: 4cm;
                  margin-left: 2cm;
                  margin-right: 2cm;
                  margin-bottom: 2cm;
            }
            header {

                  position: fixed;
                  top: 0.5cm;
                  left: 0.5cm;
                  right: 0cm;
                  height: 3cm;
            }
            footer {
                  margin-right: 0cm;
                  position: fixed;
                  bottom: 0cm;
                  left: 0cm;
                  right: 0cm;
                  height: 2cm;
            }
            </style></head><body>';
            $idempresa=Auth::user()->RUC_empresa;
            $cantidad_mat=count($detalle)+2;
            $cantidad_mano=count($detalle2)+1;
            $rows=$cantidad_mat+$cantidad_mano;
            $tallabase=(floatval($cabecera[0]->tallaFinal)+floatval($cabecera[0]->tallaInicial))/2;
            $horma=explode("-",$cabecera[0]->descrip_material);
            if ($photo!="") {
              $output.='
              <header>
              <table align="center" border="1" style="width:90%">
                <tr align="center">
                  <th  rowspan="4"><img  src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block"></th>
                  <th  colspan="6"><div align="center">Ficha Tecnica de Prototipado</div></th>
                </tr>
                <tr align="center">
                  <th><strong>Modelo:</strong></th>
                  <td>'.$cabecera[0]->codigo_modelo.'</td>
                  <th><strong>Linea:</strong></th>
                  <td>'.$cabecera[0]->nombre_linea.'</td>
                  <th><strong>Coleccion:</strong></th>
                  <td>'.$cabecera[0]->nombre_coleccion.'</td>
                </tr>
                <tr align="center">
                  <th><strong>Horma:</strong></th>
                  <td>'.$horma[0].'</td>
                  <th><strong>Version:</strong></th>
                  <td>00</td>
                  <th><strong>Talla base:</strong></th>
                  <td>'.$tallabase.'</td>
                </tr>
                <tr align="center">
                  <th><strong>Elaborado por:</strong></th>
                  <td></td>
                  <th><strong>Aprobado por:</strong></th>
                  <td></td>
                  <th><strong>Firma Aprobacion:</strong></th>
                  <td></td>
                </tr>
              </table>
              </header>
              <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
              ';
            }
            else {
              $output.='
              <header>
              <table align="center" border="1" style="width:90%">
                <tr align="center">
                  <th  rowspan="3">Sin Logo</th>
                  <th  colspan="6"><div align="center">Ficha Tecnica de Prototipado</div></th>
                </tr>
                <tr align="center">
                  <th><strong>Modelo:</strong></th>
                  <td>'.$cabecera[0]->codigo_modelo.'</td>
                  <th><strong>Linea:</strong></th>
                  <td>'.$cabecera[0]->nombre_linea.'</td>
                  <th><strong>Coleccion:</strong></th>
                  <td>'.$cabecera[0]->nombre_coleccion.'</td>
                </tr>
                <tr align="center">
                  <th><strong>Horma:</strong></th>
                  <td>'.$horma[0].'</td>
                  <th><strong>Version:</strong></th>
                  <td>00</td>
                  <th><strong>Talla base:</strong></th>
                  <td>'.$tallabase.'</td>
                </tr>
              </table>
              </header>
              <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
              ';
            }
            $output.='
            <br>
            <table align="center" border="1" style="width:100%">';
            $output.='<tr align="center">
                <th rowspan="'.$rows.'"><img  src="photo/modelos/'.$idempresa.'/'.$cabecera[0]->imagen.'" alt="" style="width:125px" class="img-rounded center-block"></th>
                <th>Area</th>
                <th>Material</th>
                <th>Precio S/.</th>
                <th>Consumo</th>
                <th>Medida</th>
                <th>Costo_Par S/.</th>
              </tr>';
              //echo $detalle;
            $total_directo=0;
            $lista_area=["P-Corte","P-Habilitado","P-Aparado","P-Alistado","P-Montaje","P-Acabado"];
            for ($i=0; $i < count($lista_area); $i++) {
              foreach ($detalle as $det) {
                if ($lista_area[$i]==$det->descrip_area) {
                  $precio=$det->costo_sin_igv_material;
                  if($det->t_moneda==1)
                  {
                    $precio=$precio*3.33;
                  }
                  $output.='
                  <tr>
                  <td>'.$det->descrip_area.'</td>
                  <td>'.$det->descrip_material.'</td>
                  <td>'.$precio.'</td>
                  <td>'.$det->consumo_por_par.'</td>
                  <td>'.$det->descrip_unidad_medida.'</td>
                  <td>'.number_format($det->total,5,'.','').'</td>
                  </tr>';
                  $total_directo=$total_directo+$det->total;
                }
              }
            }
            $output.='<tr><td colspan="5">Total</td><td style="background-color:yellow">'.$total_directo.'</td></tr>
            <tr>
            <th>Area</th>
            <th colspan="5">Operacion</th>
            </tr>';
            for ($i=0; $i < count($lista_area); $i++) {
              foreach ($detalle2 as $det) {
                if ($lista_area[$i]==$det->descrip_area) {
                  $precio=$det->costo_sin_igv_material;
                  if($det->t_moneda==1)
                  {
                    $precio=$precio*3.33;
                  }
                  $output.='
                  <tr>
                  <td>'.$det->descrip_area.'</td>
                  <td colspan="5">'.$det->operacion.'</td>
                  </tr>';
                }
              }
            }
            $output.='</table>
            </body></html>
            ';
            return $output;
        }
        else {
          return "El modelo no posee ningun material ni operacion registrada";
        }
      }
      function aprobar_modelo(Request $request)
      {
        try {
          $codigo=$request["codigo"];
          $aprobacion=CostoModelo::where('modelo_serie',$codigo)
          ->update(["estado"=>"1"]);
          session()->flash('success','Modelo Aprobado con exito');
        } catch (\Exception $e) {
          session()->flash('error','Error al aprobar el modelo');
        }
      }
    }
