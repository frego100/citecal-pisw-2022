<?php

namespace erpCite\Http\Controllers;

use erpCite\OrdenPedidoEntregaCabeceraModel;
use erpCite\OrdenPedidoEntregaDetalleModel;
use erpCite\OrdenPedidosVentasCabeceraModel;
use erpCite\OrdenPedidosVentasModel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rules\In;

class OrdenPedidosVentasController extends Controller
{
    public function index()
    {
        $ordenes = DB::table('orden_pedidos_ventas')->where('estado', '=', '1')
            ->leftJoin('cliente_ventas', 'orden_pedidos_ventas.cliente_id', '=', 'cliente_ventas.cliente_id')
            ->get();


        return view('Ventas.pedido.index', ['ordenes' => $ordenes]);
    }

    public function create()
    {
        $clientes = DB::table('cliente_ventas')->get();
        $modelos_inventario = DB::table('inventario_producto_terminado')
            ->leftJoin('desarrollo_producto_ventas_tmp', 'inventario_producto_terminado.cod_desarrollo_producto', '=', 'desarrollo_producto_ventas_tmp.cod_desarrollo_producto')
            ->get();

        foreach ($modelos_inventario as $modelo_inventario) {
            $modelo_inventario->tallas = explode("-", $modelo_inventario->tallas);
        }

        return view('Ventas.pedido.create', ['clientes' => $clientes, 'modelos_inventario' => $modelos_inventario]);
    }

    // Almacenar registro submódulo orden_pedido_entregas
    public function storeOrdenEntrega(Request $request)
    {
        //$id_tmp = DB::table('orden_pedido_entrega')->count() + 1;
        // Orden_pedidos_entrega_cabecera
        //$orden_pedido_entrega_cabecera = new OrdenPedidoEntregaCabeceraModel;
        //$orden_pedido_entrega_cabecera->codigo = "OPE-" . date('Y-m-d') . "00-" . $id_tmp;
        //$orden_pedido_entrega_cabecera->orden_pedidos_ventas_id = Input::get('idCabecera');
        //$orden_pedido_entrega_cabecera->total = '100';
        //$orden_pedido_entrega_cabecera->observaciones = Input::get('observaciones');
        // $orden_pedido_entrega_cabecera->save();

        $cantidad_modelos = Input::get('codModelo');
        $id_detalles = Input::get('idDetalle');
        for ($i = 0; $i < sizeof($cantidad_modelos); $i++) {
            $cantidad_tallas_modelo = Input::get('cantidad-tallas-' . $cantidad_modelos[$i]);
            // Orden_pedido_entrega_detalle
//            $orden_pedido_entrega_detalle = new OrdenPedidoEntregaDetalleModel;
//            $orden_pedido_entrega_detalle->orden_pedido_entrega_id = $id_tmp;
//            $orden_pedido_entrega_detalle->orden_pedido_ventas_detalle_id = $id_detalles[$i];
//
//            $orden_pedido_entrega_detalle->cantidad_pedida = Input::get('cantidad-pedida-' . $i);
//            $orden_pedido_entrega_detalle->cantidad_entregada = Input::get('cantidad-entregada-' . $i);
//            $orden_pedido_entrega_detalle->cantidad_faltante = Input::get('cantidad-faltante-' . $i);
//            $orden_pedido_entrega_detalle->total = Input::get('cantidad-costo-' . $i);
//            $orden_pedido_entrega_detalle->save();

            for ($j = 0; $j < $cantidad_tallas_modelo; $j++) {
                $k = $j + 1;
                //ACTUALIZAR REGISTROS
                $cant_talla = Input::get('cantidad-por-talla-' . $j);
                if (is_numeric($cant_talla)) {
                    $cant_talla_original = Input::get('cantidad-por-talla-original-' . $j);
                    $cantidad_actual = $cant_talla_original - $cant_talla;
                    DB::table('orden_pedidos_ventas_detalle')
                        ->where('id', '=', $id_detalles[$i])
                        ->update([('pedido_cantidad_t' . $k) => $cantidad_actual]);
                }
            }
        }

        session()->flash('Success', 'Información agregada correctamente.');

        return Redirect::to('Ventas/pedidos');
    }

    public function store(Request $request)
    {
        $orden_pedidos_ventas_id = DB::table('orden_pedidos_ventas')->count() + 1;
        $codigo_orden_pedidos_ventas = "ORD-PD-VNT-00" . $orden_pedidos_ventas_id;
        $cliente = Input::get('cliente');
        $fecha_entrega = Input::get('fecha_entrega');
        $costo_total = 0;
        $observaciones = Input::get('observaciones');


        // $valor_total_talla = Input::get('cantidad-por-talla-total');

        $modelos = Input::get('codModelo');

        // Obtener monto total
        for ($i = 0; $i < sizeof($modelos); $i++) {
            $costo_total = $costo_total + Input::get('cantidad-por-talla-' . $modelos[$i] . '-total');
        }

        // Generar cabecera del pedido
        $orden_pedidos_cabecera = new OrdenPedidosVentasCabeceraModel;
        $orden_pedidos_cabecera->codigo = $codigo_orden_pedidos_ventas;
        $orden_pedidos_cabecera->cliente_id = $cliente;
        $orden_pedidos_cabecera->fecha_entrega = $fecha_entrega;
        $orden_pedidos_cabecera->costo_total = $costo_total;
        $orden_pedidos_cabecera->observaciones = $observaciones;
        $orden_pedidos_cabecera->save();

        for ($i = 0; $i < sizeof($modelos); $i++) {
            $cant_tmp = Input::get('cantidad-tallas-' . $modelos[$i]);
            $orden_pedido_detalle = new OrdenPedidosVentasModel;
            $orden_pedido_detalle->orden_pedidos_ventas_id = $orden_pedidos_ventas_id;
            $orden_pedido_detalle->modelo_id = $modelos[$i];

            $tmp_cant_pares = 0;
            $tmp_cant_tallas = "";

            for ($j = 0; $j < $cant_tmp; $j++) {
                $k = $j + 1;
                $tmp_cant_talla = Input::get('cantidad-por-talla-' . $modelos[$i] . '-' . $j);
                $tmp_cant_pares = $tmp_cant_pares + $tmp_cant_talla;
                $orden_pedido_detalle->{'pedido_cantidad_t' . $k} = $tmp_cant_talla;
                $tmp_cant_tallas = $tmp_cant_tallas . $tmp_cant_talla;
                if ($j != $cant_tmp - 1) {
                    $tmp_cant_tallas = $tmp_cant_tallas . "-";
                }
            }
            $orden_pedido_detalle->pedido_cantidad_tallas = $tmp_cant_tallas;
            $orden_pedido_detalle->cantidad_total_pares = $tmp_cant_pares;
            $orden_pedido_detalle->valor_total = Input::get('cantidad-por-talla-' . $modelos[$i] . '-total');
            $orden_pedido_detalle->save();
        }

        session()->flash('Success', 'Información agregada correctamente.');

        return Redirect::to('Ventas/pedidos');
    }

    public function show($id)
    {
        $orden_pedido = DB::table('orden_pedidos_ventas')
            ->where('orden_pedidos_ventas.id', '=', $id)
            ->leftJoin('cliente_ventas', 'cliente_ventas.cliente_id', '=', 'orden_pedidos_ventas.cliente_id')
            ->get();

        $orden_pedidos_ventas_detalles = DB::table('orden_pedidos_ventas_detalle')
            ->where('orden_pedidos_ventas_detalle.orden_pedidos_ventas_id', '=', $id)
            ->leftJoin('inventario_producto_terminado', 'inventario_producto_terminado.cod_producto_terminado', '=', 'orden_pedidos_ventas_detalle.modelo_id')
            ->leftJoin('desarrollo_producto_ventas_tmp', 'desarrollo_producto_ventas_tmp.cod_desarrollo_producto', '=', 'inventario_producto_terminado.cod_desarrollo_producto')
            ->get();

        foreach ($orden_pedidos_ventas_detalles as $orden) {
            $orden->tallas = explode("-", $orden->tallas);
        }

        return view('Ventas.pedido.entrega', ["idPedido" => $id, "orden_pedido" => $orden_pedido, "orden_pedidos_ventas_detalles" => $orden_pedidos_ventas_detalles]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        DB::table('orden_pedidos_ventas')
            ->where('id', '=', $id)
            ->update(['estado' => '0']);

        session()->flash('Success', 'Registro eliminado correctamente.');
        return Redirect::to('Ventas/pedidos');
    }

    public function getDataDesarrollo()
    {
        $registrosModelos = DB::table('desarrollo_producto_ventas_tmp')
            ->where('desarrollo_producto_ventas_tmp.estado', '=', 1)
            ->get();
        $modelos = [];
        foreach ($registrosModelos as $modelo) {
            $tallas = explode("-", $modelo->tallas);
            $modelos = Arr::add(
                $modelos,
                $modelo->cod_desarrollo_producto,
                new Modelo(
                    $modelo->cod_desarrollo_producto,
                    $modelo->imagen, // "/images/modelo-test-001.png",
                    $modelo->coleccion,
                    $modelo->linea,
                    $modelo->serie,
                    $modelo->cod_modelo,
                    $modelo->desc_modelo,
                    $tallas
                )
            );
        }
        return $modelos;
    }
}
