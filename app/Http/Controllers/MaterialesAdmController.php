<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\Serie;
use erpCite\Empresa;
use erpCite\CostoMateriales;
use erpCite\MaterialSuministroModel;
use erpCite\ManoObraModel;
use erpCite\GastosRepresentacion;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;


class MaterialesAdmController extends Controller
{
  public function __construct()
  {
    $this->middleware('jefe');
  }
  public function index(Request $request)
  {
    if ($request) {
      $mes_actual=date("m");
      $idempresa=Auth::user()->RUC_empresa;
      $manoobra=DB::table('gasto_sueldos')
      ->join('area','gasto_sueldos.cod_area','=','area.cod_area')
      ->where('gasto_sueldos.RUC_empresa',$idempresa)
      ->where('gasto_sueldos.cod_area','=',36637)
      ->where('gasto_sueldos.es_externo','=',0)
      ->whereMonth('gasto_sueldos.fecha_creacion', '=', $mes_actual)
      ->get();
      $materiales=DB::Table('gasto_suministro')
      ->join('material','gasto_suministro.cod_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
      ->where('gasto_suministro.RUC_empresa','=',$idempresa)
      ->where('gasto_suministro.cod_area','=',36637)
      ->get();
      $representacion=DB::Table('gasto_representacion')
      ->where('gasto_representacion.RUC_empresa',$idempresa)
      ->whereMonth('gasto_representacion.fecha_creacion', '=', $mes_actual)
      ->get();
      return view('costos.indirectos.MaterialesAdm.index',['materiales'=>$materiales,'manoobra'=>$manoobra,'representacion'=>$representacion]);
    }
  }
  public function crear_material()
  {
    $idempresa=Auth::user()->RUC_empresa;
    $material=DB::table('material')
    ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
    ->where('material.cod_subcategoria','=','975')
    ->where('material.RUC_empresa','=',$idempresa)
    ->get();
    return view("costos.indirectos.MaterialesAdm.crear_material",['material'=>$material]);
  }
  public function guardar_material()
  {
    $idempresa=Auth::user()->RUC_empresa;
    $sigla=DB::table('empresa')->where('RUC_empresa',$idempresa)->get();
    $siglax = $sigla[0]->siglas;
    $codigo_material=Input::get("material");
    $areas=36637;
    $consumo=Input::get('consumo');
    $meses=Input::Get('meses');
    $gasto_mensual=Input::Get('gasto_mensual');

    $detalle=DB::table('gasto_suministro')
    ->where('gasto_suministro.cod_material','=',$codigo_material)
    ->where('gasto_suministro.cod_area','=',$areas)
    ->where('gasto_suministro.RUC_empresa','=',$idempresa)
    ->get();
    if (count($detalle)==0) {
      $articulo=new MaterialSuministroModel;
      $articulo->codigo_suministro=$siglax.'-'.rand(100000,999999);
      $articulo->cod_material=$codigo_material;
      $articulo->consumo=$consumo;
      $articulo->meses_duracion=$meses;
      $articulo->cod_area=$areas;
      $articulo->gasto_mensual_suministro=$gasto_mensual;
      $articulo->estado_suministro=1;
      $articulo->RUC_empresa=$idempresa;
      $articulo->save();
      session()->flash('success','Material Administrativo Guardado');
    }
    else {
      session()->flash('warning','Material Administrativo ya se encuentra registrado');
    }
    return Redirect::to('costos_indirectos/MaterialesAdm');
  }
  public function crear_sueldo()
  {
    $area=DB::table('area')
    ->orderBy('descrip_area','asc')->get();
    $general=DB::Table('datos_generales')
    ->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->get();

    $empresas=DB::table('empresa')
    ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
    ->select('cod_regimen_laboral')
    ->get();
    $beneficios=DB::table('escala_salarial')
    ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where('cod_regimen_laboral','=',$empresas[0]->cod_regimen_laboral)
    ->avg('tasa_beneficio');
    return view("costos.indirectos.MaterialesAdm.crear_sueldo",['area'=>$area,'beneficios'=>$beneficios]);
  }
  public function guardar_sueldo()
  {
    //Se Registra el campo detalle_orden_compra
    $identificador=rand(100000,999999);
    $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
    $siglax = $sigla[0]->siglas;
    $res=$siglax.'-'.$identificador;
    $idempresa=Auth::user()->RUC_empresa;
    $manoobra=new ManoObraModel;
    $manoobra->id_gastos_sueldos=$res;
    $manoobra->beneficios=Input::get('beneficios_sociales');
    $manoobra->otros=Input::get('otros_sueldos');
    $manoobra->gasto_mensual=Input::get('gasto_mensual');
    $manoobra->cod_area=Input::get('cod_area');
    $manoobra->RUC_empresa=$idempresa;
    $manoobra->sueldo_mensual=Input::get('sueldo_mensual');
    $manoobra->puesto=Input::get('puesto');
    $manoobra->fecha_creacion=Input::get('fecha_creacion');
    $manoobra->estado=1;
    $manoobra->save();
    session()->flash('success','Sueldo Administrativo registrado');
    return Redirect::to('costos_indirectos/MaterialesAdm');
  }
  public function crear_representacion()
  {
    return view("costos.indirectos.MaterialesAdm.crear_representacion");
  }
  public function guardar_representacion()
  {
    $identificador=rand(100000,999999);
    $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
    $siglax = $sigla[0]->siglas;
    $res=$siglax.'-'.$identificador;
    $idempresa=Auth::user()->RUC_empresa;
    $manoobra=new GastosRepresentacion;
    $manoobra->cod_gasrepre=$res;
    $manoobra->descripcion=Input::get('descripcion');
    $manoobra->fecha_creacion=Input::get('fecha_creacion');
    $manoobra->estado=1;
    $manoobra->gasto=Input::get('gasto');
    $manoobra->RUC_empresa=$idempresa;
    $manoobra->save();
    session()->flash('success','Gasto representativo registrado');
    return Redirect::to('costos_indirectos/MaterialesAdm');
  }
  public function borrar_material()
  {
    $email=Input::get('email');
    $act=MaterialSuministroModel::where('cod_material',$email)
    ->delete();
      session()->flash('success','Material administrativo eliminado');
    return Redirect::to('costos_indirectos/MaterialesAdm');
  }
  public function borrar_sueldo()
  {
    $email=Input::get('email');
    $act=ManoObraModel::where('id_gastos_sueldos',$email)
    ->delete();
      session()->flash('success','Mano de obra administrativo eliminado');
    return Redirect::to('costos_indirectos/MaterialesAdm');
  }
  public function borrar_representacion()
  {
    $email=Input::get('email');
    $act=GastosRepresentacion::where('cod_gasrepre',$email)
    ->delete();
      session()->flash('success','Gasto de representacion Eliminado');
    return Redirect::to('costos_indirectos/MaterialesAdm');
  }
}
