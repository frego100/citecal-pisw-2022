<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\FinancieroModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class FinancieroController extends Controller
{
  public function __construct(){
      $this->middleware('jefe');
  }
    public function index()
    {
      $empresa=Auth::user()->RUC_empresa;
      $prestamo=DB::table('gasto_financiero')
      ->where('estado','=','1')
      ->where('RUC_empresa','=',$empresa)
      ->get();
      $negociacion=DB::table('gasto_financiero')
      ->where('estado','=','2')
      ->where('RUC_empresa','=',$empresa)
      ->get();
      $letra=DB::table('gasto_financiero')
      ->where('estado','=','3')
      ->where('RUC_empresa','=',$empresa)
      ->get();

      return view('costos.indirectos.Financiero.ej',['prestamo'=>$prestamo,'negociacion'=>$negociacion,'letra'=>$letra]);
    }
    public function crear_prestamo()
    {
      return view('costos.indirectos.Financiero.crear_prestamo');
    }
    public function almacenar_prestamo()
    {
      $empresa=Auth::user()->RUC_empresa;
      $aleatorio=rand(100000,999999);
      $entidad=Input::get('entidad');
      $interes=Input::get('intereses');
      $fecha=Input::get('fecha');
      $mes=Input::get('meses');
      $sigla=DB::table('empresa')->where('RUC_empresa',$empresa)->get();
      $siglax = $sigla[0]->siglas;
      $res=$siglax.'-'.$aleatorio;
      $trabajador=new FinancieroModel;
      $trabajador->cod_gasto=$res;
      $trabajador->intereses=$interes;
      $trabajador->RUC_empresa=$empresa;
      $trabajador->fecha_prestamo=$fecha;
      $trabajador->meses_pago=$mes;
      $trabajador->estado=1;
      $trabajador->descripcion_financiera=$entidad;
      $trabajador->save();
      session()->flash('success','Prestamo registrado');
      return Redirect::to('costos_indirectos/financiero');
    }
    public function crear_gasto()
    {
      return view('costos.indirectos.Financiero.crear_gasto');
    }
    public function crear_letra()
    {
      return view('costos.indirectos.Financiero.crear_letra');
    }
    public function almacenar_gasto()
    {
      $empresa=Auth::user()->RUC_empresa;
      $aleatorio=rand(10000,99999);
      $entidad=Input::get('entidad');
      $interes=Input::get('intereses');
      $fecha=Input::get('fecha');
      $mes=Input::get('meses');
      $estado=Input::get('estado');
      $sigla=DB::table('empresa')->where('RUC_empresa',$empresa)->get();
      $siglax = $sigla[0]->siglas;
      $res=$siglax.'-'.$aleatorio;
      $trabajador=new FinancieroModel;
      $trabajador->cod_gasto=$res;
      $trabajador->intereses=$interes;
      $trabajador->RUC_empresa=$empresa;
      $trabajador->fecha_prestamo=$fecha;
      $trabajador->meses_pago=$mes;
      $trabajador->estado=$estado;
      $trabajador->descripcion_financiera=$entidad;
      $trabajador->save();
      session()->flash('success','Gasto registrado');
      return Redirect::to('costos_indirectos/financiero');
    }
    public function eliminar()
    {
        $entidad=Input::get('entidad');
        $trabajador=FinancieroModel::where('cod_gasto','=',$entidad)
        ->delete();
        session()->flash('success','Gasto Eliminado');
        return Redirect::to('costos_indirectos/financiero');
    }
}
