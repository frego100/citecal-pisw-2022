<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\kardex;
use erpCite\KardexDetalle;
use erpCite\DetalleOrdenCompra;
use erpCite\OrdenCompra;
use erpCite\Material;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;


class RecogerController extends Controller
{
  public function __construct()
  {
    $this->middleware('logistica');
  }
  public function index($var)
  {
    if ($var) {

      $empresa=  $idempresa=Auth::user()->RUC_empresa;
      $cabecera=DB::Table('orden_compra')
      ->join('proveedor','orden_compra.RUC_proveedor','=','proveedor.RUC_proveedor')
      ->where('cod_orden_compra','=',$var)
      ->get();
      $detalleorden=DB::table('detalle_orden_compra')
      ->join('material','detalle_orden_compra.cod_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('subcategoria.cod_categoria','=','969')
      ->where('detalle_orden_compra.cod_orden_compra','=',$var)
      ->where('detalle_orden_compra.cantidad_restante','>','0')
      ->where('material.t_compra','=','0')
      ->get();
      $detalleordeninsu=DB::table('detalle_orden_compra')
      ->join('material','detalle_orden_compra.cod_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('subcategoria.cod_categoria','=','306')->where('detalle_orden_compra.cod_orden_compra','=',$var)
      ->where('detalle_orden_compra.cantidad_restante','>','0')
      ->where('material.t_compra','=','0')
      ->get();
      $detalleordensumi=DB::table('detalle_orden_compra')
      ->join('material','detalle_orden_compra.cod_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('subcategoria.cod_categoria','=','634')
      ->where('detalle_orden_compra.cod_orden_compra','=',$var)
      ->where('detalle_orden_compra.cantidad_restante','>','0')
      ->where('material.t_compra','=','0')
      ->get();
      $detalletallas=DB::table('detalle_orden_compra')
      ->join('material','detalle_orden_compra.cod_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('detalle_orden_compra.cod_orden_compra','=',$var)
      ->where('detalle_orden_compra.cantidad_restante','>','0')
      ->where('material.t_compra','=','1')
      ->orderBy('material.descrip_material','asc')
      ->get();
      if(!count($detalleorden) && !count($detalleordeninsu) && !count($detalleordensumi) && !count($detalletallas))
      {
        $actualizar=OrdenCompra::where('cod_orden_compra',$var)
        ->update(['estado_orden_compra'=>'0']);
      }
      $trabajadores=DB::table('trabajador')->where('RUC_empresa','=',$empresa)->get();
      $kardex=DB::table('kardex_material')
      ->join('almacen','kardex_material.cod_almacen','=','almacen.cod_almacen')
      ->join('material','kardex_material.cod_material','=','material.cod_material')
      ->where('kardex_material.RUC_empresa','=',$empresa)->get();
      $almacen=DB::table('almacen')->where('RUC_empresa','=',$empresa)->where('tipo_almacen','=','0')->where('estado_almacen','=','1')->get();
      return view('logistica.recoger.index',['var'=>$var,'detalleorden'=>$detalleorden,'detalleordeninsu'=>$detalleordeninsu,
      'detalleordensumi'=>$detalleordensumi,'kardex'=>$kardex,'almacen'=>$almacen,'trabajadores'=>$trabajadores,'detalletallas'=>$detalletallas,"cabecera"=>$cabecera]);
    }
  }

  public function store(Request $data)
  {
    try
    {
      $errores="";
      $stocktotal;
      $Today=date('y:m:d');
      $id=Input::get('cod_orden');
      $mytime = date('Y-m-d G:i:s');
      $empresa=  $idempresa=Auth::user()->RUC_empresa;
      $cod_material=Input::get('cod_material');
      $cod_tallas=Input::get('codtalla');
      $cantidadsinrecibir=Input::get('cantidad');
      $cod_almacen=Input::get('almacen');
      $lugaralmacenaje=Input::get('lugar');
      $personal_traslado=Input::get('persona_traslado');
      $cantidadrecogida=Input::get('recogido');
      $comentario=Input::get('comentarios');
      $act=OrdenCompra::where('cod_orden_compra',$id)
      ->update(['comentario_oc'=>$comentario]);
      if(is_array($cod_material))
      {
        for ($i=0; $i < count($cod_material) ; $i++) {
            if( $personal_traslado[$i]=="" && $cantidadrecogida[$i]=="" )
            {

            }
            else {
              $cantidad=DB::Table('detalle_orden_compra')
              ->where('cod_material','=',$cod_material[$i])
              ->where('cod_orden_compra','=',$id)
              ->get();
              $costo_unitario=$cantidad[0]->costo_unitario;
              $restante=$cantidadsinrecibir[$i]-$cantidadrecogida[$i];
              if($cantidadrecogida[$i]>$cantidadsinrecibir[$i])
              {
                $errores.=$cod_material[$i]." ";
                $cantidadrecogida[$i]=$cantidadsinrecibir[$i];
                $restante=0;
              }
              $kardex=DB::table('kardex_material')->where('RUC_empresa','=',$empresa)->where('cod_material','=',$cod_material[$i])->get();
              if(count($kardex))
              {
                $detalle=new KardexDetalle;
                $detalle->dni_usuario=Auth::user()->id;
                $detalle->costo_material=$costo_unitario;
                $detalle->cod_area=47812;
                $detalle->estado_detalle_kardex=1;
                $detalle->restante_detalle=$cantidadrecogida[$i];
                $detalle->cod_kardex_material=$cod_material[$i];
                $detalle->fecha_ingreso=$mytime;
                $record=DB::table('detalle_kardex_material')->where('cod_kardex_material','=',$cod_material[$i])->get();
                if(count($record))
                {
                  $cantidad=count($record)-1;
                  $detalle->stock=$record[$cantidad]->stock+$cantidadrecogida[$i];
                  $stocktotal=$record[$cantidad]->stock+$cantidadrecogida[$i];
                }
                else {
                  $detalle->stock=$record[0]->stock+$cantidadrecogida[$i];
                  $stocktotal=$record[0]->stock+$cantidadrecogida[$i];
                }
                $detalle->cantidad_ingresada=$cantidadrecogida[$i];
                $detalle->trasladador_material=$personal_traslado[$i];
                $detalle->save();
                $act=DetalleOrdenCompra::where('cod_orden_compra',$id)
                ->where('cod_material',$cod_material[$i])
                ->update(['cantidad_restante'=>$restante,'cantidad_recibida'=>$cantidadrecogida[$i]]);
                $act=DB::table('detalle_orden_compra')
                ->where('cod_orden_compra',$id)
                ->where('cod_material',$cod_material[$i])
                ->whereNull('fecha_deposito')
                ->get();
                if(count($act)>0)
                {
                  for ($j=0; $j < count($act); $j++) {
                    $fpago=Date('y:m:d', strtotime("+".$act[$j]->fecha_pago." days"));
                    $actfecha=DetalleOrdenCompra::where('cod_orden_compra',$id)
                    ->where('cod_material',$cod_material[$i])
                    ->whereNull('fecha_deposito')
                    ->update(['fecha_deposito'=>$fpago]);
                  }
                }
                $act=Kardex::where('cod_material',$cod_material[$i])
                ->update(['stock_total'=>$stocktotal]);

              }
              else {
                $producto=new Kardex;
                try {

                  $producto->cod_kardex_material=$cod_material[$i];
                  $producto->cod_almacen=$cod_almacen[$i];
                  $producto->cod_material=$cod_material[$i];
                  $producto->RUC_empresa=$empresa;
                  $producto->lugar_almacenaje=strtoupper($lugaralmacenaje[$i]);
                  $detalle=new KardexDetalle;
                  $detalle->dni_usuario=Auth::user()->id;
                  $detalle->costo_material=$costo_unitario;
                  $detalle->cod_area=47812;
                  $detalle->estado_detalle_kardex=1;
                  $detalle->restante_detalle=$cantidadrecogida[$i];
                  $detalle->cod_kardex_material=$cod_material[$i];
                  $detalle->fecha_ingreso=$mytime;
                  $detalle->stock=$cantidadrecogida[$i];
                  $stocktotal=$cantidadrecogida[$i];
                  $detalle->cantidad_ingresada=$cantidadrecogida[$i];
                  $detalle->trasladador_material=$personal_traslado[$i];
                  $producto->save();
                  $detalle->save();
                  $act=DetalleOrdenCompra::where('cod_orden_compra',$id)
                  ->where('cod_material',$cod_material[$i])
                  ->update(['cantidad_restante'=>$restante,'cantidad_recibida'=>$cantidadrecogida[$i]]);
                  $act=DB::table('detalle_orden_compra')
                  ->where('cod_orden_compra',$id)
                  ->where('cod_material',$cod_material[$i])
                  ->whereNull('fecha_deposito')
                  ->get();
                  if(count($act)>0)
                  {
                    for ($j=0; $j < count($act); $j++) {
                      $fpago=Date('y:m:d', strtotime("+".$act[$j]->fecha_pago." days"));
                      $actfecha=DetalleOrdenCompra::where('cod_orden_compra',$id)
                      ->where('cod_material',$cod_material[$i])
                      ->whereNull('fecha_deposito')
                      ->update(['fecha_deposito'=>$fpago]);
                    }
                  }
                  $act=Kardex::where('cod_material',$cod_material[$i])
                  ->update(['stock_total'=>$stocktotal]);
                } catch (Exception $e) {
                  break;
                }
              }
            }

        }
      }
      if(is_array($cod_tallas))
        {
          for ($i=0; $i <  count($cod_tallas); $i++) {
            $cantidadessinrecibir_tallas=Input::get($cod_tallas[$i].'res');
            $cantidadrecibida_tallas=Input::get($cod_tallas[$i].'ca');
            $almacen_tallas=Input::get($cod_tallas[$i].'alma');
            $lugar_tallas=Input::get($cod_tallas[$i].'lug');
            $persona_talla=Input::get($cod_tallas[$i].'per');
            $familia=Input::get($cod_tallas[$i]);
            $tallasp=Input::get($cod_tallas[$i].'tallap');
            $materiales=DB::table('material')
            ->where('RUC_empresa','=',$empresa)
            ->where('descrip_material','like',$familia.'-%')
            ->where('estado_material','=','1')
            ->orderBy('descrip_material','asc')
            ->get();
              for($j=0; $j<count($materiales);$j++)
              {
                $codigo_mat=$materiales[$j]->cod_material;
                $cadena=$materiales[$j]->descrip_material;
                $pos=strpos($cadena,"-");
                $descripcion=substr($cadena,$pos+1);
                for($k=0;$k<count($tallasp);$k++)
                {
                  if($descripcion==$tallasp[$k] && $cantidadrecibida_tallas[$k]!="")
                  {
                    $kardex=DB::table('kardex_material')
                    ->where('RUC_empresa','=',$empresa)
                    ->where('cod_material','=',$materiales[$j]->cod_material)
                    ->get();
                    $costo=DB::table('detalle_orden_compra')
                    ->where('cod_material','=',$materiales[$j]->cod_material)
                    ->where('cod_orden_compra','=',$id)
                    ->select('costo_unitario')
                    ->get();
                    $costo_unitario="0";
                    foreach($costo as $cost)
                    {
                      $costo_unitario=$cost->costo_unitario;
                    }
                    if(count($kardex))
                    {
                      $restante=$cantidadessinrecibir_tallas[$k]-$cantidadrecibida_tallas[$k];
                      if($cantidadrecibida_tallas[$k]>$cantidadessinrecibir_tallas[$k])
                      {
                        $errores.=$codigo_mat." ";
                        $cantidadrecibida_tallas[$k]=$cantidadessinrecibir_tallas[$k];
                        $restante=0;
                      }
                      $detalle=new KardexDetalle;
                      $detalle->costo_material=$costo_unitario;
                      $detalle->dni_usuario=Auth::user()->id;
                      $detalle->cod_area=47812;
                      $detalle->estado_detalle_kardex=1;
                      $detalle->restante_detalle=$cantidadrecibida_tallas[$k];
                      $detalle->cod_kardex_material=$codigo_mat;
                      $detalle->fecha_ingreso=$mytime;
                      $record=DB::table('detalle_kardex_material')->where('cod_kardex_material','=',$codigo_mat)->get();
                      if(count($record))
                      {
                        $cantidad=count($record)-1;
                        $detalle->stock=$record[$cantidad]->stock+$cantidadrecibida_tallas[$k];
                        $stocktotal=$record[$cantidad]->stock+$cantidadrecibida_tallas[$k];
                      }
                      else {
                        $detalle->stock=$record[0]->stock+$cantidadrecibida_tallas[$k];
                        $stocktotal=$record[0]->stock+$cantidadrecibida_tallas[$k];
                      }
                      $detalle->cantidad_ingresada=$cantidadrecibida_tallas[$k];
                      $detalle->trasladador_material=$persona_talla[0];
                      $detalle->save();
                      $act=DetalleOrdenCompra::where('cod_orden_compra',$id)
                      ->where('cod_material',$codigo_mat)
                      ->update(['cantidad_restante'=>$restante,'cantidad_recibida'=>$cantidadrecibida_tallas[$k]]);
                      $act=DB::table('detalle_orden_compra')
                      ->where('cod_orden_compra',$id)
                      ->where('cod_material',$codigo_mat)
                      ->whereNull('fecha_deposito')
                      ->get();
                      if(count($act)>0)
                      {
                        for ($l=0; $l < count($act); $l++) {
                          $fpago=Date('y:m:d', strtotime("+".$act[$l]->fecha_pago." days"));
                          $actfecha=DetalleOrdenCompra::where('cod_orden_compra',$id)
                          ->where('cod_material',$codigo_mat)
                          ->whereNull('fecha_deposito')
                          ->update(['fecha_deposito'=>$fpago]);
                        }
                      }
                      $act=Kardex::where('cod_material',$codigo_mat)
                      ->update(['stock_total'=>$stocktotal]);
                    }
                    else {
                      $restante=$cantidadessinrecibir_tallas[$k]-$cantidadrecibida_tallas[$k];
                      if($cantidadrecibida_tallas[$k]>$cantidadessinrecibir_tallas[$k])
                      {
                        $errores.=$codigo_mat." ";
                        $cantidadrecibida_tallas[$k]=$cantidadessinrecibir_tallas[$k];
                        $restante=0;
                      }
                      $kard=new Kardex;
                      $kard->lugar_almacenaje=$lugar_tallas[0];
                      $kard->cod_almacen=$almacen_tallas[0];
                      $kard->RUC_empresa=$empresa;
                      $kard->stock_total=$cantidadrecibida_tallas[$k];
                      $kard->cod_kardex_material=$codigo_mat;
                      $kard->cod_material=$codigo_mat;
                      $kard->save();
                      $detalle=new KardexDetalle;
                      $detalle->dni_usuario=Auth::user()->id;
                      $detalle->costo_material=$costo_unitario;
                      $detalle->cod_area=47812;
                      $detalle->estado_detalle_kardex=1;
                      $detalle->restante_detalle=$cantidadrecibida_tallas[$k];
                      $detalle->cod_kardex_material=$codigo_mat;
                      $detalle->fecha_ingreso=$mytime;
                      $detalle->stock=$cantidadrecibida_tallas[$k];
                      $stocktotal=$cantidadrecibida_tallas[$k];
                      $detalle->cantidad_ingresada=$cantidadrecibida_tallas[$k];
                      $detalle->trasladador_material=$persona_talla[0];
                      $detalle->save();
                      $act=DetalleOrdenCompra::where('cod_orden_compra',$id)
                      ->where('cod_material',$codigo_mat)
                      ->update(['cantidad_restante'=>$restante,'cantidad_recibida'=>$cantidadrecibida_tallas[$k]]);
                      $act=DB::table('detalle_orden_compra')
                      ->where('cod_orden_compra',$id)
                      ->where('cod_material',$codigo_mat)
                      ->whereNull('fecha_deposito')
                      ->get();
                      if(count($act)>0)
                      {
                        for ($l=0; $l < count($act); $l++) {
                          $fpago=Date('y:m:d', strtotime("+".$act[$l]->fecha_pago." days"));
                          $actfecha=DetalleOrdenCompra::where('cod_orden_compra',$id)
                          ->where('cod_material',$codigo_mat)
                          ->whereNull('fecha_deposito')
                          ->update(['fecha_deposito'=>$fpago]);
                        }
                      }
                      $act=Kardex::where('cod_material',$codigo_mat)
                      ->update(['stock_total'=>$stocktotal]);
                    }
                  }
                }
              }
          }

        }
        if($errores=="")
        {
          session()->flash('success','Materiales Recogidos');
        }
        else {
          session()->flash('warning','Solo se ingreso las cantidades registradas en la orden de compra');
        }
       return Redirect::to('logistica/ingreso_salida');
    }
    catch(Exception $e)
    {
      session()->flash('error','Hubo un error al recepcionar, Por favor de click al boton correguir orden si el problema persiste contacte al soporte tecnico');
      return Redirect::to('logistica/recoger/'.$id);
    }

  }
}
