<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Charts;
use Illuminate\Support\Facades\Auth;
class ChartController extends Controller
{
  /**

   * Show the application dashboard.

   *

   * @return \Illuminate\Http\Response

   */
   public function crear($request)
   {
     return $request;
   }
  public function makeChart($type)

  {
    $users = DB::table('trabajador')
    ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
            ->get();
    $planilla=0;
    $destajo=0;
    $jornal=0;
    for($i=0;$i<count($users);$i++)
    {
      $nombres=$users[$i]->nombres." ".$users[$i]->apellido_paterno." ".$users[$i]->apellido_materno;
      switch ($users[$i]->cod_tipo_trabajador) {
        case '1':
          $planilla++;
          break;
        case '2':
        $jornal++;
          break;
        case '3':
        $destajo++;
        break;
        default:
          break;
      }
    }
      switch ($type) {
          case 'pie':

              $chart = Charts::create('pie', 'highcharts')

                          ->title('HDTuto.com Laravel Pie Chart')

                          ->labels(['Planilla', 'Jornal', 'Destajo'])

                          ->values([$planilla,$jornal,$destajo])

                          ->dimensions(1000,500)

                          ->responsive(true);

              break;



          case 'donut':

              $chart = Charts::create('donut', 'highcharts')

                          ->title('HDTuto.com Laravel Donut Chart')

                          ->labels(['First', 'Second', 'Third'])

                          ->values([5,10,20])

                          ->dimensions(1000,500)

                          ->responsive(true);

              break;



          case 'line':

              $chart = Charts::create('line', 'highcharts')

                          ->title('HDTuto.com Laravel Line Chart')

                          ->elementLabel('HDTuto.com Laravel Line Chart Lable')

                          ->labels(['First', 'Second', 'Third'])

                          ->values([5,10,20])

                          ->dimensions(1000,500)

                          ->responsive(true);

              break;



          case 'area':

              $chart = Charts::create('area', 'highcharts')

                          ->title('Produccion')

                          ->elementLabel("pro")


                          ->labels(['Enero', 'Febrero', 'Marzo','Abril','Mayo','Junio','Agosto','Setiembre','Octubre','Noviembre','Diciembre'])

                          ->values([200,100,500,150,250])

                          ->dimensions(1000,500)

                          ->responsive(true);

              break;



          case 'geo':

              $chart = Charts::create('geo', 'highcharts')

                          ->title('HDTuto.com Laravel GEO Chart')

                          ->elementLabel('HDTuto.com Laravel GEO Chart label')

                          ->labels(['PE', 'FR', 'RU'])

                          ->colors(['#3D3D3D', '#985689'])

                          ->values([5,10,20])

                          ->dimensions(1000,500)

                          ->responsive(true);

              break;



          default:

              # code...

              break;

      }

      return view('chart', compact('chart'));

  }
}
