<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
use \Milon\Barcode\DNS1D;

class PdfVentasOrdenPedidosController extends Controller
{
    public function index($id)
    {
        if ($id != "") {
            $pdf = \App::make('dompdf.wrapper');
            $pdf->setPaper('a4', 'landscape');
            $pdf->loadHTML($this->convert_data($id));
            return $pdf->stream();
        }
    }

    function get_data_header($id_sale_order)
    {
        return DB::table('orden_pedidos_ventas')
            ->where('orden_pedidos_ventas.id', '=', $id_sale_order)
            ->leftJoin('cliente_ventas', 'cliente_ventas.cliente_id', '=', 'orden_pedidos_ventas.cliente_id')
            ->get();
    }

    function get_data_detail($id_sale_order)
    {
        $orden_pedidos_ventas_detalles = DB::table('orden_pedidos_ventas_detalle')
            ->where('orden_pedidos_ventas_detalle.orden_pedidos_ventas_id', '=', $id_sale_order)
            ->leftJoin('inventario_producto_terminado', 'inventario_producto_terminado.cod_producto_terminado', '=', 'orden_pedidos_ventas_detalle.modelo_id')
            ->leftJoin('desarrollo_producto_ventas_tmp', 'desarrollo_producto_ventas_tmp.cod_desarrollo_producto', '=', 'inventario_producto_terminado.cod_desarrollo_producto')
            ->get();

        foreach ($orden_pedidos_ventas_detalles as $orden) {
            $orden->tallas = explode("-", $orden->tallas);
        }

        return $orden_pedidos_ventas_detalles;
    }

    function get_imagen()
    {
        $idempresa = Auth::user()->RUC_empresa;
        return DB::table('empresa')->where('RUC_empresa', '=', $idempresa)->limit(1)->get();
    }

    function convert_data($id_sale_order): string
    {
        $header_sale_order = $this->get_data_header($id_sale_order);
        $detail_sale_order = $this->get_data_detail($id_sale_order);
        $img = $this->get_imagen();
        $photo = "";

        foreach ($img as $i) {
            if ($i->imagen != "") {
                $photo = $i->imagen;
            }
        }

        $output = '<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {
          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    .column {
      float: left;
      width: 50%;
      padding: 5px;
    }

    .row::after {
      content: "";
      clear: both;
      display: table;
    }
    </style></head><body>';
        $output .= '
              <header>
                  <div class="row">
                    <div class="column">';
        if ($photo != "") {
            $output .= '<img src="photo/' . $photo . '" alt="" style="width:120px;" class="img-rounded">';
        } else {
            $output .= '<div></div>';
        }
        $output .= '
                    </div>
                    <div class="column" style="margin-top:3%;">
                      <img src="photo/pie2.png" width="100%"/>
                    </div>
                  </div>
              </header>';

        $output .= '<h1>Orden de Pedido ' . $header_sale_order[0]->codigo . ' </h1>

              <table style="width:100%;" CELLPADDING="10"
              ALIGN="center">
                            <tr>
                                <td>Cliente: ' . $header_sale_order[0]->apellidos . ', ' . $header_sale_order[0]->nombres . ' </td>
                                <td></td>
                                <td style="text-align:right;">Fecha de Pedido: ' . $header_sale_order[0]->fecha_pedido . '</td>
                                <td style="text-align:right;">Fecha de Entrega: ' . $header_sale_order[0]->fecha_entrega . '</td>
                            </tr>
              </table>
                        <br>
              <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
                <tr>
                  <th style="border-collapse: collapse; border: 1px solid black;">Imagen</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Cod. Modelo</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">T1</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">T2</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">T3</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">T4</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">T5</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">T6</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">T7</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Cantidad Pedida</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Costo Total</th>
                </tr>';

        foreach ($detail_sale_order as $body) {
            //vars needed :c
            //Extracción ruta imagen, debo quitarle un / por la posición
            $ruta = substr($body->imagen, 1);
            //Numero de tallas
            $num_size = count($body->tallas);
            //Agrupamos en un array las tallas del producto
            $arr_amount_per_size = [$body->pedido_cantidad_t1, $body->pedido_cantidad_t2, $body->pedido_cantidad_t3, $body->pedido_cantidad_t4, $body->pedido_cantidad_t5,
                $body->pedido_cantidad_t6, $body->pedido_cantidad_t7];
            //variable para saber la cantidad de modelos pedidos y for para sumar los valores del anterior array
            $ordered_quantity = 0;
            foreach ($arr_amount_per_size as $item) {
                $ordered_quantity += $item;
            }
            $output .= '
              <tr>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">
                    <img src="' . $ruta . '" alt="Foto modelo" width="80">
                </td>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $body->modelo_id . '</td>';

            for ($i = 0; $i < $num_size; $i++) {
                $output .= '<td style="border-collapse: collapse; border: 1px solid black;text-align: center;">T' . $body->tallas[$i] . '
                                <br /> ' . $arr_amount_per_size[$i] . '
                            </td>';
            }
            $output .= str_repeat('<td style="border-collapse: collapse; border: 1px solid black;text-align: center;">-</td>', 7 - $num_size);

            $output .= '
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $ordered_quantity . '</td>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;"> ' . $body->valor_total . '</td>
              </tr>
            ';
        }


        $output .= '
              </table>
              <br><br>
              <table  CELLPADDING="3" ALIGN="right">
                <tr>
                  <td>Sub Total: </td>
                  <td>S/.' . $header_sale_order[0]->costo_total . '</td>
                </tr>
                <tr>
                    <td>IGV (18%): </td>
                    <td>S/.' . $header_sale_order[0]->costo_total * 0.18 . '</td>
                </tr>
                <tr>
                      <td>Total (Con igv): </td>
                      <td>S/.' . $header_sale_order[0]->costo_total * 1.18 . '</td>
                </tr>
              </table>
              ';
        $output .= '</body></html>';
        return $output;
    }
}

