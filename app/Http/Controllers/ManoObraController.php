<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\ManoObraModel;
use erpCite\Http\Requests\ManoObraFormRequest;
use DB;

class ManoObraController extends Controller
{
    public function __construct(){
      $this->middleware('jefe');

    }

    public function index(Request $request){
        if ($request) {
          $mes_actual=date("m");
            $manoobra=DB::table('gasto_sueldos')
            ->join('area','gasto_sueldos.cod_area','=','area.cod_area')
            ->where('gasto_sueldos.RUC_empresa',Auth::user()->RUC_empresa)
            ->where('gasto_sueldos.es_externo','=',0)
            ->whereMonth('gasto_sueldos.fecha_creacion', '=', $mes_actual)
            ->get();
            return view('costos/indirectos/mano_obra/index',['manoobra'=>$manoobra]);
        }
    }

    public function create(Request $request){
        if($request)
        {

          $area=DB::table('area')
          ->orderBy('descrip_area','asc')->get();
          $general=DB::Table('datos_generales')
          ->where('RUC_empresa',Auth::user()->RUC_empresa)
          ->get();

          $empresas=DB::table('empresa')
          ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
          ->select('cod_regimen_laboral')
          ->get();
          $beneficios=DB::table('escala_salarial')
          ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
          ->where('cod_regimen_laboral','=',$empresas[0]->cod_regimen_laboral)
          ->avg('tasa_beneficio');
          return view('costos.indirectos.mano_obra.create',['beneficios'=>$beneficios,'area'=>$area,'general'=>$general]);
        }

    }


    public function store()
    {
      //Se Registra el campo detalle_orden_compra
      $identificador=rand(100000,999999);
      $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
      $siglax = $sigla[0]->siglas;
      $res=$siglax.'-'.$identificador;
      $idempresa=Auth::user()->RUC_empresa;
      $manoobra=new ManoObraModel;
      $manoobra->id_gastos_sueldos=$res;
      $manoobra->beneficios=Input::get('beneficios_sociales');
      $manoobra->otros=Input::get('otros_sueldos');
      $manoobra->gasto_mensual=Input::get('gasto_mensual');
      $manoobra->cod_area=Input::get('cod_area');
      $manoobra->RUC_empresa=$idempresa;
      $manoobra->sueldo_mensual=Input::get('sueldo_mensual');
      $manoobra->puesto=Input::get('puesto');
      $manoobra->fecha_creacion=Input::get('fecha_creacion');
      $manoobra->estado=1;
      $manoobra->save();
      session()->flash('success','Mano de Obra Indirecta ingresado satisfactoriamente');
      return Redirect::to('costo/indirectos/mano_obra');
    }
  public function show()
  {
    return view ("costos/indirectos/mano_obra/index");
  }
  public function edit($id)
  {
    return Redirect::to('costos/indirectos/mano_obra');
  }
  public function update(ManoObraFormRequest $request,$id)
  {
    /*$categoria=Categoria::findOrFail($id);
    $categoria->nombre=$request->get('nombre');
    $categoria->descripcion=$request->get('descripcion');
    $categoria->update();
    return Redirect::to('almacen/categoria');*/
    return Redirect::to('costos/indirectos/mano_obra');
  }
  public function destroy($id)
  {
    $email=Input::get('email');
    $estado=Input::get('estado');
    if($estado==0){$mensaje=" Eliminado";}
    else {
      $mensaje=" Activado";
    }
    $act=ManoObraModel::where('id_gastos_sueldos',$email)
    ->delete();
      session()->flash('success','Registro'.$mensaje);
      return Redirect::to('costo/indirectos/mano_obra');
  }
}
