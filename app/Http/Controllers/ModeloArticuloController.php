<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\Modelo;
use erpCite\SerieModeloModel;
use erpCite\CostoModelo;
use erpCite\DetalleCostoModeloMaterial;
use erpCite\DetalleCostoModeloMano;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;
class ModeloArticuloController extends Controller
{
  public function __construct()
  {
    $this->middleware('costo');
  }
  public function index($var)
  {
    if ($var) {
      $model=explode("+",$var);
      $modelo=$model[0];
      $serie=$model[1];
      $codigo=$model[2];
      $empresa=Auth::user()->RUC_empresa;
      $modelos=DB::table('serie_modelo')
      ->join("modelo",'serie_modelo.codigo_modelo','=','modelo.cod_modelo')
      ->join("coleccion","modelo.cod_coleccion",'=',"coleccion.codigo_coleccion") //'=' <-- Puede ser omitido
      ->join("material",'modelo.cod_horma','=','material.cod_material')
      ->join("serie",'serie_modelo.codigo_serie','=','serie.cod_serie')
      ->leftJoin('costo_modelo','serie_modelo.codigo','=','costo_modelo.modelo_serie')
      ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where("serie_modelo.codigo_modelo","Like",$modelo."%")
      ->where("serie_modelo.codigo_serie","=",$serie)
      ->where('serie_modelo.estado','=','3')
      ->orWhere('serie_modelo.estado','=','4')
      ->get();
      return view('costos.modelo_articulo.index',['modelos'=>$modelos,"var"=>$modelo,"serie"=>$serie,"codigo"=>$codigo]);
    }
  }
  public function nuevo($var)
  {
    if ($var) {
      $model=explode("+",$var);
      $modelo=$model[0];
      $serie=$model[1];
      $codigo=$model[2];
      $modelos=DB::table('serie_modelo')
      ->join("modelo",'serie_modelo.codigo_modelo','=','modelo.cod_modelo')
      ->join("coleccion","modelo.cod_coleccion","=","coleccion.codigo_coleccion")
      ->join("material",'modelo.cod_horma','=','material.cod_material')
      ->join("serie",'serie_modelo.codigo_serie','=','serie.cod_serie')
      ->join("linea",'modelo.cod_linea','=','linea.cod_linea')
      ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where('serie_modelo.codigo','=',$codigo)
      ->get();
      $cantidad=DB::table('serie_modelo')
      ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where("serie_modelo.codigo_modelo","Like",$modelo."%")
      ->where("serie_modelo.codigo_serie","=",$serie)
      ->where('serie_modelo.estado','=','3')
      ->count();
     $capellada=DB::table('capellada')
     ->get();
      return view("costos.modelo_articulo.create",["modelo"=>$modelos,"capellada"=>$capellada,"cantidad"=>$cantidad,"var"=>$var]);
    }

  }
  public function store(Request $data)
  {
    $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
    $siglax = $sigla[0]->siglas;
    $url=Input::get('ur');
    $model=explode("+",$url);
    $ModeloCalzado=new Modelo;
    $empresa=Auth::user()->RUC_empresa;
    if(!file_exists('photo/modelos/'.$empresa))
    {
      mkdir('photo/modelos/'.$empresa);
    }
    $photo="";
    $destination='photo/modelos/'.$empresa;
    $file= $data->photo;
    $extension=$file->getClientOriginalExtension();
    $filename=Input::get('codigo_modelo').Input::get('photo').".".$extension;
    $file->move($destination,$filename);
    $photo=$filename;

    $ModeloCalzado->cod_modelo=Input::get('codigo_modelo');
    $ModeloCalzado->cod_linea=Input::get('linea');
    $ModeloCalzado->cod_capellada=Input::get('capellada');
    $ModeloCalzado->cod_forro=Input::get('forro');
    $ModeloCalzado->cod_plantilla=Input::get('plantilla');
    $ModeloCalzado->cod_piso=Input::get('piso');
    $ModeloCalzado->nombre=Input::get('nombre_modelo');
    $ModeloCalzado->RUC_empresa=$empresa;
    $ModeloCalzado->descripcion=Input::get('descripcion_modelo');
    $ModeloCalzado->estado_modelo=3;
    $ModeloCalzado->cod_horma=Input::get('horma');
    $ModeloCalzado->cod_coleccion=Input::get('coleccion');
    $ModeloCalzado->imagen=$photo;
    $ModeloCalzado->save();

    $codseriemodelo=rand(1,999999);
    $seriestotales=Input::get('serie');
    $serieModelo=new SerieModeloModel;
    $serieModelo->codigo=$siglax.$codseriemodelo;
    $serieModelo->codigo_modelo=Input::get('codigo_modelo');
    $serieModelo->codigo_serie=$seriestotales;
    $serieModelo->estado=3;
    $serieModelo->RUC_empresa=$empresa;
    $serieModelo->save();

    $identificador=rand(100000,999999);
    $idempresa=Auth::user()->RUC_empresa;
    $identificador=$siglax."-".$identificador;
    $articulo=new CostoModelo;
    $articulo->cod_costo_modelo=$identificador;
    $articulo->RUC_empresa=$idempresa;
    $articulo->estado=3;
    $articulo->modelo_serie=$siglax.$codseriemodelo;
    $articulo->save();

    $detallepadre=DB::table("detalle_costo_modelo_materiales")
    ->join("costo_modelo","detalle_costo_modelo_materiales.cod_costo_modelo","=","costo_modelo.cod_costo_modelo")
    ->where("costo_modelo.modelo_serie",'=',$model[2])
    ->get();
    $total=0;
    for ($i=0; $i < count($detallepadre); $i++) {
      $articulo=new DetalleCostoModeloMaterial;
      $articulo->cod_detalle_costo_material=rand(100000,999999);
      $articulo->cod_costo_modelo=$identificador;
      $articulo->cod_material=$detallepadre[$i]->cod_material;
      $articulo->cod_area=$detallepadre[$i]->cod_area;
      $articulo->consumo_por_par=$detallepadre[$i]->consumo_por_par;
      $articulo->valor_unitario=$detallepadre[$i]->valor_unitario;
      $articulo->total=$detallepadre[$i]->total;
      $articulo->estado_material_costos=1;
      $articulo->save();
      $total=$total+$detallepadre[$i]->total;
    }
    $act=CostoModelo::where('cod_costo_modelo',$identificador)
    ->update(['total_materiales'=>$total]);
    $detallepadre=DB::table("detalle_costo_modelo_manoobra")
    ->join("costo_modelo","detalle_costo_modelo_manoobra.cod_costo_modelo","=","costo_modelo.cod_costo_modelo")
    ->where("costo_modelo.modelo_serie",'=',$model[2])
    ->where("detalle_costo_modelo_manoobra.estado_trabajador","=",1)
    ->get();
    echo $detallepadre;
    $total=0;
    for ($i=0; $i < count($detallepadre); $i++) {
      $articulo=new DetalleCostoModeloMano;
      $articulo->cod_detalle_costo_mano=rand(100000,999999);
      $articulo->cod_costo_modelo=$identificador;
      $articulo->cod_tipo_trabajador=$detallepadre[$i]->cod_tipo_trabajador;
      $articulo->cod_area=$detallepadre[$i]->cod_area;
      $articulo->operacion=$detallepadre[$i]->operacion;
      $articulo->costo=$detallepadre[$i]->costo;
      $articulo->beneficio=$detallepadre[$i]->beneficio;
      $articulo->costo_por_par=$detallepadre[$i]->costo_por_par;
      $articulo->save();
      $total=$total+$detallepadre[$i]->costo_por_par;
    }
    $act=CostoModelo::where('cod_costo_modelo',$identificador)
    ->update(['total_obra'=>$total]);
session()->flash('success','Articulo de modelo creado');
    return Redirect::to('costo/directo/articulo/'.$url);
  }
  public function show()
  {
    return view('logistica.orden_compra.index',["materiales"=>$materiales]);
  }
  public function edit($id)
  {
    return Redirect::to('logistica.orden_compra');
  }
  public function update(CategoriaFormRequest $request,$id)
  {

    return Redirect::to('logistica.orden_compra');
  }
  public function destroy($id)
  {

    return Redirect::to('logistica.orden_compra');
  }
}
