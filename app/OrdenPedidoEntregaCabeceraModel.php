<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OrdenPedidoEntregaCabeceraModel extends Model
{
    protected $table = 'orden_pedido_entrega';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'codigo',
        'orden_pedidos_ventas_id',
        'total',
        'observaciones'
    ];

    protected $guarded = [];
}
