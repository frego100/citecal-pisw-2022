<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ClienteVentasModel extends Model
{

    protected $table = 'cliente_ventas';
    protected $primaryKey = "cliente_id";

    protected $keyType = "integer";

    public $timestamps = false;

    protected $fillable = [
        'tipo_documento',
        'numero_documento',
        'nombres',
        'apellidos',
        'pais_id',
        'departamento_id',
        'direccion_1',
        'direccion_2',
        'celular_1',
        'celular_2',
        'correo'
    ];

    protected $guarded = [];
}
