<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class GastosRepresentacion extends Model
{
  protected $table='gasto_representacion';

  protected $primaryKey="cod_gasrepre";

  public $timestamps=false;


  protected $fillable=[ 'descripcion','estado','RUC_empresa','gasto','fecha_creacion'];

  protected $guarded=[];
}
