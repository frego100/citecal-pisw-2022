<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OrdenPedidosVentasCabeceraModel extends Model
{
    protected $table = 'orden_pedidos_ventas';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'codigo',
        'cliente_id',
        'fecha_entrega',
        'costo_total',
        'observaciones'
    ];

    protected $guarded = [];
}
