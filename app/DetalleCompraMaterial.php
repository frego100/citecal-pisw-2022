<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleCompraMaterial extends Model
{
    protected $table='detalles_compramaterial';

    protected $primaryKey='id_commat';
  
    public $timestamps=false;
  
    protected $fillable=['codigo_orden','cod_material','cantidad_recibida','cantidad_faltante','cantidad_pedida','total','subtotal'];
  
    protected $guarded=[];
}
