<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleOrdenPedido extends Model
{
    //
	protected $table='detalle_orden_pedido';

  protected $primaryKey="codigo_orden_pedido";

  public $timestamps=false;

  protected $keyType = "string";

  protected $fillable=['codigo_serie_articulo','cantidades','costo_total'];

  protected $guarded=[];

}
