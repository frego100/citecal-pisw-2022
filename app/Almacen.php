<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{
  protected $table='almacen';

  protected $primaryKey="cod_almacen";

  public $timestamps=false;


  protected $fillable=['nom_almacen','descrip_almacen', 'DNI_trabajador','RUC_empresa','estado_almacen','tipo_almacen','cod_categoria'];

  protected $guarded=[];
}
