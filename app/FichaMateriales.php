<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class FichaMateriales extends Model
{
  protected $table='ficha_materiales';

  protected $primaryKey="cod_ficha_m";

  public $timestamps=false;


  protected $fillable=['cod_modelo_comb', 'cod_proceso', 'cod_material','consumo_real','costo_por_par','RUC_empresa'];

  protected $guarded=[];
}