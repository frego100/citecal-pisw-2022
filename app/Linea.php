<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Linea extends Model
{
  protected $table='linea';

  protected $primaryKey="cod_linea";

  public $timestamps=false;


  protected $fillable=['nombre_linea','RUC_empresa','estado_linea'];
						

  protected $guarded=[];
}
