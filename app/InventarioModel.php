<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class InventarioModel extends Model
{
    protected $table = 'inventario_producto_terminado';

    protected $primaryKey = "cod_producto_terminado";

    public $timestamps = false;

    protected $fillable = [
        'cod_desarrollo_producto',
        'cod_almacen',
        'codigo',
        'descripcion',
        'imagen',
        'cantidad_T1',
        'cantidad_T2',
        'cantidad_T3',
        'cantidad_T4',
        'cantidad_T5',
        'cantidad_T6',
        'cantidad_T7'
    ];

    protected $guarded = [];
}
