<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DatosGeneralesModel extends Model
{
  protected $table='datos_generales';

  protected $primaryKey="RUC_empresa";

  public $timestamps=false;


  protected $fillable=['cod_retencion_utilidades','TCEA_capital','lead_time','porcentaje_reproceso','TCEA_credito','porcentaje_comision_ventas','politica_desarrollo_producto','	politica_desarrollo_horma','politica_desarrollo_troqueles','produccion_promedio'];
  protected $guarded=[];
}
