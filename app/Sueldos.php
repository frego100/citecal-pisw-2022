<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Sueldos extends Model
{
  protected $table='gasto_sueldos';

  protected $primaryKey="id_gastos_sueldos";

  public $timestamps=false;


  protected $fillable=['area','puesto','dni_trabajador','nombre_trabajador','sueldo_mensual','beneficios','otros','gasto_mensual','estado','RUC_empresa'];
														
  protected $guarded=[];
}

								