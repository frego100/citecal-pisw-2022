<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OrdenPedidosVentasModel extends Model
{
    protected $table = 'orden_pedidos_ventas_detalle';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'orden_pedidos_ventas_id',
        'modelo_id',
        'pedido_cantidad_t1',
        'pedido_cantidad_t2',
        'pedido_cantidad_t3',
        'pedido_cantidad_t4',
        'pedido_cantidad_t5',
        'pedido_cantidad_t6',
        'pedido_cantidad_t7',
        'pedido_cantidad_t8',
        'cantidad_total_pares',
        'valor_total'
    ];

    protected $guarded = [];
}
