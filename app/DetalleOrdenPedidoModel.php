<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleOrdenPedidoModel extends Model
{
  protected $table='detalle_orden_pedido';

  public $timestamps=false;


  protected $fillable=['codigo_orden_pedido','codigo_serie_articulo','cantidades','costo_total','tipo_urgencia'];

  protected $guarded=[];
}
