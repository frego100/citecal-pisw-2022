<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class OrdenPedidoEntregaDetalleModel extends Model
{
    protected $table = 'orden_pedido_entrega_detalle';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'orden_pedido_entrega_id',
        'orden_pedido_ventas_detalle_id',
        'cantidad_pedida',
        'cantidad_faltante',
        'total'
    ];

    protected $guarded = [];
}
