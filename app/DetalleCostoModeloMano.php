<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleCostoModeloMano extends Model
{
  protected $table='detalle_costo_modelo_manoobra';

  protected $primaryKey='cod_detalle_costo_mano';

  public $timestamps=false;
protected $keyType = "string";

  protected $fillable=['cod_costo_modelo','cod_area','cod_tipo_trabajador','operacion','costo','beneficio','costo_por_par','porcentaje','otros_costos'];

  protected $guarded=[];
}
