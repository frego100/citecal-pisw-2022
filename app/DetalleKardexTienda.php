<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleKardexTienda extends Model
{
    protected $table='detalle_kardex_tienda';

    protected $primaryKey='cod_detalle_tienda';

    protected $keyType="string";

    public $timestamps=false;

    protected $fillable=['fecha_ingreso','fecha_salida','fecha_devolucion','stock','cantidad_ingresada','cantidad_salida','trasladador_material',
    'cod_kardex_tienda','comentario_devolucion','cod_area','estado_detalle_kardex','restante_detalle','costo_material','dni_usuario','codigo_salida'];

    protected $guarded=[];
}
