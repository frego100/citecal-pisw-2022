<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class SalidaProductoModel extends Model
{
    protected $table = 'salida_producto';

    protected $primaryKey = "cod_salida";

    public $timestamps = false;

    protected $fillable = [
        'cod_producto_terminado',
        'motivo_salida',
        'cantidad_T1',
        'cantidad_T2',
        'cantidad_T3',
        'cantidad_T4',
        'cantidad_T5',
        'cantidad_T6',
        'cantidad_T7'
    ];

    protected $guarded = [];
}
