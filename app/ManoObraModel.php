<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ManoObraModel extends Model
{
  protected $table='gasto_sueldos';

public $timestamps=false;
  protected $primaryKey="id_gasto_sueldos";

protected $fillable=['sueldo_mensual','beneficios','otros','gasto_mensual','cod_area','RUC_empresa','puesto','estado','fecha_creacion'];

protected $guarded=[];
}
