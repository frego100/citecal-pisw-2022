<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleCostoModeloMaterial extends Model
{
  protected $table='detalle_costo_modelo_materiales';

  protected $primaryKey='cod_detalle_costo_material';

  public $timestamps=false;
protected $keyType = "string";

  protected $fillable=['cod_costo_modelo','cod_material','cod_area','consumo_por_par','valor_unitario','total','estado_material_costos'];

  protected $guarded=[];
}
