<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class NoticiaModel extends Model
{
  protected $table='noticias';

  protected $primaryKey="cod";

  public $timestamps=false;


  protected $fillable=['url_imagen','nombre_evento','descripcion','categoria_difusion'];

  protected $guarded=[];
}
